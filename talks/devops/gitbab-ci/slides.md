##### GitLab CI

![logo](backgrounds/cover.webp) <!-- .element height="60%" width="60%" -->

*Nicolas Singer*
---

#### Que fait un outil de CI/CD ? 
- aide à automatiser le **développement**, le **déploiement** et le **test**.
- Cette automatisation fiabilise les opérations
 
  ![logo](backgrounds/ci-cd.png) 

---

#### Les principaux outils

![logo](backgrounds/outils.png) 

---

#### GitLab CI
- GitLab n’est pas qu'un GIT dans le cloud.
- C’est aussi un outil complet de CI/CD.
- Gratuit pour les opérations essentielles.

- Permet de rassembler sous un seul portail l’ensemble des opérations nécessaires au code.
   
---

#### Le fichier `.gitlab-ci.yml` 

- C’est le fichier qui définit les opérations d’automatisation.
- Il est au format *yaml* et doit se trouver à la racine du projet.

```yaml
build-job:
  stage: build
  script:
    - echo "Ici on construit le programme"

test-job:
  stage: test
  script:
    - echo "Ici on le teste"

deploy-job:
  stage: deploy
  script:
    - echo "Ici on le déploie"
 ```

---

#### Le fichier `.gitlab-ci.yml` 

- A chaque nouveau **push**, GitLab exécute un *pipeline* composé des *jobs* décrits dans le fichier `gitlab-ci.yml`. 
- Les *jobs* définissent ce qui doit être fait.
- Les *jobs* sont regroupés en étapes (*stage*) qui déterminent le moment où ils s’exécutent.
- Les étapes typiques sont *build*, *test*, *package*, *deploy*, mais on peut en créer d’autres. 

---
#### Exemple de pipeline

```yaml
variables:
  FRONTEND_IMAGE: $CI_REGISTRY/nicolassinger/jury/frontend:latest

stages:
  - build
  - package

build-job:
  stage: build
  image: node:16-alpine
  script:
   - cd frontend
   - npm install
   - npm run ng build 
  artifacts:
    paths:
      - frontend/dist/

docker-job:
  stage: package
  script:
    - cd frontend
    - docker build -t $FRONTEND_IMAGE .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $FRONTEND_IMAGE
```

---
#### Visualiser le déroulement du pipeline

- On passe à l’étape suivante quand tous les *jobs* ont réussi.

![logo](backgrounds/pipeline.png) 

- Sinon le pipeline s'arrête et échoue
 
  ![logo](backgrounds/failed_pipeline.png) 

---
#### Le pipeline en détail

- La section `stages` définit les étapes et leur séquence d'exécution.

```yaml
stages:
  - build
  - release
  - package
```

- Ici, on énumère trois étapes, les *jobs* de l’étape `build` tournent en premier, puis ceux de l’étape `release`
et enfin ceux de l’étape `package`.


---
#### Le pipeline en détail

- La section `variables` définit les variables utilisables dans la définition des *jobs*

```yaml
variables:
  DOCKER_TLS_CERTDIR: "/certs"
  BACKEND_IMAGE: $CI_REGISTRY/nicolassinger/jury/jurybackend:latest
  FRONTEND_IMAGE: $CI_REGISTRY/nicolassinger/jury/juryfrontend:latest
```

- Certaines variables sont prédéfinies comme par exemple : 
  - $CI_REGISTRY 
  : URL du registre des conteneurs

  - $CI_REGISTRY_USER
  : Login de connexion au registre ci-dessus

  - $CI_REGISTRY_PASSWORD
  : Mot de passe du login ci-dessus

---
#### Définition d’un *job*

- Un *job* se définit par une section portant son nom  et s’exécute dans une image **docker**.
- Sa section `script` définit les commandes à exécuter.
- Sa section `artifacts` définit les dossiers à transmettre à l'étape suivante.

```yaml
build-job:
  stage: build # à quelle étape éxécuter le job
  image: node:16-alpine # dans quelle image docker exécuter les commandes 
  script: # les commandes à exécuter
  - cd frontend
  - npm install
  - npm run ng build
  artifacts: # les résultats de l'exécution à conserver pour l'étape suivante
    paths:
    - frontend/dist/ # le contenu de ce dossier est transmis à l'étape suivante
```

---
#### Exécution d’un *job* sous condition
- On peut ajouter une section `rules` pour conditionner l’exécution d’un *job* à un état.
- L'état peut être par exemple : 
  - On a *commit* sur une certaine branche

    ```yaml
    rules:
       - if: $CI_COMMIT_BRANCH == "main"
    ```
  - On a modifié le contenu d'un fichier ou d'un dossier
    ```yaml
    rules:
        - changes:
          - frontend/*
    ```
  - On a *commit* un *tag*.
    ```yaml
    rules:
      - if: $CI_COMMIT_TAG  
    ```
    
---
#### Exemple complet d’un *job* sous condition
- Ici par exemple, on réalise le build quelle que soit la branche qui est *commit*, mais on ne fait le packaging
que si c’est la branche *main* :

```yaml
stages:
  - build
  - package

build-job:
  stage: build
  image: node:16-alpine
  script:
   - npm install ; npm run ng build 
  artifacts:
    paths:
      - dist/

package-job:
  stage: package
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  script:
    - docker build -t CI_REGISTRY_IMAGE .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $FRONTEND_IMAGE
```

---

#### Gitlab CI/CD et Kubernetes

- Résumons les étapes d'un CI/CD typique : 
- Le **build** c'est l'étape de construction de l'application. On utilise des outils comme *mave*n, *gradle*, ou *npm*.
- Le **package**, c'est l'étape de construction du conteneur docker. On utilise un `Dockerfile`.
- Le **deploy**, c'est l'étape de déploiement de l'application, on utilise des manifestes *kubernetes*.
---

#### Gitlab CI/CD et Kubernetes
- Avec l'outil **flux** (https://fluxcd.io), les manifestes *kubernetes* peuvent être placés dans un dépôt spécial, dont le 
contenu sera automatiquement déployé dans un cluster *kubernetes*.
- Ces dépôts sont les mêmes que ceux qui stockent les images de conteneurs.
- On place donc les manifestes à la racine du projet à déployer, et avec le pipeline de CI/CD, on les *pousse* dans un dépot de conteneur 
surveillé par `flux`.
---
 
#### Exemple complet d'une application web fullstack

- On organise son application comme ceci : 

![img.png](backgrounds/fullstack_folder.png)

- Le dossier `backend` contient le code de le partie backend.
 - Le dossier `front` contient le code de la partie front
 - Le dossier `kubernetes` contient les manifestes de déploiement.
- Le fichier `gitlab-ci.yaml` définit le pipeline de CI/CD.
---

#### Le fichier `gitlab-ci.yaml`

- Première partie : *build* et *package* du front: 

```yaml
stages:
- build
- package
- deploy

front-build:
  image: node:18-alpine
  stage: build
  rules:
    - changes: [front/**/*]
  script:
    - cd front; npm install ; npm run ng build 
  artifacts:
    paths:
      - front/dist/

front-package:
  stage: package
  rules:
    - changes: [front/**/*]
  script:
    - cd front
    - docker build -t $CONTAINER_IMAGE .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $CONTAINER_IMAGE
```

---
#### Le fichier `gitlab-ci.yaml`
- Deuxième partie : *build* et *package* du back:

```yaml
backend-build:
  image: maven:3.8.4-jdk-11
  stage: build
  rules:
    - changes: [backend/**/*]
  script:
    - cd backend
    - mvn package -Dmaven.test.skip
  artifacts:
    paths:
      - backend/target/*.jar

backend-package:
  stage: package
  rules:
    - changes: [backend/**/*]
  script:
    - cd backend
    - docker build -t $CONTAINER_MASTER_IMAGE .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $CONTAINER_MASTER_IMAGE
```
---

#### Le fichier `gitlab-ci.yaml`
- Dernière partie : écriture des manifestes dans un registre de conteneurs:

```yaml
deploy:
  stage: deploy
  rules:
    - changes: 
      - kubernetes/**/*
      - .gitlab-ci.yml
  image:
    name: fluxcd/flux-cli:v2.1.1
  script:
    - |
      flux push artifact oci://$CI_REGISTRY_IMAGE/oci:latest \
        --path="./kubernetes" \
        --source="$CI_REPOSITORY_URL" \
        --revision="$CI_COMMIT_SHORT_SHA" \
        --creds="$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" \
```






 









