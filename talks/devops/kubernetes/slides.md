![logo](backgrounds/kubernetes.png)  <!-- .element height="40%" width="40%" -->

Automatiser le déploiement, la mise à l’échelle et la gestion d’applications conteneurisées.

<div class="caption">
Nicolas Singer 
</div>
---

#### Problématique

- Les applications modernes peuvent reposer sur beaucoup de services (micro-services)
- Les conteneurs simplifient ces déploiements, mais il faut gérer l’infrastructure qui les exécute.
- Exemple sur une même infrastructure : 
  - Pour une application web, faire faire tourner 3 instances du frontend, 2 instances du back, et 1 instance de la base de données. 
  - Pour une application de traitement d’images, faire tourner 1 instance de la file d'attente, et 4 instances de jobs de traitement. 

--- 

### Problématique (suite)

- Je veux des fonctionnalités supplémentaires comme :
  - Affecter à mes applications une adresse publique
  - auto-dimensionner le nombre de conteneurs nécessaires
  - répartir la charge
  - surveiller et auto-réparer les conteneurs
  - faire des mises en jour sans interruptions des services
  - etc.

---

### Solution : l’orchestration de conteneurs

![logo](backgrounds/orchestration.svg) <!-- .element height="100%" width="100%" -->

---

#### Kubernetes (k8s)
- Plateforme open-source pour orchestrer des conteneurs dans un cluster de machines 
  - Permet de lancer plusieurs conteneurs répartis sur plusieurs machines ou répliqués sur une seule. 
  - Automatisation du redéploiement en cas d’arrêt d’un conteneur ou de panne matérielle

- Le déploiement des conteneurs se fait de façon déclarative : 
  - Le gestionnaire décrit l’état souhaité et Kubernetes fait le reste.
 
---

#### Les composants de Kubernetes

- Installer Kubernetes c’est installer un cluster de machines, chaque machine étant un nœud (*node*) 
pouvant exécuter des conteneurs. 

![logo](backgrounds/components-of-kubernetes.svg) 

---

#### Le pod

![logo](backgrounds/pod.png) <!-- .element height="50%" width="50%" -->

- Chaque nœud d’un cluster Kubernetes fait tourner des *pods*. 
- Le pod est l’élément de base d’un cluster Kubernetes.
  - En général, il exécute un seul conteneur 
  - Mais un pod peut héberger plusieurs conteneurs qu'il fait tourner comme un tout.
 

---

![logo](backgrounds/pod2.svg) 

- Chaque pod dispose d’une adresse réseau dans le cluster et d’un ou plusieurs 
volumes de stockage.

---

#### Le déploiement
- Le déploiement est un autre objet important de Kubernetes.
 
![logo](backgrounds/deploiement.jpg) <!-- .element height="30%" width="30%" -->

- Il permet de définir un pod ainsi que le nombre de ses répliques.
- Il s’occupe de maintenir la cohérence lors du déploiement de nouvelles versions.
---

### Déclaration d'un déploiement
- Kuberneters décrit ses objets avec des fichiers au format yaml que l'on appelle 
des **manifestes**.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: serveur-deploy
spec:
  replicas: 2
  selector:
    matchLabels:
      app: echoserver
  template:
    metadata:
      labels:
        app: echoserver
    spec:
      containers:
        - image: kicbase/echo-server:1.0
          name: echo-server
 ``` 

---

## Utilisation des fichiers yaml
- C’est la commande `kubectl` qui permet de gérer le cluster
- On demande à Kubernetes d’appliquer un manifeste avec : 
```shell
> kubectl apply -f deploiement.yaml
```
- On liste les déploiements créés avec :
```shell
> kubectl get deployment
NAME             READY   UP-TO-DATE   AVAILABLE   AGE    IMAGES                    
serveur-deploy   2/2     2            2           9m1s   kicbase/echo-server:1.0
```

- On liste les pods créés avec :
```shell
> kubectl get pod
NAME                              READY   STATUS    RESTARTS   AGE     IP         
serveur-deploy-7b4d94f4c5-clh2v   1/1     Running   0          8m38s   10.1.0.9   
serveur-deploy-7b4d94f4c5-wm7l8   1/1     Running   0          8m38s   10.1.0.8   
```

---
#### Cas concret: Déploiement d'un frontend web

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: frontend
  # Partie qui définit la composition du pod
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
      - name: frontend
        # adresse du registre du conteneur
        image: registry.gitlab.com/coursisis/private-devops-frontend
        ports:
        - containerPort: 8080
```

---

#### Configuration du déploiement dans le manifeste
- On peut préciser des variables d'environnements : 
 
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
        - name: frontend
          # adresse du registre du conteneur
          image: registry.gitlab.com/coursisis/private-devops-frontend
          ports:
            - containerPort: 8183
          env:
            - name: PORT
              value: "8183"
            - name: LOG_FILE
              value: "/tmp/random.log"
```
---

#### La ressource ConfigMap
- `ConfigMap` est un ressource spécialisée dans la configuration des pods 
- Il liste des associations clés-valeurs.
- Les valeurs peuvent être des chaines de caractères ou des fichiers complets.
- Les pods font référence aux clés pour accéder aux valeurs. 

---
#### Exemple de manifeste d'une ressource ConfigMap
 
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: frontend-conf
data:
  # association simple de la clé LOG_FILE avec la valeur indiquée
  LOG_FILE: /tmp/random.log
  # deuxième association clé : valeur
  PORT: 8183
  # association de la clé "default.conf" avec un contenu textuel
  default.conf: |
    server {
      listen       80;
      listen  [::]:80;
      server_name  localhost;


      location / {
          root   /usr/share/nginx/html;
          index  index.html index.htm;
          autoindex on;
          autoindex_format json;
      }
    }
```
---

#### Exemple d'utilisation d'une ConfigMap dans la spécification d'un pod

Cas des variables d'environnement
 
```yaml
apiVersion: apps/v1
kind: Deployment
...
    spec:
      containers:
        - name: frontend
          image: registry.gitlab.com/coursisis/private-devops-frontend
          ports:
            - containerPort: 8080
          env:
            - name: PORT
              valueFrom:
                configMapKeyRef:
                  name: config-frontend
                  key: PORT
            - name: LOG_FILE
              valueFrom:
                configMapKeyRef:
                  name: config-frontend
                  key: LOG_FILE
```
---
#### Exemple d'utilisation d'une ConfigMap dans la spécification d'un pod
Même chose en plus simple : 

```yaml
apiVersion: apps/v1
kind: Deployment
...
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
        - name: frontend
          image: registry.gitlab.com/coursisis/private-devops-frontend
          ports:
            - containerPort: 8080
          envFrom:
            - configMapRef:
                name: config-frontend
```
---
#### Exemple d'utilisation d'une ConfigMap dans la spécification d'un pod
Le cas du ConfigMap utilisé pour monter des fichiers 

```yaml
apiVersion: apps/v1
kind: Deployment
...
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
        - name: frontend
          image: registry.gitlab.com/coursisis/private-devops-frontend
          ports:
            - containerPort: 8080
          volumeMounts:
            - name: config-volume
              mountPath: /etc/nginx/conf.d
      volumes:
        - name: config-volume
          configMap:
            name: frontend-conf
```
---
#### Effet du manifeste précédent 
- Connexion dans le pod
```shell
kubectl exec -ti frontend-78f56d84c7-7fg82  sh
```

- Observation du contenu du dossier `/etc/nginx/conf.d`
```shell
> ls /etc/nginx/conf.d/
LOG_FILE      PORT          default.conf
```

- Que contiennent les fichiers ? 
```shell
> cat /etc/nginx/conf.d/LOG_FILE
/tmp/random.log
> cat /etc/nginx/conf.d/default.conf
server {
  listen       80;
  listen  [::]:80;
  server_name  localhost;
  ...
}
```
---
#### Spéficiation des informations sensibles 
- Quand une configuration contient une information sensible (typiquement un mot de passe ou un clé secrète), il ne
faut pas la mettre dans un `ConfigMap`.
- C'est la ressource  `Secret` qui doit être utilisée pour cela 
- On crée une ressource `Secret` avec la commande `kubectl` en précisant les clé-valeurs que l'on veut y stocker.
 
- Création du secret `monsecret` qui contient un couple login/password dont les valeurs sont précisées
```shell
kubectl create secret generic monsecret --from-literal=login=admin --from-literal=password='$:Arez8'
```
---
#### Spécification des informations sensibles
- Le manifeste créé par la commande précédente est le suivant :
```yaml
apiVersion: v1
kind: Secret
data:
  login: YWRtaW4=
  password: JDpBcmV6OA==
metadata:
  name: monsecret
type: Opaque
```
- Attention les informations **ne sont pas cryptées**, elles sont simplement encodées en base64, donc facilement décodables.
- L'intérêt du `Secret` est de déporter dans une ressource spéciale les informations sensibles pour que cette ressource
puisse être gérée à part. 
- Une ressource `Secret` ne doit **jamais** sortir telle quelle du cluster kubernetes ! 
---

#### Utilisation d'un `Secret`
- Un `Secret` s'utillise comme un `ConfigMap`
  - Comme une valeur de variable d'environnement.
  - Comme un fichier monté dans le conteneur.

---

#### Exemple d'utilisation d'un `Secret` en tant que variable d'environnement
```yaml
apiVersion: apps/v1
kind: Deployment
...
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
        - name: frontend
          image: registry.gitlab.com/coursisis/private-devops-frontend
          ports:
            - containerPort: 8080
          env:
            - name: LOGIN
              valueFrom:
                secretKeyRef:
                  name: monsecret
                  key: login
            - name: PASSWORD
              valueFrom:
                secretKeyRef:
                  name: monsecret
                  key: password
```
---
#### Mise en réseau des déploiements
- Peut-joindre les pods de l’extérieur du cluster ? Oui et Non ! 

```shell
> curl 10.1.0.9
curl : Impossible de se connecter au serveur distant
```

- Le cluster dispose de son propre réseau local. Par défaut ce réseau n'est pas accessible mais on peut effectuer une
redirection de port provisoire pour qu'il le soit : 
 
```shell
kubectl port-forward pod-75f59d57f4-4nd6q 8080:8080
``` 

- Pour rendre un déploiement durablement visible de l’extérieur, il faut créer un objet de type **service**.

---

### Rôle du service

![logo](backgrounds/services.svg) <!-- .element height="100%" width="100%" -->

---

#### Déclaration d’un service
- Le service peut être d’un des trois types suivants : 

- **ClusterIP**: Valeur par défaut, le service n’est accessible que de l’intérieur du cluster
- **NodePort**: Un port externe du cluster sur la plage 30000-32767 est redirigé vers le service
- **LoadBalancer**: Un système tiers (par exemple votre hébergeur cloud) redirige une IP publique vers le service
---

#### Déclaration d’un service

- Le service est lié aux pods par le sélecteur.

- Exemple :
```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend-service
spec:
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 8080
  selector:
    app: frontend
```
---

#### Observation et test d’un service 

```shell
> kubectl get service
NAME                      TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
service-pour-echoserver   LoadBalancer   10.105.232.146   localhost     8080:32468/TCP   3s

> curl http://localhost:8080

StatusCode        : 200
StatusDescription : OK
Content           : Request served by serveur-deploy-767c9dc7bd-zvzsh
```

- Quand on contacte le service, il transmet la requête à un de ses pods. 
 
- Remarque : Ici, c’est *localhost* qui nous permet de joindre le service, mais dans un cas réel, ce sera l’IP publique obtenue par
le load balancer.

---

#### La ressource Ingress

- Il se trouve que doter chaque service de sa propre adresse IP publique est trop couteux et peu réaliste.
- La ressource `Ingress` permet de réaliser un routage au niveau HTTP et d'aiguiller la requête vers un service visible uniquement à l'intérieur du cluster (Cluster IP).
- Ce routage est en général basé sur le nom DNS de l'URL utilisé.
---

#### Vue générale de la ressource Ingress

![logo](backgrounds/ingress.svg) <!-- .element height="100%" width="100%" -->

---

#### Définition d'une ressource Ingress

- Ce manifeste définit un `Ingress` qui redirige toutes les requêtes externes `https://frontend.isis.fr` vers
le service `frontend-service`. 
- Le service n'a pas besoin d'avoir une IP publique et peut donc être de type Cluster IP. 

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: frontend-ingress
spec:
  rules:
    - host: frontend.isis.fr
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
               name: frontend-service
               port:
                 number: 80
```
---

#### Organisation du cluster kubernetes: les *namespaces*

- Une application déployée sur kubernetes est décrite par plusieurs manifestes (déploiement, services, ingress, configmap, etc.) qui utilisent
des sélecteurs et des labels.
- Comment s'assurer que ces noms sont uniques à l'échelle du cluster ? 
- La solution est de compartimenter le cluster en espaces séparés appelés des *namespaces*.
- On s'assurera ensuite que chaque application est déployée dans son propre *namespace*.
---

#### Création d'un namespace 

- Par un manifeste
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: monapplication
```
- Ou par la commande `kubectl`
```shell
kubectl create namespace monapplication
```   

---

#### Déploiement d'un  manifeste dans un *namespace*
- On spécifie le *namespace* au moment du déploiement : 
```shell
kubectl apply -f service.yaml --namespace=monapplication
```
- On peut aussi spécifier dans le manifeste, le  *namespace* dans lequel on veut le déployer. Exemple avec
un service : 

```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend
  namespace: monapplication
spec:
  ports:
    - name: http
      port: 80
      targetPort: 8080
  selector:
    app: frontend
```

---

#### Manipulation des ressources d’un namespace
- Par défaut la commande `kubectl` manipule les ressources du *namespace* `default`. 
- Sinon, on utilise son option `-n` pour désigner le *namespace* souhaité.

```shell
> kubectl get pod -n keycloak
NAME                        READY   STATUS    RESTARTS   AGE
keycloak-5c4f5d7877-chdrw   1/1     Running   0          2d20h
postgres-0                  1/1     Running   0          2d20h

> kubectl get all -n keycloak
NAME                            READY   STATUS    RESTARTS   AGE
pod/keycloak-5c4f5d7877-chdrw   1/1     Running   0          2d20h
pod/postgres-0                  1/1     Running   0          2d20h

NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/keycloak   NodePort    10.152.183.47   <none>        8080:31150/TCP   2d20h
service/postgres   ClusterIP   None            <none>        5432/TCP         2d20h

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/keycloak   1/1     1            1           2d20h
```

---
#### Références

- Guide de référence pour la commande `kubectl` :
    - https://kubernetes.io/docs/reference/kubectl/quick-reference/ 
  
- Guide d’utilisation du cluster kubernetes d’ISIS : 
  - https://isiscloud.gitlab.io/documentation/user
