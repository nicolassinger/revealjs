##### Docker

![logo](backgrounds/docker.png)  <!-- .element height="20%" width="20%" -->

Packager et déployer son code

*Nicolas Singer*
---
#### Les conteneurs Docker
- Les **conteneurs** sont des machines virtuelles ultralégères.
- Ils sont lancés à partir d'une **image** qui contient tout ce qu'il faut pour faire tourner une application.
- Les conteneurs s'exécutent partout où on peut installer **docker** 
  - Unix (les conteneurs sont natifs sous unix)
  - Windows
  - MacOS
- Ils sont un moyen de faire tourner vos applications de façon identique quelque soit l'environnement.
- **Une application tournant dans un conteneur n'a besoin de rien d'autre pour fonctionner !**
 
---

#### Comparaison 

![logo](backgrounds/dockerVsVM.svg) 

---

#### Les images pré-construites

- La plupart des applications *connues* disposent d'images officielles pré-construites.
- Elles sont disponibles sur un dépôt public appelé le *docker hub*.
- **Docker sait télécharger automatiquement ces images identifiées par leur *tag***
 
---

### Les tags d'une image docker
- Une image docker possède un identifiant appelé *tag*.
- Ces *tags* sont en général composés de deux parties, le nom de l'image et son numéro de version.
- Par exemple `mysql:latest` ou `python:3.12`
- Le nom de l'image peut être en deux partie avec le nom du créateur (ou l'organisation) et le nom de l'image, comme par exemple
  `isis/dpi:latest`

---

#### Le Docker Hub

![logo](backgrounds/dockerhub.jpg)

---


#### Construire une image de conteneur
- L'image des conteneurs se construit à partir d'un fichier *Dockerfile*.
- Ce fichier est composé d'instructions qui donnent les étapes à suivre pour créer l'image.
  - On part toujours d'une image pré-construite.
  - On y ajoute les programmes nécessaires.
  - On donne la commande à exécuter au moment du lancement du conteneur.
---

#### Le Dockerfile
- Exemple pour créer une image de conteneur pour un service node.js :
 
```Dockerfile
# on part d'une conteneur contenant déjà node.js
FROM node:18-alpine

# on copie dans le conteneur les fichiers de l'application
COPY index.js . 
COPY package.json .

# on installe les dépendances décrites dans package.json
RUN npm install

# on spécifie la commande à exécuter au lancement du conteneur 
CMD [ "node", "index.js" ]
 ```

---

### La commande `docker build`

- Elle permet de construire une image à partir d'un dichier `Dockerfile`
- On se place à l'endroit où se trouve le `Dockerfile` et on exécute:

> `docker build -t <imagetag> .`

---

### Le Dockerfile, bien plus qu'un fichier de configuration
- Le dockerfile est la spécification de la composition d'une image
- Cette spécification au format texte est portable
  - Vous pouvez l'envoyer à un autre développeur ou ingénieur système
  - Il pourra fabriquer la même image
  - Il pourra donc lancer la même application


---


#### Fonctionnement d'un conteneur
- Les conteneurs sont lancés à partir de leur image
- Ils sont légers car : 
  - ils utilisent et partagent entre eux les resources de la machine hôte.
  - ils ne contiennent que le strict nécessaire pour faire tourner une application.

- Ils fonctionnent en isolation. 
  - Un conteneur se comporte comme une machine autonome disposant de sa propre adresse réseau.
  - Un conteneur par défaut n'est pas accessible de l'extérieur.
---

#### Lancement d'un conteneur
- Un conteneur se lance à partir de son image identifiée par une étiquette.
  - Cette image peut-être construite sur place ou téléchargée à partir d'un dépôt. 
   
- Au lancement, on peut préciser :
  - Comment se fait la connexion réseau du conteneur (7 modes possibles !).
    - Par défaut le conteneur est caché derrière l'hôte (mode NAT).
  - Comment la machine hôte partage ses données avec le conteneur.
---

#### Lancement de conteneurs avec `docker run`

- Lancement d'un conteneur avec l'image officielle de influxdb :
 
```shell
> docker run influxdb:2.5

Unable to find image 'influxdb:latest' locally
latest: Pulling from library/influxdb
620af4e91dbf: Pull complete
fae29f309a72: Pull complete
28fca74d99b6: Pull complete
5f72f7239d05: Pull complete
Digest: sha256:0b8cee9d1704555d848d96b69f40c0e83ff16063015facbd0d07f7e09d64220d
Status: Downloaded newer image for influxdb:latest
ts=2022-12-10T14:07:02.042181Z lvl=info msg="Welcome to InfluxDB" log_level=info
ts=2022-12-10T14:07:02.352518Z lvl=info msg=Listening addr=:8086 port=8086
```

---

#### Paramètres de lancement 
- L'option `--name` permet d'assigner un nom au conteneur.
  - plus pratique pour le désigner par la suite.
- L'option `-d` permet de le lancer en toile de fond. 

```shell
> docker run --name influx -d  influxdb
```

---
#### Redirection des ports au lancement 
- L'option `-p` permet de rediriger un port de la machine hôte vers un port du conteneur. 
- Le conteneur étant par défaut caché derrière la machine hôte, c'est ce qui permet de contacter le conteneur. 
- Par exemple voici comment on lance influxdb pour pouvoir y accéder via l'adresse http://localhost:8086 :

```shell
> docker run -p 8086:8086 influxdb:2.5
```
---

#### Partage de dossiers avec le conteneur 
- L'option `-v` permet de *monter* des dossiers de la machine hôte dans le conteneur.
- Cela permet de partager des fichiers entre l'hôte et le conteneur.
- Par exemple si on veut que le dossier `params` de la machine hôte soit accessible dans le dossier `/etc/config` du
  conteneur on écrira :
 
```shell
> docker run -v /params:/etc/config influxdb:2.5
```

---

#### Gestion des conteneurs
- La commande `docker ps` permet d'obtenir la liste des conteneurs en cours d'exécution.
- La commande `docker ps -a` permet d'obtenir la liste de tous les conteneurs présents sur la machine.

```shell
> docker ps
CONTAINERID   IMAGE    COMMAND       CREATED          STATUS         PORTS     NAMES 
30d450070f41  influxdb "./influxd"   10 seconds ago   Up 2 seconds   8086/tcp  influx
```
---

#### Arrêt, redémarrage et effacement d'un conteneur 

```shell
# Arrêt du conteneur
> docker stop influx

# Démarrage du conteneur
> docker start influx

# Accès aux logs du conteneur 
> docker logs influx

# Arrêt et effacement du conteneur 
> docker stop influx ; docker rm influx

```
---

#### Volumes externes
- Quand on efface un conteneur, toutes ses données sont perdues (sauf celles qui étaient partagées avec l'hôte).
- On peut choisir de créer des volumes de stockage externes qui persistent après l'effacement du conteneur. 
- On *monte* ensuite ce volume dans le dossier dans lequel le conteneur stocke ses données.
- Par exemple, si influxdb stocke ses données dans `/var/data` : 
 
```shell
# création du voume externe 
> docker volume influxdata

# montage du volume dans un dossier du conteneur
> docker run -v influxdata:/var/data influxdb:2.5
```

---


#### Docker Compose
- Docker Compose est un outil qui permet de spécifier le lancement de plusieurs conteneurs en même temps. 
- Cette spécification se fait dans un fichier appelé 
   > `docker-compose.yaml`
- Ce fichier décrit aussi les paramètres de chaque lancement 
---

#### Un exemple de docker-compose.yaml
      
```yaml
services:
  influxdb:
    image: influxdb:2.5
    container_name: influxdb
    ports:
      - "8086:8086"
    volumes:
      - influxdata:/var/data

  telegraf:
    image: telegraf:1.24
    container_name: telagraf
    restart: always
    volumes:
      - ./telegraf:/etc/telegraf
     
volumes:
  influxdata:
```

---

#### Actions avec docker compose : 

```shell
# Lancement des conteneurs
> docker compose up 

# Arrêt des conteneurs
> docker compose stop

# Lancement des conteneurs en toile de fond
> docker compose up -d

# Arrêt et effacement des conteneurs
> docker compose down
```
---

#### Intérêt de docker compose

- C'est un formidable outil pour orchestrer le lancement d'un ensemble de programmes et de services.
- **C'est une spécification portable que vous pouvez envoyer à tous ceux qui souhaitent recréer le même
environnement**. 

---

#### Références

- Le Docker Hub 
  - https://hub.docker.com/search
  
- Documentation sur Docker Compose : 
  - https://docs.docker.com/compose/

- Vidéo caféinée sur les conteneurs en réseau (pour les spécialistes du réseau)
  - https://www.youtube.com/watch?v=bKFMS5C4CG0

