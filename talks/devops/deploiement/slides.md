##### Mise en production d'une application

![logo](backgrounds/webapp.jpg)  <!-- .element height="20%" width="20%" -->

Le cas d'une application web fullstack

*Nicolas Singer*
---
#### Composants d'une application webfullstack
- Le client écrit en HTML/CSS/Javascript
- Le serveur écrit dans un framework backend
- La base de données 
---

#### Le client

- Quel que soit le framework utilisé (angular, reactjs, vue.js, ...) le client est composé 
d'un ensemble de fichiers écrits en **HTML**, en **CSS**, et en **Javascript**
 
- Il est exécuté par un navigateur web
 
- Pour pouvoir être chargé dans le navigateur, il doit être hébergé quelque part, c'est-à-dire 
être accessible au bout d'un **URL** !  

> https://application.domaine.com

---
#### Le serveur
- Il peut être écrit dans plein de langages différents selon le framework utilisé : 
 
![logo](backgrounds/serverframeworks.png)  <!-- .element height="40%" width="40%" -->

- Son code s'exécute sur la machine serveur qui l'héberge
- Comme le navigateur-client doit le contacter, il doit lui aussi être hébergé avec un URL public ! 

---

#### La base de données

- Choisie en fonction des besoins parmi les propositions du marchés (Mysql, PostGres, SqlServer, Oracle, MongoDB, Redis, SQLite ...)
- Elle s'exécute en général *à proximité* du serveur
  - Elle n'est visible que du serveur 
  - Elle n'a donc pas d'URL public

---

#### Que faut-il pour déployer le client ? 

- Pour le client : 
   - Un ordinateur dont le port https (443) est accessible via une adresse IP publique
   - Un nom de domaine pointant vers cette adresse IP
   - Un serveur web donnant accès aux pages web du client.

- Le déploiement consiste à uploader la partie client du projet sur cette machine et à la placer 
dans un dossier de son serveur web.
- Parmi les principaux serveurs web du marché on trouve : 
  - Apache
  - Nginx
  - Lighttpd

---
#### Que faut-il pour déployer le serveur ?

- Pour le serveur : 
  - Un ordinateur dont le port https (443) est accessible via une adresse IP publique
  - Un nom de domaine pointant vers cette adresse IP qui sera utilisé par le client.
  
- Le déploiement consiste à installer sur le serveur: 
  - les outils nécessaires au framework du projet, par exemple :  
    - *node.js* si projet node.js
    - *java* si projet Spring Boot
    - *Tomcat* si projet J2EE
    - ...
  - le code du projet, compilé et empaqueté pour la production
---
  
#### Que faut-il pour déployer la base de données ?

- Pour la base de données :
  - Un ordinateur accessible par le serveur pour héberger la base de données.
  
- Le déploiement consiste à installer sur la machine la base de données en question et à 
la paramétrer comme on le désire 
---

#### La révolution docker

Et si on pouvait lier le code avec l'environnement nécessaire à son fonctionnement ?  

- Sans docker
  - On installe ce qu'il faut sur les ordinateurs hébergeurs, et on uploade le code

- Avec docker : 
  - On crée un paquet contenant le code et tout ce dont il a besoin pour fonctionner, et on uploade ce paquet

> Avec Docker l'installation est définie par le code !
  
---
#### Docker en quelques mots

  <img class="r-stretch" src="backgrounds/docker.png">

- La technologie Docker permet de créer et d'exécuter des conteneurs qui sont des mini-machines virtuelles 
contenant tout ce qu'il faut pour exécuter un service ou un programme. 

- Ces mini-machines sont définies par un fichier (le Dockerfile) qui précise tous leurs composants

- Elles sont portables et peuvent s'exécuter n'importe où dès l'instant ou le *runtime* docker est installé.

---
#### Le déploiement fullstack avec docker 

- Phase de *build* des conteneurs : 
  - Un conteneur client
  - Un conteneur serveur
  - Un conteneur base de données

- Phase de *push* des conteneurs sur un dépot 

- Phase de *pull* et de *run* de ces conteneurs sur les machines

---
#### Spécification du build 

Exemple : le Dockerfile du serveur

```dockerfile
FROM node:16

# Création du dossier de l'application
WORKDIR /usr/src/app

# Installion des dépendances 
COPY ../../../package.json .
RUN npm install

# Copie des fichiers du projet dans le workdir
COPY . .

EXPOSE 4000
CMD [ "node", "index.js" ]
```

---

#### Automatisation du build et du push

- Les outils de CI (Continuous integration) permettent l'automatisation de 
la construction des conteneurs docker, et leur placement dans un dépôt.
- Exemple avec Gitlab : 

```yaml
image: docker:latest
services:
  - docker:18.09-dind
  
stages:
  - build
  - package

build:
  image: node:12-alpine
  stage: build
  script:
   - npm i
   - ng build --prod --base-href /flux/ 

package:
  stage: package
  script:
  - docker build -t $CI_REGISTRY_IMAGE .
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - docker push $CI_REGISTRY_IMAGE
```
---

#### Phase de déploiement 
- Sur une plateforme non managée, avec un pull suivi de l'adresse du dépôt
  - `docker pull registry.gitlab.com/projet/client`
- Sur une plateforme proposant des instances managée (AWS, Azure, Heroku, ...) via la console
d'administration.
 
  ![logo](backgrounds/azure.png) 
---

#### Phase de déploiement avec kubernetes 
![logo](backgrounds/kube.png)
- Orchestration des conteneurs
- Créations et répartition des conteneurs sur plusieurs machines en fonction des besoins
- load balancing
- Serverless services (knative)
