![img.png](backgrounds/img.png)  <!-- .element height="60%" width="60%" -->


*Nicolas Singer*
---

#### GIT = Distributed Gestion Control System 
- Un outil de gestion distribuée de versions.
- Réalisé en 2005 par Linus Torvald (le créateur de Linux).
- Gratuit et *open-source*.

---

#### L'historique des changements

- Git conserve un historique **local** de l'évolution des fichiers
- Pour cela il permet d'historiser (*commit*) l'état d'un système de fichiers.
- Il permet ensuite de naviguer parmi les états mémorisés
 
![img.png](backgrounds/git1.png)

---

### Intérêt

- Permettre de suivre l'évolution d'un projet
  - En effet les états mémorisés font apparaître l'évolution incrémentale des versions
- Pouvoir revenir en arrière 
  - en cas d'effacement accidentel 
  - reprendre le développement à partir d'un état antérieur

---

#### Les branches
- GIT permet le développement de branches d'évolutions parallèles.

![img.png](backgrounds/branches.png)

---

#### Intérêt

- Pouvoir explorer un nouveau développement (**feature**) ou régler un bug (**bugfix**) en préservant le 
développement principal.
  - On crée alors une nouvelle branche qui servira au nouveau développement.
- Pouvoir fusionner deux branches.
  - On intègre dans une branche, les changements effectuées dans une autre (*merge*).

---

#### Les dépôts distants

- Git permet de synchroniser son dépôt local avec un (ou plusieurs) dépôts distants.
 
![img.png](backgrounds/depots.png)

---

#### Intérêt

- Réaliser une sauvegarde de son projet
- Permettre un travail commun sur des fichiers en synchronisant les modifications de différents développeurs.

![socialcoding.png](backgrounds/socialcoding.png)

---

#### Commencer à travailler avec Git 

- A partir d'un nouveau projet: 
```shell
git init
```
  - Initialise un nouveau dépôt local avec une branche principale (`main`).
  - Crée un dossier caché `.git` qui contient ce dépôt.
  - En général, on crée dans la foulée un fichier `.gitignore` qui liste les fichiers et dossiers dont on ne veut pas 
    suivre les versions.
 
- ou à partir d'un dépôt distant 
```shell
git clone https://url_du_dépôt
```
  - Crée un nouveau dossier contenant une copie du dépôt distant 

---

#### Suivre son projet avec Git

- Avant chaque *commit*, il faut ajouter les fichiers à suivre. Souvent on ajoute tous les fichiers avec : 
```shell
git add -A
```

- Quand on veut prendre une photo de l'état de son projet, on réalise un *commit* avec : 
```shell
git commit -m "commentaire sur le nouveau commit"
```

- On peut obtenir l'historique des *commit* avec la commande : 
```shell
git log
```

---

#### Explorer l'état local 

- `git status` est une commande trés utile pour obtenir de l'aide et savoir où on en est : 
- Exemple : 

```shell
> git status
Sur la branche master
Votre branche est à jour avec 'origin/master'.

Fichiers non suivis:
(utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)
../../devops/git/

aucune modification ajoutée à la validation mais des fichiers non suivis sont présents 
(utilisez "git add" pour les suivre)
```
---
#### Explorer l'état local (suite de l'exemple)

```shell
> git add -A 

> git status
Sur la branche master
Votre branche est à jour avec 'origin/master'.

Modifications qui seront validées :
  (utilisez "git restore --staged <fichier>..." pour désindexer)
        nouveau fichier : ../../devops/git/index.html
        nouveau fichier : ../../devops/git/slides.md

> git commit -m "Ajout du cours sur GIT"

> git status
Sur la branche master
Votre branche est en avance sur 'origin/master' de 2 commits.
  (utilisez "git push" pour publier vos commits locaux)

rien à valider, la copie de travail est propre
```

---

#### Explorer l'état local 

- `git diff` permet de liste les différences entre deux *commit*. 
- La notation `HEAD` désigne le *commit* le plus récent, `HEAD~1` le précédent, `HEAD~2` celui encore avant, et ainsi de suite...

- Exemple, donne-moi les changements entre la dernier *commit* et le précédent : 
```shell
> git diff HEAD HEAD~1
```

- Autre exemple, donne-moi les changements entre le projet en cours et le dernier *commit* : 
```shell
> git diff 
```

- Le résultat de la commande `git diff` est plus facile à visionner dans un IDE   

---

#### Travailler avec un dépôt distant

-  Lister les dépôts distants avec lesquels le projet est synchronisé : 
```shell
git remote -v
```
- Ajouter un dépot distant : 
```shell
git remote add NOMDEPOT URL
```
- pousser une branche vers un dépôt distant :
```shell
git push NOMDEPOT NOMBRANCHE
```
- Récupère, mais n'intègre pas les modifications situées sur le dépôt distant :
```shell
git fetch NOMDEPOT
```
- Récupère et intègre les modifications situées sur le dépôt distant :
```shell
git pull NOMDEPOT
```

---

#### Exemples avec un dépôt distant
```shell
> git remote -v
origin  git@gitlab.com:nicolassinger/revealjs.git (fetch)
origin  git@gitlab.com:nicolassinger/revealjs.git (push)

> git remote remove origin
> git remote -v

> git remote add origin git@gitlab.com:nicolassinger/revealjs.git 
> git remote -v
origin  git@gitlab.com:nicolassinger/revealjs.git (fetch)
origin  git@gitlab.com:nicolassinger/revealjs.git (push)

> git push origin master 
Énumération des objets: 25, fait.
Décompte des objets: 100% (25/25), fait.
Compression par delta en utilisant jusqu'à 32 fils d'exécution
Compression des objets: 100% (21/21), fait.
Écriture des objets: 100% (22/22), 1.03 Mio | 95.71 Mio/s, fait.
Total 22 (delta 8), réutilisés 2 (delta 0), réutilisés du pack 0
To gitlab.com:nicolassinger/revealjs.git
   e55158f..e1ab2e5  master -> master

> git pull origin master
Depuis gitlab.com:nicolassinger/revealjs
 * branch            master     -> FETCH_HEAD
Déjà à jour.
```

---

#### Travailler avec les branches
- Créer une nouvelle branche (sans permuter dessus) :
```shell
git branch BRANCHE
```
- Se placer sur une branche : 
```shell
git checkout BRANCHE
```
- Fusionner la branche désignée dans la branche courante : 
```shell
git merge BRANCHE
```
---

#### Comprendre les branches
- On part d'une branche principale composée de 3 *commits* 
![img.png](backgrounds/branch1.png)
- On crée une nouvelle branche *fonct1* pour implémenter une nouvelle fonctionnalité et on commute dessus : 

<div class="twocolumn">
<div>

```shell
git branch fonct1
git checkout fonct1
```
</div>
<div>

![img.png](backgrounds/branch2.png)
</div>
</div>

---

#### Comprendre les branches

- Sur la nouvelle branche, on modifie `index.html` et on réalise un *commit*.

<div class="twocolumn">
<div>

```shell
git add -A branch fonct1
git commit -m "Ajout d'un pied de page"
```
</div>
<div>

![img.png](backgrounds/branch3.png)
</div>
</div>

---
#### Comprendre les branches

- On a détecté entre-temps un problème sur le `index.html` de la branche master. 
- On recommute dessus et on crée une branche `hotfix` pour corriger le fichier.
 
<div class="twocolumn">
<div>

```shell
git checkout master
git branch hotfix
git checkout hotfix
...
git add -A
git commit -m "Correction bug dans index.html"
```
</div>
<div>

![img.png](backgrounds/branch4.png)
</div>
</div>
---
#### Comprendre les branches

- On est satisfait du résultat et on fusionne la branche `hotfix` dans la branche principale 
 
  <div class="twocolumn">
  <div>

  ```shell
  git checkout master
  git merge hotfix
  ```
  </div>
  <div>

  ![img.png](backgrounds/branch5.png)  <!-- .element height="60%" width="60%" -->
  </div>
  </div>

- Et on supprime la branche `hotfix`

<div class="twocolumn">
<div>

```shell
git branch -d hotfix
```
</div>
<div>

![img.png](backgrounds/branch6.png) <!-- .element height="60%" width="60%" -->
</div>
</div>
---

#### Comprendre les branches

- On se remet au travail sur la branch *fonct1* et on soumet un nouveau *commit*.

  <div class="twocolumn">
  <div>

  ```shell
  git checkout fonct1
  ...
  git add -A
  git commit -m "Pied de page terminé"
  ```
  </div>
  <div>

  ![img.png](backgrounds/branch7.png)  <!-- .element height="70%" width="70%" -->
  </div>
  </div>

- Et on rapatrie le travail dans la branche principale

  <div class="twocolumn">
  <div>

  ```shell
  git checkout master
  git merge fonct1
  ```
  </div>
  <div>

  ![img.png](backgrounds/branch8.png) <!-- .element height="70%" width="70%" -->
  </div>
  </div>
---
 




