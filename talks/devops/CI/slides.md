##### Déployer une application informatique

![logo](backgrounds/CI.webp) <!-- .element height="60%" width="60%" -->

*Nicolas Singer*
---

#### Les différents types d'applications
- Les applications clients lourds (Desktop)
- Les applications mobiles
- Les applications serveurs
- Les applications web

---

#### Les applications Desktop
- **Elles s'exécutent sur les postes de leurs utilisateurs.**
   
- Leur déploiement consiste à mettre à disposition un exécutable ou un script d'intallation puis à l'installer localement.
- On peut aussi passer par un magasin d'application (Windows Store, Mac App Store, Dépots Linux, ...)

- Ex: Word, Photoshop, un player multimédia, ...
 
  ![logo](backgrounds/word.png) <!-- .element height="26%" width="26%" -->
  ![logo](backgrounds/photoshop.png) <!-- .element height="20%" width="20%" -->
  ![logo](backgrounds/vlc.jpg) <!-- .element height="30%" width="30%" -->

---

#### Les applications mobiles
- **Elles s'exécutent aussi sur les appareils de leurs utilisateurs.**

- Mais peuvent avoir besoin d'une partie backend pour fonctionner. 

- Leur déploiement consiste à uploader dans un store l'application Android ou iOS.

- Ex: WhatsApp, un jeu vidéo, google map mobile ...

  ![logo](backgrounds/whatsapp.png) <!-- .element height="35%" width="35%" -->
  ![logo](backgrounds/genshin.jpeg) <!-- .element height="30%" width="30%" -->
  ![logo](backgrounds/googlemap.png) <!-- .element height="15%" width="15%" -->

---

#### Les applications serveurs
- **Elles s'exécutent sur des serveurs distants**

- Leur déploiement consiste à uploader l'application sur un ou plusieurs serveurs distants.

- Ex: Une base de données, un serveur multimédia, un partage de fichier, un serveur de sauvegarde...

  ![logo](backgrounds/mysql.png) <!-- .element height="20%" width="20%" -->
  ![logo](backgrounds/plex.jpg) <!-- .element height="25%" width="25%" -->
  ![logo](backgrounds/samba.jpg) <!-- .element height="40%" width="40%" -->

---

#### Les applications web (1/2)
- **Elles s'exécutent dans un navigateur web**

- La plupart du temps, une application web (tout comme une application mobile) fait appel à du code situé sur un serveur distant. 
- Par conséquent, quand on parle d'application web, on désigne l'ensemble des composants qui permettent à l'application de fonctionner: 
  - La partie ***frontend*** est la partie qui est exécuté par le navigateur
  - la partie ***backend*** est l'ensemble du code exécuté par des serveurs distants (serveur d'applications, base de donnée, web service, ...)

---

#### Les applications web (2/2)
- Déployer une application web consiste donc à  :
  - Placer sur un serveur public (au bout d'un URL) la partie frontend pour qu'un utilisateur puisse charger cet URL dans son navigateur.
  - Déployer la partie backend sur un ou plusieurs serveurs.
---

#### Historique des applications web : La genèse
- Acte I (1990) : Apparition du protocole HTTP et des liens hypertextes.
  ![logo](backgrounds/acteI.svg)

---
 
#### Evolution du web *coté serveur* : Langages coté serveur (PHP, J2EE, ASP.NET,...)
- Acte II (1992) : le HTML calculé par le serveur
- Le serveur web calcule la page web au moment ou le client la demande. C'est l'apparition de l'application web.
    ![logo](backgrounds/acteII.svg)
---

#### Evolution du web *coté client* : Javascript
- Acte III (1995) : Javascript 
- Le page web contient un programme qui s'exécute dans le navigateur
 ![logo](backgrounds/acteIII.svg)

---
 
#### Dernière évolution : La single page application (SPA)
- Apparition des frameworks ***frontend*** (Angular, React, Vue.js) et des services Web (2002)
- L'application web est écrite pour être autonome : 
  - Aucun téléchargement de pages supplémentaires
  - Elle va chercher ses données sous la forme de données structurées au format JSON ou XML en contactant des **Web Services**
---

#### Architecture d'une application web SPA
![logo](backgrounds/dernieracte.svg)

---

#### Comment déployer une application web

- Déployer le frontend
- Déployer le backend en général composé :
  - d'un service web
  - d'une base de donnée

  ---


#### Schéma général
![logo](backgrounds/archiweb.svg)

---

#### Le frontend

- Quel que soit le framework utilisé (angular, reactjs, vue.js, ...) le *frontend* est composé
  d'un ensemble de fichiers écrits en **HTML**, en **CSS**, et en **Javascript**

- Il est exécuté par un navigateur web

- Pour pouvoir être chargé dans le navigateur, il doit être hébergé quelque part, c'est-à-dire
  être accessible au bout d'un **URL** !

> https://application.domaine.com

---
#### Le serveur
- Il peut être écrit dans plein de langages différents selon le framework utilisé :

![logo](backgrounds/serverframeworks.png)  <!-- .element height="40%" width="40%" -->

- Son code s'exécute sur la machine serveur qui l'héberge
- Comme le navigateur-client doit le contacter, il doit lui aussi être hébergé avec un URL public !

---

#### La base de données

- Choisie en fonction des besoins parmi les propositions du marchés (Mysql, PostGres, SqlServer, Oracle, MongoDB, Redis, SQLite ...)
- Elle s'exécute en général *à proximité* du serveur
  - Elle n'est visible que du serveur
  - Elle n'a donc pas d'URL public

---

#### Que faut-il pour déployer le client ?

- Pour le client :
  - Un ordinateur dont le port https (443) est accessible via une adresse IP publique
  - Un nom de domaine pointant vers cette adresse IP
  - Un serveur web donnant accès aux pages web du client.

- Le déploiement consiste à uploader la partie client du projet sur cette machine et à la placer
  dans un dossier de son serveur web.
- Parmi les principaux serveurs web du marché on trouve :
  - Apache
  - Nginx
  - Lighttpd

---
#### Que faut-il pour déployer le serveur ?

- Pour le serveur :
  - Un ordinateur dont le port https (443) est accessible via une adresse IP publique
  - Un nom de domaine pointant vers cette adresse IP qui sera utilisé par le client.

- Le déploiement consiste à installer sur le serveur:
  - les outils nécessaires au framework du projet, par exemple :
    - *node.js* si projet node.js
    - *java* si projet Spring Boot
    - *Tomcat* si projet J2EE
    - ...
  - le code du projet, compilé et empaqueté pour la production
---

#### Que faut-il pour déployer la base de données ?

- Pour la base de données :
  - Un ordinateur accessible par le serveur pour héberger la base de données.

- Le déploiement consiste à installer sur la machine la base de données en question et à
  la paramétrer comme on le désire
---

#### La révolution docker

Et si on pouvait lier le code avec l'environnement nécessaire à son fonctionnement ?

- Sans docker
  - On installe ce qu'il faut sur les ordinateurs hébergeurs, et on uploade le code

- Avec docker :
  - On crée un paquet contenant le code et tout ce dont il a besoin pour fonctionner, et on uploade ce paquet

> Avec Docker l'installation est définie par le code !
