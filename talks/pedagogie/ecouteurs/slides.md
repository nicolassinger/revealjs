##### Etudiants et écouteurs : Pour ou contre ? 

![img.png](img.png)

### L'usage des écouteurs semble s'intensifier 
- De plus en plus d'étudiants portent des écouteurs (ou des casques)
- Ces dernières années ont vu l'explosion des écouteurs sans fils
  - Ces derniers sont discrets et donc difficile à repérer
- L'usage que font les étudiants de ces écouteurs, pose question 
---

#### Les différents types d'écouteurs : Les intra-auriculaires

- L'écouteur pénètre dans le creux de l'oreille
- bonne isolation acoustique

- ex: les Airpods d'Apple 

<div class="twocolumn">
<div>

![img_1.png](img_1.png)  <!-- .element width="60%" -->
</div>
<div>

![img_2.png](img_2.png)  <!-- .element width="60%" -->
</div>
</div>

- modèles pro avec réduction des bruits adaptatif qui permet de ne pas se couper
complètement du monde extérieur.
---
 
#### Les différents types d'écouteurs : Les intra-auriculaires

- Samsung, Bose, Jabra, Google, Senheiser, Sony, etc.
 
<div class="twocolumn">
<div>

![img_5.png](img_5.png) <!-- .element width="30%" -->
</div>
<div>

![img_3.png](img_3.png) <!-- .element width="50%" -->
</div> 
<div>

![img_4.png](img_4.png) <!-- .element width="50%" -->
</div>
<div>

![img_6.png](img_6.png) <!-- .element width="90%" -->
</div>
</div>

---

#### Les différents types d'écouteurs : Le casque supra-auriculaire
- l'écouteur repose sur l'oreille
- Isole moins des bruits extérieurs
- Moins onéreux


<div class="twocolumn">
<div>

![img_8.png](img_8.png) <!-- .element width="100%" -->
</div>
<div>

<div>

![img_9.png](img_9.png) <!-- .element width="100%" -->
</div>
</div>

---

#### Les différents types d'écouteurs : Le casque circum-auriculaire
- l'écouteur englobe l'oreille avec le coussinet sur le crane.
- Peut isoler en fonction de la caractéristique ouverte ou fermée de l'écouteur
- volumineux
- Plutôt pour un usage sédentaire


![img_10.png](img_10.png) <!-- .element width="40%" -->

---

#### Pourquoi porter un écouteur quand on est étudiant ? 
- discussions avec les FIE4
  - Regarder des vidéos ou écouter des podcasts sans déranger les autres
  - S'isoler du monde extérieur. 
    - Même sans sons (avec mode réduction du bruit)
  - Se concentrer sur une tâche 
    - avec de la musique
  - Faire autre chose en même temps 
    - podcast

---
#### Les étudiants se sentent-ils autoriser à les utiliser en cours ? 
- Oui en situation de travail individuel 
- Non quand l'enseignant s'adresse à eux

![img_11.png](img_11.png)

---
#### Qu'observe-t-on en cours ? 
- Certains étudiants portent leurs écouteurs en permanence
  - Ils ne les enlèvent pas en entrant en cours
  - Ils ne demandent pas la permission de les porter
    - Ont-ils oublié de les enlever ? 
  - Impossible de savoir s'ils s'en servent ou si c'est juste un accessoire de mode. 
  - Certains portent un écouteur sur deux

- Un volume trop fort peut laisser filtrer du son
---

#### Qu'en pensent les enseignants et le personnel de l'école&nbsp;? 
- Pas de consignes de la part de la direction des études 
- Rien dans le règlement intérieur
- Autoriser ou interdire ? Dans quelles conditions ? 
- Utiliser les écouteurs dans le cadre des cours ? 
  - Cas particulier des étudiants souffrant de certains problèmes médicaux ? 
- Quid des examens ? 

---
 
#### Enquête sur le net sur l'avis d'enseignants (1/2)
- Porter des écouteurs quand l'enseignant parle est un manque de respect
- Ce n'est pas le problème de l'enseignant si l'étudiant n'écoute pas tant qu'il ne perturbe pas la classe.
  - formations où la présence n'est pas obligatoire
- Que dirait l'étudiant si l'enseignant faisait son cours avec un casque sur la tête ? 
- Se concentrer en se coupant du monde extérieur est-il un signe d'inaptitude au travail collectif ? 
- Interdiction des écouteurs pendant les TP où il y a des enjeux de sécurité (produits dangereux, électricité,...)

---
#### Enquête sur le net sur l'avis d'enseignants (2/2)
- On peut s'ennuyer et laisser ses pensées divaguer quand son cerveau ne travaille pas à 100%. Pouvoir travailler et écouter autre chose en même temps
peut aider à rester concentré.
- Certains règlements intérieurs interdisent les écouteurs



 



