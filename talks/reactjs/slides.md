##### ReactJS

![logo](backgrounds/logo-react-js.png)  <!-- .element height="20%" width="20%" -->

<https://reactjs.org>

Nicolas Singer
---
#### ReactJS est une bibliothèque Javascript  
- pour le développement web côté client (frontend)
- créé par Facebook en 2013
- repose sur des composants basés sur un état
- **permet donc de définir les vues d'une application web**
---
#### Approche composants
React permet de définir des composants qui affichent du code HTML 
<div class="twocolumn">
<div>

```javascript
function AppComposant() {
  return (
   <h4>React c'est cool</h4>
  );
}

ReactDOM.render(
    <AppComposant />,
    document.getElementById('root')
);
```
</div>
<div>

```html
<div id="root"></div>
```
<div class="webpage"> <h4>React c'est cool</h4> </div>
</div>
</div>
---

#### Approche composants
Ces composants peuvent s'imbriquer 
<div class="twocolumn">
<div>

```javascript
function AppComposant() {
  return (
          <div>
            <h1>React c'est cool</h1>
            <Bonjour/>
          </div>
  );
}

function Bonjour() {
  return (<div>Bonjour à tous</div>);
}
```
</div>
<div>
<div class="webpage">
<h4>React c'est cool</h4> 
<div>Bonjour</div>
</div>
</div>
</div>
---

#### Approche composants
Et donc on peut décomposer ses pages 
<div class="twocolumn">
<div>

```javascript
function AppComposant() {
  return (
          <div>
            <Entete/>
            <ListePatient/>
            <PiedDePage/>
          </div>
  );
}

function Entete() {
  return (<h4>Infirmerie</h4>);
}

function ListePatient() {
  return (<ul>
    <li>John Snow</li>
    <li>Catelyn Stark</li>
  </ul>)
}

function PiedDePage() {
    return (<p>&copy; Nicolas Singer</p>)
}
```
</div>
<div>
<div class="webpage">
<h4>Infirmerie</h4>
<ul><li>John Snow</li><li>Catelyn Stark</li></ul>
<p>© Nicolas Singer</p></div></div>
</div>
---

#### Le JSX
- Revenons en arrière sur une des fonctions précédentes :
```javascript
function Entete() {
    return (<h4>Infirmerie</h4>);
}
```

- De quelle nature est l'expression `<h4>Infirmerie</h4>` ? 
  - Ce n'est pas une chaine de caractère (pas de guillemets autour)
  - Ce n'est pas une expression javascript valide
  - **C'est du JSX !**

- Le JSX est une extension syntaxique propre à React qui est traduit en javascript lors
de la *compilation* des composants.
```javascript
function Entete() {
  return React.element('<h4>Infirmerie</h4>');
}
```
---

#### Le JSX
- Le JSX peut intégrer du code javascript 
```javascript
function Entete() {
    const titre = "Infirmerie"
    return (<h4>{titre}</h4>);
}
```
- Et comme le JSX est une expression on peut aussi s'en servir dans du code javascript:
```javascript
function Entete() {
    const titre = "Infirmerie"
    const titrevisible = false
    if (titrevisible)
        return (<h4>{titre}</h4>);
    else return (null)
}
```
- le fait de renvoyer null provoque l'absence d'affichage
---
#### Le JSX
- On peut utiliser des attributs 
```javascript
function Recherche() {
  const nomchamp = "nom de famille"
  return (<div>
      <label for={nomchamp}>Nom</label>
      <input name={nomchamp} className="input" type="text"/>
   </div>)
}
```
- Quand une expression javascript donne la valeur d'un attribut, on ne met pas de guillemets !
- Attention certains attributs ont un nom spécial en JSX. C'est le cas de `className` qui remplace `class`.
---
#### Listes en JSX
- Pour travailler sur les listes, on utilise la méthode `map` des collections : 
```javascript
function ListePatient() {
  const liste = [ { nom: "John Snow"}, { nom: "Catelyn Stark"}]
  return (<ul> {
        liste.map(elem => 
          <li>{elem.nom}</li>
      )}
      </ul>)
}
```
- Une bonne pratique est d'affecter à chaque élément html de liste, un attribut `key` distinct. 
 ```javascript 
function ListePatient() {
  const liste = [ { id: 1, nom: "John Snow"}, {id:2, nom: "Catelyn Stark"}]
  return (<ul> {
        liste.map(elem => 
          <li key={elem.id}>{elem.nom}</li>
      )}
      </ul>)
}
```
---
#### Les composants classes
- On peut aussi utiliser des classes pour définir les composants
- Ainsi le composant `Entete` peut s'écrire de deux façons :
 
<div class="twocolumn">
<div>

```javascript
function Entete() {
  const titre = "Infirmerie"
  const titrevisible = false
  if (titrevisible)
    return (<h4>{titre}</h4>);
  else return (null)
}
```
</div>
<div>

```javascript
class Entete extends React.Component {
  titre = "Infirmerie"
  titrevisible = true
 
  render() {
   if (this.titrevisible)
    return (<h4>{this.titre}</h4>);
   else return (null)
  }
}
```
</div>
</div>

- Avec une classe, c'est la méthode `render` qui doit définir le rendu du composant.
- On verra plus tard qu'au contraire des fonctions, les classes 
permettent de conserver un état.
---
#### Les propriétés 
- On peut passer des propriétés à un composant via les attributs
- la fonction (ou la classe) définissant ce composant reçoit ces propriétés en arguments
sous la forme d'un unique paramètre
 
```javascript
function AppComposant() {
    return (
      <Entete titre="Infirmerie" titrevisible="true"/>
    );
}

function Entete(props) {
  if (props.titrevisible)
    return (<h4>{props.titre}</h4>);
  else return (null)
}
```
Ce que l'on écrira plutôt :
```javascript
function Entete({ titre, titrevisible }) {
  if (titrevisible)
    return (<h4>{titre}</h4>);
  else return (null)
}
```
---
#### Les propriétés forment un état 
- Bien entendu l'état du composant dépend de ses propriétés (ce que react appelle les *props*). 
- Les propriétés passées en paramètre forment ce que l'on appelle un **état**
- Si la valeur d'une des propriétés change, le composant est mis à jour
 
```javascript
function AppComposant() {
    return (
      <Entete titre="Infirmerie" titrevisible="true"/>
      // si plus tard la propriété titrevisible change, le titre disparait...
      <Entete titre="Infirmerie" titrevisible="false"/>
    );
}

function Entete(props) {
  if (props.titrevisible)
    return (<h4>{props.titre}</h4>);
  else return (null)
}
```
---
#### L'état local
- L'état d'un composant dépend donc des propriétés qui lui sont transmises par le composant parent 
- Mais un composant peut aussi gérer des propriétés qui lui sont propres : c'est l'**état local**
  - Seul les composants définis par des classes peuvent avoir un état local
  - Un composant ne peut pas faire évoluer ses *props* (seul le parent le peut) mais peut faire évoluer
son état local.
- On définit l'état local dans le constructeur et on le met à jour avec `setState` :

<div class="twocolumn">
<div>

```javascript
constructor(props) {
    super(props)
    this.state =  { compteur : 0 }
}
```
</div><div>

```javascript
this.setState( { compteur: 1})
``` 
</div></div>

---
#### Exemple de gestion de l'état local
- On veut afficher un compteur incrémenté à chaque click sur un bouton

```javascript
class AppComposant extends React.Component {
  
  constructor(props) {
    super(props)
    this.state =  { compteur : 0 } 
  }
  
  compteurIncrement() {
    this.setState( { compteur: this.state.compteur + 1 })
  }

  render() {
    return(<div>
      <button onClick={ () => this.compteurIncrement()}>Click Me</button>
        <div>Valeur du compteur : {this.state.compteur}</div>
    </div>  
    )
  }
}
```
---
#### Exemple de gestion de l'état local
- Même exemple mais avec un composant qui affiche le compteur

```javascript
class AppComposant extends React.Component {
  constructor(props) {
    super(props)
    this.state =  { compteur : 0 }
  }

  compteurIncrement() {
    this.setState( { compteur: this.state.compteur + 1 })
  }

  render() {
    return(<div>
              <button onClick={ () => this.compteurIncrement()}>Click Me</button>
              <Compteur valeur= { this.state.compteur }/>
            </div>
    )
  }
}

function Compteur({ valeur }) {
  return(
          <div>Valeur du compteur : {valeur}</div>
  )
}
```
---
#### Exemple de gestion de l'état local
- Même chose mais avec le bouton dans le composant compteur

```javascript
class AppComposant extends React.Component {
  constructor(props) {
    super(props)
    this.state =  { compteur : 0 }
  }
  compteurIncrement() {
    this.setState( { compteur: this.state.compteur + 1 })
  }
  render() {
    return(<div>
              <Compteur valeur= { this.state.compteur }
                        fonctionIncrement={ this.compteurIncrement.bind(this) }/>
            </div>
    )
  }
}

function Compteur({ valeur, fonctionIncrement }) {
  return(
          <div>
            <button onClick={ () => fonctionIncrement()}>Click Me</button>
            <div>Valeur du compteur : {valeur}</div>
          </div>
  )
}
```
---
#### Résumé sur les états
- Deux types d'état en React
    - Les *props*: Propriétés passées en paramètre par le composant parent
      - non modifiables par le composant mais peuvent changer si le composant parent
les met à jour.
    - L'*état local* : Propriétés définies par le composant dans une variable de classe appelé *state*
      - Initialisées dans le constructeur
      - Mises à jour en appelant *setState(nouvellevaleur)*
- La modification d'un de ces états provoque un nouveau rendu du composant et donc sa mise à jour en fonction
des nouvelles valeurs
- React optimise ce nouveau rendu en ne mettant à jour que les parties HTML qui changent.
---
#### Les méthodes de cycle de vie
- Dans les composants à état local (les classes), il est important de pouvoir faire des choses lors de 
la première création du composant, et juste avant sa destruction 
  - Par opposition aux méthodes appelées dans la partir `render()` qui sont 
  exécutées à chaque fois que le composant est mis à jour
- Il existe des méthodes spéciales qui jouent ce rôle : 
 
```javascript
class AppComposant extends React.Component {
  constructor(props) {
    super(props)
    this.state =  { compteur : 0 } 
  }
  
  // exécutée une seule fois au début
  componentDidMount() {  }
  
  // exécutée une seule fois avant de quitter le composant
  componentWillUnmount() {  }
}
```
---
#### Les méthodes de cycle de vie
- Par exemple mettre en place l'appel régulier d'une fonction avec `setInterval()`et cesser l'appel
quand le composant n'est plus utilisé : 
 
```javascript
class AppComposant extends React.Component {

    constructor(props) {
        super(props)
        this.state =  { compteur : 0 }
    }

    componentDidMount() {
        this.interval = setInterval( () => 
            this.setState({ compteur : this.state.compteur + 1}), 1000)
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return(<div>Valeur du compteur: { this.state.compteur } </div>)
    }
}
```
---

#### Les méthodes de cycle de vie
- Autre exemple, aller chercher une donnée sur le web et 
l'affecter à l'état local.
 
<div class="twocolumn">
<div>

```javascript
class AppComposant extends React.Component {
  constructor(props) {
    super(props)
    this.state = { facts: [] }
  }
  componentDidMount() {
    axios.get(
        `https://cat-fact.herokuapp.com/facts`)
            .then(res => { this.setState( 
                { facts: res.data } 
            );
        })
  }
  render() {
    return(<div>Cat Facts:
      <ul>
        { this.state.facts.map( f =>
                <li>{ f.text } </li>)
        }
      </ul></div>)
  }
}
```
</div>
<div class="smaller">
Cat Facts:<ul><li>Wikipedia has a recording of a cat meowing, because why 
not? </li><li>When cats grimace, they are usually "taste-scenting." 
They have an extra organ that, with some breathing control, allows the 
cats to taste-sense the air. </li><li>Cats make more than 100 different 
sounds whereas dogs make around 10. </li></ul>
</div>
</div>

---

#### ou mieux

```javascript
class AppComposant extends React.Component {
  constructor(props) {
    super(props)
    this.state = { facts: [] }
  }
  componentDidMount() {
    axios.get(`https://cat-fact.herokuapp.com/facts`)
            .then(res => {
              this.setState( { facts: res.data } );
            })
  }
  render() {
    return(<ListFact facts={this.state.facts}/>)
  }
}

function ListFact({ facts }) {
  return(<div>Cat Facts:
    <ul>
      { facts.map( f =>
              <li key={f._id}>{ f.text } </li>)
      }
    </ul></div>)
}
```
---
####  Partager des états entre composant

- Nous avons vu qu'avec les *props*, un composant parent transmettait des données
à un composant enfant.
  - Comment faire la même chose mais de l'enfant vers le parent ? 
- Le parent définit une méthode qui sera appelée par l'enfant et lui passe 
via les *props*.


<div class="twocolumn">
<div>

```javascript
class AppComposant extends React.Component {
  constructor(props) {
    super(props)
    this.state = { facts: [], 
      selecttext: undefined }
  }
  render() {
    return(<div>
      <ListFact facts={this.state.facts} 
          onSelect={(text) => this.setState(
              { selecttext: text})
          }/>
      
        { this.state.selecttext && <div>
        Mon enfant a sélectionné le texte :
          { this.state.selecttext} </div> }
    </div>)
  }
}
```
</div><div>

```javascript
function ListFact({ facts, onSelect }) {
  return(<div>Cat Facts:
    <ul>
      { facts.map( f =>
        <li onClick={() => onSelect(f.text)} 
            key={f._id} >{ f.text } </li>)
      }
    </ul></div>)
}
```
</div>
</div>

---
### La prop children
- Cette propriété donne accès au JSX de l'élément défini par le parent.

<div class="twocolumn">
<div>

```javascript
class AppComposant extends React.Component {
  render() {
    return(<div>
      <ListFact facts={this.state.facts} 
          onSelect={(text) => this.setState(
          { selecttext: text})}>
        <AffTexteSiPasNull 
            texte={this.state.selecttext}>
        </AffTexteSiPasNull>
      </ListFact>
    </div>
    )
  }
}

function AffTexteSiPasNull( { texte } ) {
  return( { texte } && <div> 
        Mon enfant a sélectionné le texte :  
        { texte }  </div>)                  
}
```
</div>
<div>

```javascript
function ListFact({ facts, onSelect, children })
{
    return(<div>Cat Facts: <ul>
        { facts.map( f =>
           <li onClick={() => onSelect(f.text)} 
                    key={f._id}>
            { f.text } </li>)
        }
        </ul>
        { children }
        </div>
    )
}
```
</div>
</div>

---
#### Les hooks
- Les hooks sont une nouveauté apparue dans React 16 qui permet de doter les fonctions d'états comme on peut le faire
dans les classes.
- Il existe plusieurs types de hooks (hooks d'états, hooks d'effet, ...).
- Les hooks ne fonctionnent que dans les fonctions (et pas dans les classes).
- Simplifie la gestion des états (les fonctions sont plus légères à utiliser les classes).
- Mais au prix de certains effets parfois indésirables quand on ne maitrise pas les fermetures lexicales de Javascript.

---
#### Le hook d'état
- Dans une classe, on déclare l'état dans le constructeur, et on le met à jour avec `setState`
 
<div class="twocolumn">
<div>

```javascript
constructor(props) {
    super(props)
    this.state =  { compteur : 0 }
}
```
</div><div>

```javascript
this.setState( { compteur: 1})
``` 
</div></div>

- Avec le hook d'état, on fait ça plus simplement : 

```javascript
function Example() {
  const [compteur, setCompteur] = useState(0);
  
  // lire l'état
  console.log(compteur);

  // modifier l'état
  setCompteur(1);
```

---
#### Le compteur revisité avec le hook d'état
```javascript
function AppComposant {
  const [compteur, setCompteur] = useState(0);
  
  compteurIncrement() {
    setCompteur(compteur.value + 1) 
  }

    return(<div>
      <button onClick={ () => compteurIncrement()}>Click Me</button>
        <div>Valeur du compteur : {compteur}</div>
    </div>)
}
```

---
#### Ce qu'il faut savoir sur le hook d'état
- Le hook garde sa valeur quand le composant est remis à jour, y compris quand cette mise à jour est déclenchée 
par le composant parent
- Pour que le composant soit redessiné, il faut que la nouvelle valeur soit différente de l'ancienne. 
  - Attention donc quand on un hook est une référence à un objet complexe. Si seule les propriétés de l'objet changent, 
mais que l'objet en lui même ne change pas, il n'y aura pas de mise à jour graphique du composant. 
---
#### Les hooks d'effet
- Les hooks d'effet permettent aux fonctions d'exécuter des effets de bord, comme les méthodes ` componentDidMount()` et
  `componentWillUnMount()` des classes.
- On les déclare avec `userEffect(fonction, [dependances])`
- La fonction passée en paramètre est alors exécutée au montage du composant et à chaque mise à jour d'une des
dépendances.
---
#### Les hooks d'effet

- Exemple : 
<div class="large">

```javascript
function AppComposant {
  const [compteur, setCompteur] = useState(0);

  compteurIncrement() {
    setCompteur(compteur + 1)
  }
  
  useEffect(() => {
    console.log(`Le compteur vaut: { compteur.value }`)
  }

  return(<div>
    <button onClick={ () => compteurIncrement()}>Click Me</button>
    <div>Valeur du compteur : {compteur}</div>
  </div> )
}
```
</div>

---
#### Les hooks d'effet

- Si on veut que l'effet ne soit exécuté qu'au montage du composant, on passe un tableau vide en deuxième paramètre.
- Typiquement si l'effet consiste à appeler une API web service, et qu'on ne veut pas le faire à chaque mise à jour 
du composant : 
<div class="large">

```javascript
function AppComposant {
  const [facts, setFacts] = useState([]);

  useEffect(() => {
      axios.get(`https://cat-fact.herokuapp.com/facts`)
              .then(res => { setFacts(res.data) })
    }, [])
    
 return(<ListFact facts={this.state.facts}/>)
}
```
</div>

---
#### Les hooks d'effet <!-- .slide: data-visibility="hidden" -->
- Attention, la fermeture lexicale de Javascript peut jouer des tours dans les hooks d'effet ! 
- Imaginons qu'au démarrage du composant, on veuille lancer une méthode qui s'exécute toutes les secondes : 
<div class="large">

```javascript
function AppComposant {

  useEffect(() => {
      setInterval(() => console.log("Bip ! "), 1000)
    }, [])
    
 return(null)
}
```
- Pas de problème, ça fonctionne et vous verrez bien marqué *Bip* dans la console toutes les secondes.
</div>

---
#### Les hooks d'effet <!-- .slide: data-visibility="hidden" -->
- Maintenant, je veux que toutes les secondes l'état de mon composant soit mis à jour :
<div class="large">

```javascript
function AppComposant {
  const [compteur, setCompteur] = useState(0);

  useEffect(() => {
      setInterval(() => setCompteur(compteur + 1) , 1000)
    }, [])
    
 return(<div>  { compteur } </div>)
}
```
</div>

- Le compteur n'évolue pas et reste bloqué sur 1 ! 
- C'est à cause de la fermeture lexicale de javascript 

---

#### Le hook de référence 
- `useRef` renvoie un objet modifiable avec une propriété `current` initialisée avec l'argument fourni. 
- L'objet persiste pendant toute la durée de vie du composant 
  - Il n'est donc pas réinitialisé lors de la mise à jour du composant 

```react
function Ref() {

  const [elapsetime, setElapseTime] = useState(0)
  
  const lastupdate_ok = useRef(Date.now())
  let lastupdate_wrong = Date.now()

  function tempsecoule() {
    setElapseTime(Date.now() - lastupdate_ok.current)
    lastupdate_ok.current = Date.now()
    lastupdate_wrong = Date.now()
  }

  return( <div> 
     <button onClick={tempsecoule}> Temps écoulé ! { elapsetime } </button>
    </div>)
  
}
```
---

#### Ce qu'il faut savoir sur le hook de référence
- Il se comporte comme le hook d'état en ce qui concerne la conservation de sa valeur en cas de mise à jour
du composant
- Par contre sa modification ne provoque pas de mise à jour du composant 
---

#### Résumé  sur les hooks 

<table> 
<tr><th>Type de variable</th><th>Quand la variable est modifiée</th><th>Quand le composant est mis à jour</th></tr>
<tr><th>Variable classique</th><td>Pas de mise à jour du composant</td><td>La variable est remise à sa valeur initiale</td></tr>
<tr><th>Hook d'état</th><td>Remise à jour du composant</td><td>Conservation de la valeur de la variable</td></tr>
<tr><th>Hook de référence</th><td>Pas de mise à jour du composant</td><td>Conservation de la valeur de la variable</td></tr>
</table>

---

#### Créer une application en ReactJS

- Le plus simple est d'utiliser la commande:
```shell
npx create-react-app my-app
```

- Pour utiliser typescript :
```shell
npx create-react-app my-app --template typescript
```

---
### Typescript
- **ECMAScript Edition 5 (ES5)**
  - C’est le javascript *traditionnel* aujourd’hui bien implémenté dans tous les navigateurs
- **ECMAScript Edition 6 (ES6)**
  - Aussi appelé ES2015 (publié en juin 2015), progressivement implémenté dans les navigateurs et presque totalement dans node.js 
- **TypeScript** 
  - ES6 avec des types. Développé par Microsoft avec typage fort des données. Non supporté par les navigateurs, nécessite donc une transpilation vers ES6 (ou 5).
    
---
#### Typescript, un exemple
```typescript
class User {
  firstname: string // le type se met après le : 
  name = "singer" // possibilité de déduire le type de la valeur, ici name est 'string'
  age: number // un seul type numérique, number
  
  
  // Le type de retour des méthodes se met après son nom 
  name(): string {
    return this.firstname + " " + this.name
  }
  
  // Même chose avec des paramètres
  memeNomQue(name: string): boolean {
    return this.name === name
  }
  
  // méthode en notation fonctionnelle avec le type de retour implicitement 'boolean'
  isMajeur = () => age >= 18 
}  
```

