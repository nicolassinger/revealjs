
![logo](backgrounds/kotlinlogo2.png)  <!-- .element height="50%" width="50%" -->

*To be null* ou *to be pas null*

La gestion des nulls en Kotlin

*Nicolas Singer*
---
####  Rappel sur les objets  
- Création d'un objet
```kotlin
val loup = Wolf() // la variable loup référence un objet de type Wolf
```
- Comment faire pour qu'une variable ne référence rien ? 
```kotlin
val loup : Wolf = null 
```
- Dans la plupart des langages, une variable non initialisée représente la valeur `null`
```kotlin
val loup: Wolf // loup est null
```
---

#### Kotlin n'autorise pas une variable à être null

- Accomplir une action avec une variable null est une source d'erreur fréquente en programmation.
- Cela fait planter le programme, c'est la fameuse `null exception`

```kotlin
val loup: Wolf 

loup.hurler() // plantage
```

<span class="material-symbols-outlined"  style="font-size: 50px; vertical-align: middle">
warning
</span>
Le programme ci-dessus n'est pas possible en Kotlin
---

#### Pourquoi le null ? 

- Pourtant, il est parfois utile de gérer une valeur `null`.
- En effet, un objet peut avoir une raison légitime pour ne pointer vers rien

```kotlin
 val liste = listOf(2, 7, 1)
    
 val element = liste.find { it == 4 } // pas de 4 dans la liste
    
 println(element)  // element est null
```
v--

<iframe class="frame" src="https://pl.kotl.in/MnyfTwJFd"></iframe>

---

#### En Kotlin, le null est explicite

- Un type qui accepte la valeur `null` se déclare en le faisant suivre d'un point d'interrogation
 
```kotlin
val loup : Wolf? = null

println(loup) // ok, null
```
- Maintenant que Kotlin sait que loup peut être null, le compilateur nous empêche de faire n'importe quoi.
 
```kotlin
val loup : Wolf? = null

loup.hurler() // pas ok

if (loup != null) loup.hurler() // ok
```

v--
<iframe class="frame" src="https://pl.kotl.in/SEiW801yX"></iframe>

---

#### Utiliser un objet null

- Protéger l'appel en utilisant une condition avant 
```kotlin
if (loup != null) loup.hurler() // ok
```

- Utiliser l'opérateur *safe call* 
  - il se note `?.` 
  - il appelle la méthode ou la propriété si l'objet n'est pas *null*
  - sinon il retourne *null*
  ```kotlin
  loup?.hurler() 
  ```

- Utiliser la méthode `?.let {  }` pour exécuter du code uniquement si l'objet n'est pas *null*
```kotlin
loup?.let {
    it.hurler() // it représente loup
}
```
v--
<iframe class="frame" src="https://pl.kotl.in/CtCDQtKWg"></iframe>

---
#### Digression sur les *scope functions* de kotlin

- Kotlin possède des fonctions qui permettent d'exécuter du code dans le contexte d'un objet
- Elles s'appliquent sur l'objet et prennent en paramètre du code qui s'exécute dessus
- A l'intérieur du code, l'objet est désigné par `it` ou par `this` selon la fonction
- Elles rendent le code plus concis, car elles évitent la répétition du nom de l'objet

---

#### Exemple avec les *scope functions*

 - Écriture classique </li>

```kotlin
val loup = Wolf("Loup des neiges", "blanc")
println(loup.type)
loup.hurler()
println(loup.couleur)
```

- Écriture avec `.let` &nbsp;                                                                         

```kotlin
Wolf("Loup des neiges", "blanc").let {
		println(it.type)
		it.hurler()
		println(it.couleur)
}
```

v--
<iframe class="frame" src="https://pl.kotl.in/VUbhFqCdu"></iframe>
---

#### Toutes les *scope functions*

<table>
  <thead>
    <tr>
      <th>Fonction</th>
      <th>Référence</th>
      <th>Retourne</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>let</td>
      <td><code>it</code></td>
      <td><code>Lambda</code></td>
    </tr>
    <tr>
      <td>run</td>
      <td><code>this</code></td>
      <td><code>Lambda</code></td>
    </tr>
    <tr>
      <td>also</td>
      <td><code>it</code></td>
      <td><code>L'objet</code></td>
    </tr>
    <tr>
      <td>apply</td>
      <td><code>this</code></td>
      <td><code>L'objet</code></td>
    </tr>
  </tbody>
</table>

v--
<iframe class="frame" src="https://pl.kotl.in/yjxvkfJGh"></iframe>

---
#### Utiliser un objet *null*

- On peut chaîner l'opérateur 
 
```kotlin
val loups = listOf(
                Wolf("loup des neiges", "blanc"),
                Wolf("Loup des forêts", "roux"))

println(loups.find { it.couleur == "roux"}?.type?.uppercase())
```
    
- On cherche le loup
  - s'il n'est pas *null*, on prend son type
  - et si le type n'est pas *null*, on le met en majuscule.
  - sinon on retourne null

v--
<iframe class="frame" src="https://pl.kotl.in/oT8CtUFn_"></iframe>
---

####  Désactiver la sécurité du traitement des objets null
- Si le programmeur sait ce qu'il fait, il peut affirmer que la valeur est non *null*.
- Cela se fait avec l'opérateur `!!` (double point d'exclamation)

```kotlin
println(loups.find { it.couleur == "roux"}!!.type.uppercase())
```
- Cela signifie : *moi le programmeur, je suis sûr qu'on trouvera toujours un renard roux*
- **Ce genre d'affirmation est à éviter** !

v--
<iframe class="frame" src="https://pl.kotl.in/J2PKYm_3T"></iframe>

---
#### Substituer une valeur par défaut à null
- Parfois, on ne peut pas se satisfaire d'une valeur *null*
- On peut la remplacer par une valeur de son choix avec l'opérateur *Elvis*
- Il se note `?:`
 
```kotlin
println(loups.find { it.couleur == "noir"}?.type ?: "Loup inconnu")
```

- Ce qui est plus court que l'équivalent avec une condition : 
```kotlin
val type = loups.find { it.couleur == "noir"}?.type
if (type == null) println("Loup inconnu")
else println(type)
```

v--
<iframe class="frame" src="https://pl.kotl.in/ZuZVOZjqR"></iframe>