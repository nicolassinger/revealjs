
![logo](backgrounds/kotlinlogo2.png)  <!-- .element height="50%" width="50%" -->

Objets et classes avec Kotlin

*Nicolas Singer*
---
#### Les classes 

- Les classes permettent de définir de nouveaux **types** de valeurs
- Les valeurs obtenues à partir de ces classes sont appelées des **objets**
- En kotlin, toutes les valeurs sont des objets appartenant à une classe

---

#### Les classes prédéfinies

- Les types de base sont des classes prédéfinies
- Pour ces types, il existe des notations littérales qui créent l'objet directement

```kotlin
    // 5 est un objet littéral appartenant à la classe Int
	val a : Int = 5 
    
    // "coucou" est un objet littéral appartenant à classe String
    val mot = "coucou" 
    
    val liste = listOf(3, 4) 
    // listOf(3,4) est un objet littéral appartenant à la classe List<Int> 
```

---

#### Propriétés et méthodes

- Les objets peuvent avoir des propriétés et des méthodes propres.
- Quand on affecte une valeur à une variable, cette variable référence l'objet en question.

```kotlin
// la variable a référence l'objet 5 de classe Int
val a : Int = 5 
    
// les objets peuvent avoir des méthodes propres
// ici la méthode toString()
println(a.toString())
        
// même chose directement sur l'objet sans passer par la référence
println(5.toString())
        
// certains objets ont aussi des propriétés
val b : List<Int> = listOf(4, 10)        
        
// ici la propriété size d'une liste
println(b.size)
```

v--
<iframe class="frame" src="https://pl.kotl.in/rSwL3nRdb"></iframe>

---

#### La classe permet de créer ses propres types d'objets

- Une classe est un gabarit (*template*) qui décrit un type avec : 
  - ses propriétés ; 
  - ses méthodes.

- Une fois la classe définie, on peut créer un objet à partir de sa classe. 
  - C'est l'opération d'**instanciation**.
  
---

#### Définition d'une classe

- Le mot clé *class* sert à définir et à nommer une classe 
- La liste des propriétés qui permet de l'instancier est spécifiée entre parenthèses
- Un bloc de code permet ensuite de définir son contenu avec les autres propriétés et méthodes

```kotlin
// le type Chien avec ses propriétés nom, poids et race
class Chien(val nom: String, val poids: Double, val race: String) {

}

// quand une classe ne contient rien d'autres que ses propriétés 
// de construction, on peut ne pas mettre le bloc de contenu
class Chien(val nom: String, val poids: Double, val race: String)
```

---

#### Instanciation d'un objet
- Pour créer un objet du type de la classe, on appelle la classe comme une fonction.
- On passe en paramètre la valeur des propriétés.

```kotlin
   class Chien(val nom: String, val poids: Double, val race: String)
        
      
   val monchien1 = Chien("Groutch", 31.0, "Boxer")
        
   // comme pour les fonctions on peut préciser le nom de la propriété
   val monchien2 = Chien(nom = "Choupi", poids = 3.5, race = "Pékinois" )
```
---

#### Utilisation d'un objet
- Une fois l'objet instancié, on peut accéder à ses propriétés
  - En lecture seule, si la propriété a été déclarée avec `val`
  - En modification également, si elle a été déclarée avec `var`
  
```kotlin
   class Chien(val nom: String, val poids: Double, val race: String)
        
      
   val monchien1 = Chien("Groutch", 31.0, "Boxer")
        

   println("Mon chien s'appelle ${monchien1.nom} et pèse ${monchien1.poids} kilos")
```

v--
<iframe class="frame" src="https://pl.kotl.in/A6a3xKYGO"></iframe>

---
#### Méthodes
- Pour ajouter une méthode à un objet, on définit une fonction dans sa classe

```kotlin
 class Chien(val nom: String, val poids: Double, val race: String) {
        
     fun aboyer() {
         println( 
            when (race) {
                "Boxer" -> "warf warf"
                "Pékinois" -> "yip yip"
                else -> "wa wa"
         	}
         )
     }
 }
        
      
 val monchien1 = Chien("Groutch", 31.0, "Boxer")
 monchien1.aboyer() // affiche "warf warf"
```

v--
<iframe class="frame" src="https://pl.kotl.in/Akcbm8Rrf"></iframe>

---

#### Retour sur la création d'un objet
- Cela ressemble à un appel de fonction, mais c'est une instanciation d'objet

```kotlin
    val monchien = Chien("Groutch", 31.0, "Boxer")
```

- Techniquement, il y a bien une fonction qui est appelée, c'est le **constructeur primaire** de l'objet tel que 
défini par les paramètres de la classe
  - Cette fonction crée et retourne l'objet en question
  - La référence à l'objet est ensuite affecté à la variable `monchien`

---

#### La technique derrière le constructeur
- La déclaration : 

```kotlin
class Chien(val nom: String, val poids: Double, val race: String) 
```
- est un raccourci pour : 
 
```kotlin
class Chien(nom_param: String, poids_param: Double, race_param: String) {
    val nom = nom_param
    val poids = poids_param
    val race = race_param
}
```

v--
<iframe class="frame" src="https://pl.kotl.in/iv9awrdw-"></iframe>

---

#### Le bloc d'initialisation
- Dans une classe, on peut définir du code qui sera exécuté quand une instance d'objet est initialisée.
- Ce bloc est défini par une ou plusieurs sections `init  { ... }`

```kotlin
class Chien(val nom: String, val poids: Double, val race: String) {
  val categorie: String

  init {
    categorie = when (poids.toInt()) {
      in 0..10 -> "petit chien"
      in 11..20 -> "chien moyen"
      else -> "molosse"
    }
  }
}
```

v--
<iframe class="frame" src="https://pl.kotl.in/qCzdAae2g"></iframe>

---

#### Visibilité des propriétés
- On peut décider qu'une propriété de classe ne doit pas être visible à l'extérieur de la classe
- On utilise alors le modificateur `private`
- Ici par exemple, `poids` n'est pas utilisable à partir d'une référence à l'objet

```kotlin
class Chien(val nom: String, private val poids: Double, val race: String) {
  val categorie: String

  init {
    categorie = when (poids.toInt()) {
      in 0..10 -> "petit chien"
      in 11..20 -> "chien moyen"
      else -> "molosse"
    }
  }
}

val monchien = Chien("Groutch", 15.0, "Boxer")

println(monchien.categorie) // ok
println(monchien.poids) // pas ok 
```

v--
<iframe class="frame" src="https://pl.kotl.in/sRCiFjjiY"></iframe>







