
![logo](backgrounds/kotlinlogo2.png)  <!-- .element height="50%" width="50%" -->

Un langage de programmation moderne et multiplaforme

*Nicolas Singer*
---
#### Origine

- publié par la société JetBrain en 2011
![logo](backgrounds/jetbraintools.png)  <!-- .element height="90%" width="90%" -->
- Avoir un langage **concis** et **interopérable avec Java**
---
#### Spécificités
- Orienté objet et fonctionnel 
- Concis (moins de choses à écrire et plus facile à lire) 
- Statiquement typé ce qui permet au compilateur de vérifier le code 

```kotlin
fun main() {
    val saison = when (LocalDate.now().month) {
        in listOf(Month.JANUARY, Month.FEBRUARY, Month.MARCH) -> "Hiver"
        in listOf(Month.APRIL, Month.MAY, Month.JUNE) -> "Printemps"
        in listOf(Month.JULY, Month.AUGUST, Month.SEPTEMBER) -> "Eté"
        else -> "Automne"
    }
    print(saison)
}
```

---
#### Périmètre
- Koltin est **multiplateforme**

    - 100% interopérable avec Java
    - Android natif
    - Permet de créer des applications mobiles cross-platform (Ios, Android, Desktop)
    - Transpilable en Javascript 
    - Compilable en langage machine (Kotlin Native)

---

#### Cas d'utilisation

![logo](backgrounds/kotlinusage.png)  <!-- .element height="100%" width="100%" -->
---

#### Introduction au langage

- Plan : 
    1. Variables et types
    2. Opérations
    3. Structures de contrôles
    4. Fonctions
---
### 1. Les variables

- Une variable est un réceptacle contenant une valeur d'un certain type. 
- Quand on déclare une variable, on indique son nom, sa valeur initiale et le type de cette valeur. 
- Si cette valeur peut changer pendant l'exécution du programme, on déclare la variable en utilisant le mot `var`. Sinon, 
on utilise le mot `val` 

```kotlin
val nom : String = "Snow" 
val codePostal: Int = 81000
var temperature : Float = 37.5
``` 
---

#### Kotlin sait déduire un type

- La plupart du temps, on n'a pas besoin d'expliciter le type
    - Kotlin le déduit de la valeur

```kotlin
val nom = "Snow"  // c'est un String car valeur entre guillemets
val codePostal = 81000 // c'est un entier
val temperature = 37.5 // c'est un flottant car présence d'un point
``` 
---
#### On doit spécifier le type quand pas de valeur initiale

```kotlin
val mot: String
val codePostal: Int
    
mot = "hello"
println(mot)
println(codePostal)
``` 

<span class="material-symbols-outlined"  style="font-size: 50px; vertical-align: middle">
warning
</span>
<span> Le programme ci-dessus génère une erreur, laquelle ?  </span>

v--
#### Une variable doit être initialisée avant son utilisation

<iframe class="frame" src="https://pl.kotl.in/oU2o9GPaT"></iframe>

---

#### Les types disponibles 

- Les types de base
    - Les nombres entiers : `Byte` (8 bits), `Short` (16 bits), `Int` (32 bits), `Long` (64 bits)
    - Les nombres flottants:  `Float` (32 bits), `Double` (64 bits)
    - Les booléens : `Boolean`,
    - Les caractères et les chaînes de caractères : `Char`, `String`

---

#### Exemples avec les types de base

- Devinez le type de chacune des variables ci-dessous ?

```kotlin
val a = 129
val b = 100000000000
val c = 'c'
val d = "Salut"
val e  = false
var f = 3.4
val g = 12234.4f
val i: Byte = 23
```
---
#### Une variable `var` peut changer de valeur mais pas de type


```kotlin
var a = 2
var b = "salut"
   
a = 10000000000 
b  = a
```

<span class="material-symbols-outlined"  style="font-size: 50px; vertical-align: middle">
warning
</span>
Qu'est-ce qui ne va pas dans le programme ci-dessous ?

v--
#### Une variable `var` peut changer de valeur mais pas de type
<iframe class="frame" src="https://pl.kotl.in/aFLHh8UK_"></iframe>
---

#### Mais, on peut convertir un type vers un autre !

- On utilise pour cela les méthodes `toByte()`, `toInt()`, `toString()`, etc.


```kotlin
var a = 2
var b = "salut"
var c : Float
   
a = 10000000000.toInt() 
b  = a.toString()
   
c = "123.4".toFloat()
```

---
#### Les types collections 

- Les collections sont des types qui représentent des listes d'éléments.

- Parmis les types de collections les plus utilisés :
    - `List`, liste ordonnée de valeurs
    - `Map`, liste d'associations clé - valeur.
---
#### Le type `List`

- On utilise la fonction `listOf()` pour initialiser une valeur de type `List` non modifiable
- Si la liste doit être modifiable, on utilise mutableListOf()

```kotlin
val liste1 = listOf("fraise", "banane", "pomme")
val liste2 = mutableListOf(2, 8, 30, 1, 2)
    
liste2.add("framboise")
```
---
#### Le type `List` doit être précisé
 - Le type d'une liste est `List` mais ce type doit être affiné en spécifiant aussi le type
 des élements qu'elle contient :
    - `List<Int>` pour une liste d'entiers
    - `List<String>` pour une liste de chaînes de caractère, etc.

```kotlin
var liste : List<String>
liste = listOf("fraise", "banane", "pomme") // ok
liste = listOf("fraise", 4) // pas ok
```

---
#### Une liste peut contenir des éléments de types différents

- Le type `Any` correspond à n'importe quelle valeur

```kotlin
var liste : List<Any>
liste = listOf("fraise", "banane", "pomme") // ok
liste = listOf("fraise", 4) // ok aussi
```

- Néanmoins, on fera en sorte de manipuler des collections de valeurs d'un même type.

--- 
#### Accès aux élements individuels d'une liste

- La liste est ordonnée, on accède aux éléments par leur index qui commence à zéro
- C'est une erreur d'accéder à un numéro d'élément supérieur à la taille de la liste 


```kotlin
val liste = listOf("fraise", "banane", "pomme") 
val element2 = liste[1] // element2 contient "banane"
val element4 = liste[3] // erreur, la liste ne contient que trois éléments
```

v--
<iframe class="frame" src="https://pl.kotl.in/8Rl1OPFAL"></iframe>


---
#### Le type `Map`

- Le type `Map` permet de définir une liste d'associations entre deux valeurs. 
    - La première valeur est appelée la clé.
    - La deuxième valeur est simplement appelée la valeur.
- On initialise une `Map` avec la fonction `mapOf()` ou `mutableMapOf()` 
- On spécifie l'association en séparant la clé de la valeur avec le mot clé `to`.


```kotlin
val liste = mapOf("Tarn" to 81, "Averyon" to 12, "Haute-Garonne" to 31) 
```
---

#### Type complet d'une `Map` 
- Le type complet définit le type de la clé et le type de la valeur
- Si on reprend l'exemple précédent : 

```kotlin
val liste : Map<String, Int> 
liste = mapOf("Tarn" to 81, "Averyon" to 12, "Haute-Garonne" to 31) 
```
---

#### Accès aux éléments individuels d'une Map

- On peut obtenir la valeur en fonction de la clé 
```kotlin
val liste = mapOf("Tarn" to 81, "Averyon" to 12, "Haute-Garonne" to 31) 
val valeurTarn = liste["Tarn"]
```
- Dit autrement, une *Map* est une liste de valeurs indexées par une clé. 
 
---
#### Accès aux clés et aux valeurs d'une Map
- On peut aussi obtenir la liste des clés avec la propriété `keys`
- et la liste des valeurs avec la propriété `values`

```kotlin
val liste = mapOf("Tarn" to 81, "Averyon" to 12, "Haute-Garonne" to 31) 
val cles = liste.keys
val valeurs = liste.values
```
- A votre avis, quel est le type des variables `cles` et `valeurs` ? 
---

#### Le type `Set`

- Le type `Set` permet de définir une liste de valeurs non ordonnées et sans doublons. 
- Le type complet spécifie le type des éléments contenus avec `Set<...>`

```kotlin
val ensembleEntiers: Set<Int> = setOf(1, 12, 4)
var ensembleSalutations: Set<String> = setOf("salut", "hello", "bonjour")
val cles: Set<String> = mapOf("Tarn" to 81, "Averyron" to 12).keys
```

---

#### Accès aux élements individuels d'un ensemble
- Les valeurs d'un ensemble ne sont pas ordonnées
- On peut transformer un ensemble en liste, mais l'ordre des éléments est indéterminé 
```kotlin
val ensembleEntiers: Set<Int> = setOf(1, 12, 4)
val liste = ensembleEntiers.toList()
println(liste[0]) // resultat indeterminé, peut être 1, 12, ou 4
```

---

### 2. Les opérations

- Le langage propose un certain nombre de mots clés qui permettent de réaliser des opérations classiques 
  - Assignation d'une valeur à une variable (=)
  - Opérations mathématiques sur les nombres (+, -, *, /)
  
```kotlin
val a = 6
val b = a / 2 + 1

println(b * 2 + 4) // affiche 12
println( ( a * 2 ) / 4) // affiche 3
```
---
#### Les assignations augmentées
- On peut modifier la valeur d'une variable de façon plus concise avec les opérations `+=`, `-=`, `*=`, et `/=\`

```kotlin
var a = 6
val b = 3
    
a += 2 // remplace a = a + 2
a /= b // remplace a = a / b
a *= 4 // remplace a = a * 4
a -= 6 // remplace a = a - 6
```

---

#### Les opérations d'incrémentation et de décrémentation 
- On peut incrémenter la valeur d'une variable de façon plus concise avec `++` et `--` 
```kotlin
var a = 6
a++  // remplace a = a + 1
a-- // remplace a = a - 1
```

---

#### Les opérations de comparaison 
- `==`, `!=` (égalité et différence)
- `<`, `<=`, `>`, `>=` (inf, inf égal, sup, sup égal)
- Le résultat est une valeur de type `Boolean`
 
```kotlin
val a = 2
var b = a < 2 // b est égal à false
b = a <= 2 // b est égal à true
b = a != 2 // b est égal à false
```

---

#### Les opérations logiques
- On associe des conditions entre elles avec les opérateurs logiques
  - `&&` pour le *et* 
  - `||` pour le *ou* 
  - `!` pour la *négation* 
```kotlin
val a = 4
val b = (a > 2) && (a < 10) // b est égal à true
val c = ! (a <= 4) // c est égal à false
val d = ((a > 4) || b ) && !c 
```
- Que vaut `d` ? 

v--
<iframe class="frame" src="https://pl.kotl.in/C_1eY1rCs"></iframe>

---
#### Les opérations sur les autres types
- Certaines opérations arithmétiques peuvent s'appliquer aux listes et aux chaînes de caractères
 
```̀kotlin
 val adresse = "9 rue de la mairie"
 println("81100, " + adresse) // "81100, 9 rue de la mairie"
    
 val liste = listOf(1, 2, 3)
 println(liste + 4) // [1, 2, 3, 4]
 println(liste + listOf(4,5)) // [1, 2, 3, 4, 5]
```

---

### 3. Les structures de contrôles

- Pour contrôler le déroulement du programme, Kotlin propose classiquement : 
  - La structure conditionnelle *if/then/else*
  - les structures de boucle *while* et *do/while*
  - la structure de boucle *for*
  - la structure de multi-choix *when*
  - la structure d'interception d'exceptions

---

#### La structure conditionnelle *if/then/else*

```kotlin
val age = 18
    
if (age >= 18) println("peut voter")
    
if (age <= 14 || age >= 60) {
    println("tarif réduit")
}
else {
    println("tarif plein")
}
```

---

#### En Kotlin, la conditionnelle est une expression
- Elle retourne donc une valeur
- La valeur de la dernière expression du bloc est le résultat

```kotlin
val age = 18

val tarif = 
    if (age <= 14 || age >= 60) {
        println("Tarif réduit")
        7
    }
    else {
        println("Tarif plein")
        10
    }

println(tarif) // 10
```

---

#### Les boucle *while* et *do/while*

```kotlin
var compteur = 1
// affiche 1 puis 2 puis 3
while (compteur < 4) {
    println(compteur)
    compteur++
}
    
// affiche 0.44846187159283624, ..., 0.7453341832549193
do {
    val nombre = Math.random()
    println(nombre)
}
while(nombre < 0.5)
```

---

#### La boucle *for*

- Elle sert à parcourir les collections (List, Set, Map) 

```kotlin
val liste = listOf("fraise", "banane" , "abricot")

// affiche "fraise" puis "banane" puis "abricot"
for (fruit in liste) {
  println(fruit)
}

val deps = mapOf("Tarn" to 81, "Aveyron" to 12, "Lot" to 46)

// affiche "Tarn" puis "Aveyron" puis "Lot"
for ((cle, valeur) in deps) {
  println(cle)
}
```

---

#### La boucle *for* et les intervalles (*range*)
- Kotlin permet de définir des intervalles de valeurs très utiles avec la boucle *for*


```kotlin
// affiche 1, puis 2, puis 3 
for (valeur in 1..3) {
    println(valeur)
}
  
// même chose
for (valeur in 1..<4) {
    println(valeur)  
}
  
// affiche 3, puis 2, puis 1
for (valeur in 3 downTo 1) {
    println(valeur)
}
  
// affiche 1 puis 3
for (valeur in 1..4 step 2) {
    println(valeur)
}
```

---
#### La test multi-choix *when*
- Il permet de comparer une expression à une liste de valeurs et d'agir en conséquence

```kotlin
val fruit = "fraise"
var saison = ""
    
when (fruit) {
    "clémentine" -> 
        saison = "hiver"
	
    "fraise" -> 
    	saison = "été"
    	
  	"raisin" -> 
        saison = "automne"
        
    "rhubarbe" -> 
        saison = "printemps"
    }

// affiche "été"
println(saison)
```
 

v--
<iframe style="width:800px;height:800px" src="https://pl.kotl.in/Hwryr-Kbz"></iframe>

---

#### Le multi-choix est une expression

```kotlin
    val fruit = "fraise"
    val saison =
    	when (fruit) {
        	"clémentine" -> "hiver"
        	"fraise" -> "été"
    		"raisin" -> "automne"
        	"rhubarbe" -> "printemps"
            else -> "?"
    	}

    // affiche "été"
	println(saison)
```

v--
<iframe class="frame" src="https://pl.kotl.in/urh3wktxM"></iframe>

---

#### Le multi-choix permet des comparaisons plus complexes

```kotlin

    val age = 18
    
    var tarif  =
    	when (age) {
            in 15..59 -> 10
            else -> 7
    	}

    // affiche 10 euros
    println("$tarif euros")
    
    val fruit = "clémentine"
    val saison = when (fruit) {
            in listOf("framboise", "fraise") -> "été"
            "melon" -> "été"
            "clémentine", "mandarine" -> "hiver"
        	else -> "?"
    	}

  // affiche "hiver"
  print(saison)
```

v--
<iframe class="frame" src="https://pl.kotl.in/ZIgHSEQNg"></iframe>

---

#### La réaction aux erreurs *try/catch/finally*

- Quand on sait qu'un bloc d'instructions peut générer une erreur, on peut intercepter cette erreur et éviter
au programme de planter
```kotlin
try {
    // instructions potentiellement avec erreurs
    ...
}
catch (e: Exception) {
    // Si on est ici c'est qu'il y a eu une erreur
    ...
}
finally {
    // ces instructions sont exécutées quoi qu'il arrive
    ...
}
```


- le bloc `finally` est optionnel

---
#### Exemple de *try/catch*

```kotlin
    val prix = listOf("115", "200", "c150", "60")
    val tva = 0.2
    
    for (tarif in prix) {
        try {
        	val nombre = tarif.toInt()
            println("${nombre * (1 + tva)} euros")
        }
        catch (e: Exception) {
            println("$tarif n'est pas un entier")
        }
    }
```

v--
<iframe class="frame" src="https://pl.kotl.in/qblq-wyLx"></iframe>

---

### 4. Les fonctions

- Les fonctions permettent de modulariser son code 
- Elles définissent un bloc de code et lui donnent un nom 
- Ce bloc de code peut être exécuté en *appelant* le nom de la méthode
- On définit une fonction avec le mot clé **fun**
 
```kotlin
    fun coucou() {
        println("coucou")
    }
    
    coucou()
```

---
- Les fonctions peuvent avoir des paramètres et retourner une valeur
- On déclare les paramètres comme des variables, entre parenthèses après le nom de la fonction 
- Le type de la valeur retournée doit être également déclaré
 
```kotlin
fun add(a: Int, b: Int): Int {
    return a + b
}
    
val resultat = add(2,3) // resultat vaut 5
```

v--
<iframe class="frame" src="https://pl.kotl.in/lehB1prP1"></iframe>

---
#### Vérification des types
- Le compilateur vérifie que les valeurs passées à une fonction sont du bon type
- Il vérifie aussi que la valeur retournée est du bon type

```kotlin
 fun estMajeur(age: Int): Boolean {
        return age >= 18
    }
    
    val resultat = estMajeur(21) // ok
    val pasbon  = estMajeur("21") // pas ok 
```

v--
<iframe class="frame" src="https://pl.kotl.in/1BapldMB-"></iframe>

---

#### Fonctions *overload*
- On peut définir deux fonctions de même nom si elles ont des paramètres différents
- On appelle cela *overloader* une fonction
 
```kotlin
    fun estMajeur(age: Int): Boolean {
        return age >= 18
    }
    
    fun estMajeur(age: String): Boolean {
        return age.toInt() >= 18
    }
    
    val resultat = estMajeur(21) // ok
    val resultat2 = estMajeur("21") // ok 
```

---
#### Fonctions avec une seule expression
- Quand le code d'une fonction n'est composé que d'une seule expression, on peut éviter les accolades et le mot clé `return` : 
 
```kotlin
fun estMajeur(age: Int) = 
        age >= 18
    
val resultat = estMajeur(21)
```

v--
<iframe class="frame" src="https://pl.kotl.in/BVmHbnQJR"></iframe>

---

#### Type de la valeur d'une fonction qui ne retourne rien
- Quand une fonction ne retourne rien, son type implicite est `Unit`

 ```kotlin
fun affiche(message: String) {
        println(message)
    }
    
    
    // même chose avec type de retour explicite
     fun affiche2(message: String): Unit {
        println(message)
    }
    
    
    affiche("coucou")  
 ```

---

#### Variables déclarées dans des fonctions
- Les variables d'une fonction ne sont visibles que par celle-ci
- On les appelle des variables **locales**

```kotlin
    fun afficheAvecTitre(nom: String, titre: String) {
        val separateur = ". " // variable locale
        
        println("${titre}${separateur}${nom}")
    }
    
    
    afficheAvecTitre("Singer", "Prof") 
```
v--
<iframe class="frame" src="https://pl.kotl.in/ju8v_1E5a"></iframe>

---

#### Appel d'une fonction avec nommage des paramètres
- Lors de l'appel d'une fonction on passe les paramètres dans l'ordre de leur déclaration
- On peut aussi les nommer pour plus de clarté
- Le fait de les nommer, permet de changer l'ordre lors de l'appel

```kotlin
    fun afficheAvecTitre(nom: String, titre: String) {
        val separateur = ". "
        
        println("${titre}${separateur}${nom}")
    }
    
    afficheAvecTitre("Singer", "Prof")  // d'abord le nom, puis le titre
    afficheAvecTitre(nom = "Singer", titre = "Prof") // ici on nomme les paramètres
    afficheAvecTitre(titre = "Prof", nom = "Singer") // quand on nomme, on peut changer l'ordre 
```

v--
<iframe class="frame" src="https://pl.kotl.in/4r4KS2qbr"></iframe>

---

#### Paramètres avec valeur par défaut
- Lors de la déclaration d'une fonction, on peut affecter une valeur par défaut aux paramètres
- Cela permet de ne pas préciser de valeur lors de l'appel

```kotlin
fun afficheAvecTitre(nom: String, titre: String, separateur: String = ". ") {
    println("${titre}${separateur}${nom}")
}
    
afficheAvecTitre(nom = "Singer", titre = "Prof") // separateur non précisé, valeur par défaut
afficheAvecTitre(titre = "Prof", nom = "Singer", separateur=", ") // on change le séparateur 
```

v--
<iframe class="frame" src="https://pl.kotl.in/kSSyFXOHC"></iframe>