
![logo](backgrounds/kotlinlogo2.png)  <!-- .element height="50%" width="50%" -->

Programmation fonctionnelle avec Kotlin

*Nicolas Singer*
---
####  Traiter le code comme une donnée  
- En Kotlin, on peut affecter du code à une variable.
- On peut aussi le passer en paramètre.
- Un tel code se représente sous la forme d'une lambda expression.

```kotlin
val codeAddition = { a: Int, b: Int ->
  a + b
}
```
- Le mot "lambda" vient de l'utilisation du caractère grec λ dans le lambda-calcul
---

#### Syntaxe d'une lambda
- Une lambda ressemble à une fonction sans nom.
- Elle se note entre accolades, avec une flèche pour séparer les paramètres du corps de la lambda.
- La dernière expression du corps est ce que retourne la lambda.

```kotlin
{ // accolade d'ouverture 
    a: Int, b: Int // paramètres avec leurs types
    -> // fleche séparatrice
    a + b // corps 
} // accolade de fermeture
```

---
#### Lambda sans paramètres
- Quand une lambda n'a pas de paramètres, on donne directement son corps.
- Voici une lambda qui ne fait qu'afficher *coucou* puis *hello*.
```kotlin
{
    println("coucou" )
    println("hello")
}
```
---

#### Invocation d'une lambda
- Le code d'une lambda s'exécute quand on appelle sa méthode `.invoke(...)` 
```kotlin
val addition = { a: Int, b: Int -> a + b }
println(addition.invoke(2,3)) // affiche 5
```

- Mais le plus souvent, on l'exécute en utilisant la notation habituelle d'une fonction.
```kotlin
println(addition(3,4)) // affiche 7
```
---

#### Type d'une lambda

- Une lambda a un type qui est sa signature
- Il se note `(parametres) -> type_de_retour`
- Quand une lambda ne retourne rien, le type de retour est `Unit`
- Par exemple : 

```kotlin
{ a: Int, b: Int -> a + b } // le type est (Int, Int) -> Int

{ println("coucou") } // le type est () -> Unit

{ p: Point -> p.x * p.x + p.y * p.y } // le type est (Point) -> Double
```
v--
<iframe class="frame" src="https://pl.kotl.in/ckw2gwUkc"></iframe>

---
#### Type d'une lambda
- Quand le type d'une lambda est explicitement déclaré, on peut omettre le type
des paramètres.

```kotlin
val a: (Double) -> Double = { a: Double -> a * a }
val a: (Double) -> Double = { a -> a * a } // plus court
```

- Quand la lambda possède un seul paramètre dont le type est connu, on peut l'omettre et utiliser 
`it` pour en parler

```kotlin
val a: (Double) -> Double = { it * it } // encore plus court
```
v--
<iframe class="frame" src="https://pl.kotl.in/WKSdAlQVu"></iframe>
---

#### A quoi servent les lambdas ? 
- **On peut passer des lambdas à des fonctions**.
- Cela permet de passer un comportement spécifique à une fonction plus générale.
- Une fonction qui prend en paramètre une lambda est une fonction d'**ordre supérieur**.

---

#### Exemple de fonction d'ordre supérieur
- Soit une fonction générale qui convertit un nombre flottant vers un autre.
  - Soit une fonction spécifique qui converti une température en fahrenheit vers une température en Celsius.
  - Soit une autre fonction spécifique qui converti des poids en livre vers des poids en kilo.
- On peut passer à la fonction générale, une des fonctions spécifiques qui réalise la conversion de la valeur.

```kotlin
 
val ftoc = { a: Double -> (a - 32) * 5.0/9.0 } // Farenheight vers Celcius
val ltok = { a: Double -> a * 0.4536 } // Livre vers Kilo

fun convertir(valeur: Double, convertisseur: (Double) -> Double) : Double {
  return convertisseur(valeur)
}
```
v--
<iframe class="frame" src="https://pl.kotl.in/7LbBuP4SV"></iframe>

---
#### Simplification de la syntaxe d'appel avec une lambda
- Quand le dernier paramètre d'une fonction est une lambda, on peut sortir la lambda de la
parenthèse lors de l'appel.

```kotlin
fun convertir(valeur: Double, convertisseur: (Double) -> Double) : Double {
    return convertisseur(valeur)
}
    
convertir(100.0) { it * 0.4536 } // conversion livres vers kilo
```
- Quand le seul paramètre est une lambda, on peut omettre complètement les parenthèses lors de l'appel.
 
```kotlin
fun convertir100(convertisseur: (Double) -> Double) : Double {
    return convertisseur(100.0)
}
    
convertir100 { it * 0.4536 } // conversion livres vers kilo
```

v--
<iframe class="frame" src="https://pl.kotl.in/JyANl_wZ_"></iframe>
---

####  Une fonction peut aussi retourner une lambda 
- Par exemple, soit une fonction qui choisi quel est le convertisseur à utiliser : 

```kotlin
fun convertir(valeur: Double, convertisseur: (Double) -> Double) : Double {
        return convertisseur(valeur)
 }
    
 fun choixConvertisseur(s: String) : (Double) -> Double {
        when (s) {
            "temperature" -> return { (it - 32) * 5.0/9.0 }
            "poids" -> return { it * 0.4536 }
            else -> return { it }
        }
 }
 println(convertir(100.0, choixConvertisseur("poids"))) 
```
---

#### Les fonctions d'ordre supérieur existantes
- Les collections sont parmi les objets comportant le plus de méthodes d'ordre supérieur. 
- En effet leur parcours se prête bien à l'application de code sur chacun de leurs éléments.
- Exemple, la méthode `maxBy`
  - La méthode `maxBy` de l'objet `List` prend en paramètre une lambda qui doit sélectionner le critère 
qui sert à classer les objets de la liste.

```kotlin
class Personne(val nom: String, val age: Int)

val liste = listOf(Personne("Marie", 41), Personne("Jean", 43), Personne("Samuel", 17))
val maxage_personne =  liste.maxBy { it.age } // la lambda retourne l'age
    
println(maxage.nom)
```
v--
<iframe class="frame" src="https://pl.kotl.in/qcpQi8fWd"></iframe>

---
#### Citons quelques autres méthodes sur les listes

```kotlin
// vérifie que tous la lamdba est vraie pour tous les éléments
val tousMajeurs =  liste.all { it.age >= 18 } 

// vérifie que la lambda est vraie pour au moins un élément
val unMineur = liste.any { it.age < 18 }

// retourne le premier élément pour lequel la lambda est vraie, null sinon
val jean = liste.find { it.nom == "Jean"}

// exécute la lambda si la liste est vide
val personne = listeVide.ifEmpty {
  println("La liste est vide")
  listOf(Personne("Initial", 20))
}
```
- Il y en a beaucoup d'autres ! 

v--
<iframe class="frame" src="https://pl.kotl.in/X4de2rtNB"></iframe>
---

#### Les parcours de liste avec les lambdas
- Les lambdas rendent plus expressifs les parcours de listes
- Quatre méthodes essentielles :
  - `forEach { }` : Applique la lambda sur chaque élément de la liste
  - `filter { }` : Retourne une nouvelle liste composée de tous les éléments pour lesquels la lambda est vraie
  - `map { }` : Retourne une nouvelle liste où chaque élément est calculé en appliquant la lambda sur l'élément initial.
  - `fold { }`: Accumule tous les éléments de la liste selon la lambda et retourne la valeur finale. 
---

#### Exemple de traitement des listes avec les lambdas

```kotlin
// affiche toutes personnes
liste.forEach { println("${it.nom} a ${it.age} ans")}
    
// retourne toutes les personnes majeures
liste.filter { it.age >= 18}
    
// retourne une liste d'entier avec l'année de naissance des personnes
liste.map { 2024 - it.age }
    
// retourne la somme des ages 
liste.fold(0) { acc, p -> acc + p.age }

// affiche le nom des personnes majeures
liste.filter { it.age >= 18 }.map { it.nom }.forEach { println(it) }
```
v--
<iframe class="frame" src="https://pl.kotl.in/C9EVW0t1dt statu"></iframe>