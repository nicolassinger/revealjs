
![logo](backgrounds/kotlinlogo2.png)  <!-- .element height="50%" width="50%" -->

Héritage et polymorphisme avec Kotlin

*Nicolas Singer*
---
####  Les classes et l'héritage  

- L'héritage permet à des classes de partager du code commun avec d'autres classes.
- Le mécanisme consiste à permettre à une classe d'hériter des propriétés et des méthodes
d'une classe de base appelée aussi **superclasse**.
- La classe héritière, aussi appelée **sous-classe**, peut ajouter des propriétés ou des 
méthodes qui n'existent pas dans la superclasse.
- Elle peut aussi redéfinir (ou **surcharger**, **override** en anglais) certaines méthodes  de la superclasse.

---

#### Exemple d'héritage 1/4

- Considérons quatre animaux, un *lion*, un *loup*, un *lynx* et un *renard* modélisés 
chacun par une classe portant leur nom.
- Utilisons une superclasse appelée `Animal` pour modéliser les propriétés et méthodes communes à ces animaux. Voici cette classe :

```kotlin
// le mot clé open permet l'héritage
open class Animal() {
  val image: String = "" // une photo de l'animal
  val aliment: String = "" // ce qu'il mange
  val habitat: String = "" // son habitat
  val faim: Int = 10 // son niveau de faim actuel

  // quatre méthodes pour faire agir l'animal
  // le mot clé open permet leur éventuelle surcharge par les sous-classes
  open fun crier() { println("L'animal fait du bruit") }

  open fun manger() { println("L'animal mange") }

  open fun dormir() { println("L'animal dort") }

  open fun errer() { println("L'animal erre") }
}
```
---

#### Exemple d'héritage 2/4

- On fait hériter la classe `Lion` de la classe `Animal` avec la syntaxe `: Animal()`

```kotlin
class Lion : Animal() {

}
```

- Si on en reste là, un Lion fait tout comme un Animal. 

v--

<iframe class="frame" src="https://pl.kotl.in/ITnutlWfs"></iframe>

---

#### Exemple d'héritage 3/4

- Voici comment on spécialise la classe `Lion` par rapport à la classe `Animal`
- On peut surcharger les méthodes et les propriétés avec le mot `override`

```kotlin

class Lion : Animal() {
  override val image = "Lion.jpg"
  override val aliment = "viande"
  override val habitat = "savane"
    
  override fun crier() { println("Le lion rugit")}
  override fun manger() { println("Le lion mange de la ${aliment}")}

}
```

v--
<iframe class="frame" src="https://pl.kotl.in/XB3d943Ka"></iframe>

---

#### Exemple d'héritage 4/4
- Les autres classes héritent aussi de `Animal`
 
```kotlin
class Renard : Animal() {
    override val image = "Renard.jpg"
    override val aliment = "de tout"
    override val habitat = "campagne"

    override fun crier() { println("Le renard glappit")}
    override fun manger() { println("Le renard mange de la ${aliment}")}
}
```

---
#### Représentation de l'héritage avec un diagramme de classe 

![logo](diagram/animal_diagram.png) <!-- .element height="50%" width="50%" -->
---

#### Hiérarchie d'héritage
- Une sous-classe peut aussi être la superclasse d'une autre classe.
- Introduisons par exemple la classe `Canide`

```kotlin
open class Canide(): Animal() {
    override fun errer() {
        println("Le canidé erre")
    }
}

class Renard : Canide() {
    override val image = "Renard.jpg"
    override val aliment = "de tout"
    override val habitat = "campagne"

    override fun crier() { println("Le renard glappit")}
    override fun manger() { println("Le renard mange de la ${aliment}")}
}
```

v--
<iframe class="frame" src="https://pl.kotl.in/2KJ7eePPb"></iframe>

---

#### Diagramme complet de l'héritage

![logo](diagram/canide_diagram.png) <!-- .element height="50%" width="50%" -->
---

#### Propriétés de l'héritage 
- Les sous-classes possèdent toutes les méthodes et propriétés de leurs superclasses.
- Une variable du type d'une superclasse, peut prendre pour valeur n'importe quel objet du type 
d'une de ses sous-classes
- Quand on appelle une méthode d'un objet sous-classe, c'est la méthode la plus spécifique qui 
est appelée.

```kotlin
// le loup est un animal
val un_animal: Animal = Loup()

// C'est aussi un canidé
val un_canidé: Canide = Loup()

// et c'est bien sûr un loup
val un_loup: Loup = Loup()

// c'est la méthode de la classe Loup qui est appelée
un_animal.crier()

```

v--
<iframe class="frame" src="https://pl.kotl.in/yWs4Re2is"></iframe>

---
#### Polymorphisme
- Soit une fonction qui prend en paramètre un `Animal`.
- On peut lui passer n'importe quelle sous-classe.
 
```kotlin
class Veto(val animal: Animal) {
    fun faire_picure() {
        animal.crier()
    }
}

val veto = Veto(Loup())
veto.faire_picure()
```

- Le polymorphisme est la capacité d'utiliser un objet (`Loup`) à la place 
d'un autre (`Animal`) et de donner plusieurs implémentations 
différentes à une fonction (`crier()`)

v--
<iframe class="frame" src="https://pl.kotl.in/UkPtlCFzD"></iframe>
---

#### Les classes abstraites
- Parfois, c'est une mauvaise chose de pouvoir créer une superclasse
- Par exemple la classe `Animal` ne devrait pas pouvoir être instanciée ()
- On la déclare alors *abstraite* avec le mot clé `abstract`

```kotlin
abstract class Animal() {
   open val image: String = "" // une photo de l'animal
   open val aliment: String = "" // ce qu'il mange
   open val habitat: String = "" // son habitat
   val faim: Int = 10 // son niveau de faim actuel


    open fun crier() { println("L'animal fait du bruit") }
    open fun manger() { println("L'animal mange") }
    open fun dormir() { println("L'animal dort") }
    open fun errer() { println("L'animal erre") }
}

val animal = Animal() // erreur, la classe est abstraite
```

---

#### Les méthodes et propriétés abstraites
- Une classe abstraite peut avoir des méthodes ou propriétés abstraites
- Ce sont des méthodes et propriétés qui n'ont pas de sens sans implémentation concrète.
  - Pas d'implémentation générique pour ces méthodes ou propriétés

```kotlin
abstract class Animal() {
      abstract val image: String // une photo de l'animal
      abstract val aliment: String // ce qu'il mange
      abstract val habitat: String // son habitat
      val faim: Int = 10 // son niveau de faim actuel
    
      abstract fun crier() 
      abstract fun manger() 
      open fun dormir() { println("L'animal dort") }
      open fun errer() { println("L'animal erre") }     
}
```

---

#### Héritage d'une superclasse abstraite
- Une sous-classe concrète doit implémenter toutes les propriétés 
et méthodes abstraites

```kotlin
// cette classe est abstraite, elle a le droit ne pas implémenter les
// propriétés et méthodes abstraites de Animal
abstract class Canide(): Animal() {
   override fun errer() { println("Le canidé erre") }
}
// celle-ci est concrète, elle doit implémenter toutes les méthodes et 
// propriétés abstraites des superclasses
class Renard : Canide() {
    override val image = "Renard.jpg"
    override val aliment = "de tout"
    override val habitat = "campagne"
    override fun crier() { println("Le renard glappit")}
    override fun manger() { println("Le renard mange de la ${aliment}")}
}
```
v--
<iframe class="frame" src="https://pl.kotl.in/92z1NxM8h"></iframe>
---
#### Rôle des propriétés et méthodes abstraites
- Même si elles n'ont pas d'implémentation, elles définissent un contrat
commun pour toutes leurs sous-classes.
- Par exemple, tous les objets de type `Animal` ont une méthode `crier()`, même si c'est à chacun 
d'implémenter cette méthode (polymorphisme). 
- Cela permet à une fonction prenant en paramètre un objet de type `Animal` de pouvoir appeler 
la méthode `crier()` de cet objet, quelque soit son implémentation.

---
#### Le polymorphisme avec des classes indépendantes
- Parfois des classes indépendantes peuvent avoir des comportements communs
- Prenons par exemple une classe `météorite` qui possède une méthode `errer()` dans l'espace.
- Une météorite n'est pas un animal, mais elle a cette méthode `errer()` en commun avec lui.
- C'est le rôle des **interfaces** de modéliser ce comportement commun
  - L'interface peut le modéliser de façon abstraite ou concrète
  - Toute classe peut **implémenter** les méthodes d'une ou plusieurs interfaces

---

#### Exemple avec l'interface `PeutErrer`

```kotlin
interface PeutErrer {
    // ici errer est une méthode abstraite
    fun errer()
    
    // on aurait pu en faire une méthode concrète 
    // fun errer() {
    //    println("L'objet qui peut errer se déplace")
    //}
 
    // Les propriétés sont forcément abstraites (sans valeur)
    val vitesse: Int
}
```

---
#### Implémentation d'une interface
- Même syntaxe que pour l'héritage (`: <interface>`)
- Les classes concrètes implémentant l'interface doivent coder toutes leurs propriétés et méthodes

```kotlin
class Meteorite() : PeutErrer {
    override val vitesse = 1000
    override fun errer() {
        println("La météorite fonce à travers l'espace " +
                "à la vitesse de ${vitesse}km/s")
    }
}
    
abstract class Animal() : PeutErrer {
    override fun errer() { println("L'animal erre") }
}

class Renard : Canide() {
    override val vitesse= 10
}
```
v--
<iframe class="frame" src="https://pl.kotl.in/yskJFQXdu"></iframe>

---
#### Implémentation de plusieurs interfaces
- Une classe peut implémenter plusieurs interfaces
- Syntaxiquement, on les sépare par une virgule après le `:`
- On peut aussi ajouter une superclasse si besoin

```kotlin 
interface PeutErrer {
    fun errer() 
    val vitesse: Int
}
interface peutTourner {
    fun tourner() 
}
abstract class Roche()

class Meteorite() : Roche(), PeutErrer, peutTourner {
	override val vitesse = 1000
    override fun errer() { .. }
    override fun tourner() { println("La météorite tourne sur elle même")  }
}
```
---
#### Quand utiliser l'héritage ou une interface ? 
- Interface et héritage permettent de définir des comportements et propriétés communs.

- L'héritage permet de définir un gabarit utilisable par toutes les sous-classes.
  - Il réduit la duplication de code en permettant au code générique d'être dans les superclasses
- L'interface permet de définir un comportement commun entre des classes qui ne sont pas reliées par
des relations de type **est un/une**. 
  - Une météorite *n'est pas* un animal, seul l'interface permet de définir un comportement commun

---

#### Utiliser un objet du type de l'interface
- Tous les objets implémentant l'interface `PeutErrer` ont donc une méthode `errer()`.
- Soit une méthode prenant en paramètre un tel objet : 

```kotlin
fun faireAvancer(items: List<PeutErrer>) {
    for (item in items) item.errer()
}
```

- On peut lui passer une liste d'objet implémentant `PeutErrer`

```kotlin
val loup = Loup()
val met = Meteorite()
faireAvancer(listOf(loup, met))
```

v--
<iframe class="frame" src="https://pl.kotl.in/7RyRv1Fa_"></iframe>
---

#### Test du type d'un objet
- L'opérateur `is` permet de tester quel est le type d'un objet.
- Quand le type connu d'un objet est générique (interface ou superclasse), cela permet 
d'appeler une des méthodes ou propriétés spécifiques à cet objet

```kotlin
  fun faireAvancer(items: List<PeutErrer>) {
    	for (item in items) { 
        	item.errer()
            if (item is Animal) item.crier()
        }
	}
     
    val loup = Loup()
    val met = Meteorite()
    faireAvancer(listOf(loup, met))
```
v--
<iframe class="frame" src="https://pl.kotl.in/2374Xat73"></iframe>











