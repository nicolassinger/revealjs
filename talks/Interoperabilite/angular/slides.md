
![logo](backgrounds/angular-banniere.png)  <!-- .element height="20%" width="20%" -->

<https://angular.io>

Nicolas Singer
---
#### Angular : Un framework MVC pour le développement frontend web 
- créé par Google en 2010
- Permet une séparation claire entre le code et l'IHM 
- spécialisé dans le *Single page application* (SPA)
- impose l'utilisation du langage *TypeScript* 
---

#### Les versions 
- 2012 : AngularJS 1 : basé sur Javascript et JQuery
- 2016 : Angular 2 : Refonte complète basée sur le langage TypeScript, incompatible avec AngularJs
- 2023 : Angular 19 : dernière version en date, compatible avec la version 2.

---

#### Typescript ? 

- ECMAScript Edition 6 (ES6) 
  - Aussi appelé ES2015 (publié en juin 2015), c'est le javascript *traditionnel* pris en charge par tous les navigateurs

- ES2016, ES2017... : 
  - Depuis ES6, un cycle de mise à jour annuelle

- TypeScript
  - Sur-ensemble de Javascript développé par Microsoft avec **typage fort des données**. Non supporté directement
par les navigateurs, nécessite donc une *transpilation* vers Javascript.
  
---

#### Typescript : un exemple

```typescript
class User {
  firstname: string // le type se met après le : 
  name = "singer" // possibilité de déduire le type de la valeur, ici name est 'string'
  age: number // un seul type numérique, number
  
  
  // Le type de retour des méthodes se met après son nom 
  name(): string {
    return this.firstname + " " + this.name
  }
  
  // Même chose avec des paramètres
  memeNomQue(name: string): boolean {
    return this.name === name
  }
  
  // méthode en notation fonctionnelle avec le type de retour implicitement 'boolean'
  isMajeur = () => age >= 18 
}  
```
---

#### Créer une application Angular
Angular dispose d'une *Command Line Interface* pour amorcer ses projets.

- Créer un projet appelé *monapp*
```shell
npm init @angular monapp
```

- Lancer l'application 
```
cd monapp
npm start
```

- Afficher l'application dans son navigateur :  <a href="http://localhost:4200">http://localhost:4200</a>

---

#### Logique composant 
Angular utilise une classe annotée par  `@Component` pour afficher du HTML.

<div class="twocolumn">

<div>

```typescript
import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>Mon premier composant Angular </h1>
  `,
  standalone: true,
})
export class AppComponent {}
```

</div>
<div>
<img src="backgrounds/premiercomposant.png"/>
</div>

</div>

---
#### Chaque composant a trois parties
<div class="twocolumn">

<div>

```angular17html
import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  // 1. Du code HTML
  template: `
    <h1>Mon second composant</h1>
  `,
  // 2. Des styles CSS
  styles: `
    :host {
      color: blue;
    }
  `,
  standalone: true,
})

// 3. une classe typescript
export class AppComponent {}
```

</div>
<div>
<img src="backgrounds/secondcomposant.png"/>
</div>

</div>

---
#### La logique du composant est défini dans la classe

- dans le template, on utilise les propriétés de la classe en les encadrant par `{{` et `}}`.
 
<div class="twocolumn">

<div>

```angular17html
import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div> mon age : {{ age }} </div>
  `,
  standalone: true,
})

export class AppComponent {
  age = 20
}
```

</div>
<div>
<img src="backgrounds/troisiemecomposant.png"/>
</div>

</div>

---
#### Composition de composants
- C'est le sélecteur qui permet à un composant d'en intégrer un autre

<div class="twocolumn">

<div>

```angular17html
@Component({
  selector: 'app-user',
  template: `mon age : {{ age }}`,
  standalone: true,
})
export class AgeComponent {
  age = 20;
}

@Component({
  selector: 'app-root',
  template: `
    <p>mon prénom: {{ prenom }} </p>
    <app-user/>`,
  standalone: true,
  imports: [AgeComponent],
})
export class AppComponent {
  prenom = "Nicolas"
}
```

</div>
<div>
<img src="backgrounds/quatriemecomposant.png"/>
</div>

</div>

---
#### Les structures de contrôle: `@if`

- Pour exprimer des conditions dans le HTML, Angular utilise la syntaxe `@if`

<div class="twocolumn">

<div>

```angular17html
@Component({
  selector: 'app-root',
  template: `
   @if (isLogin) {
      <span>Je suis connecté</span>
   }
   @else {
      <span>Se connecter...</span>
   }
  `,
  standalone: true,
})
export class AppComponent {
  isLogin = true
}
```

</div>
<div>
<img src="backgrounds/if.png"/>
</div>

</div>

---
#### Les structures de contrôle: `@for`

- Pour exprimer des boucles dans le HTML, Angular utilise la syntaxe `@for`
- L'attribut `track` désigne un élément unique qui distingue les éléments de la liste.

<div class="twocolumn">

<div>

```angular17html
@Component({
selector: 'app-root',
template: `
 <ul>
 @for (pers of personnes; track pers.nom) {
  <li> {{ pers.prenom + " " + pers.nom }} </li>
 }
 </ul>`,
standalone: true,
})
export class AppComponent {
  personnes = [ 
    { nom: "Stark", prenom: "Ned"},
    { nom: "Baratheon", prenom: "Cercei"}, 
    { nom: "Lanister", prenom: "Jaime"}]
}
```

</div>
<div>
<img src="backgrounds/for.png"/>
</div>

</div>

---
#### Lier dynamiquement les propriétés HTML 

- On utilise le *binding* de propriétés pour positionner la valeur des attributs HTML en fonction du code.
- Pour cela on entoure la propriété par `[ ... ]` et on lui affecte l'expression qui donne sa valeur.

<div class="twocolumn">

<div>


```angular17html
@Component({
  selector: 'app-root',
  styles: `
   .rouge {
       color: red
   }
  `,
  template: `
    <span [class]="style"> Salut </span>`,
  standalone: true,
})
export class AppComponent {
  style = "rouge"
}
```

</div>
<div>
<img src="backgrounds/binding.png"/>
</div>

</div>
---
#### Interception des évènements

- On peut réagir aux actions de l'utilisateur en ajoutant des intercepteur d'évènements sur les balises HTML.
- On désigne le type d'évènement entre parenthèses et on lui affecte une méthode qui sera appelée quand l'évènement a lieu.



<div class="twocolumn">

<div>

```angular17html
@Component({
  selector: 'app-root', 
  template: `
    Vous avez un message, cliquez 
       <span (click)="onClick()">ici</span>
        pour l'obtenir.
      <p>{{  message }}</p>`,
  standalone: true,
})
export class AppComponent {
  message = ""

  onClick() {
    if (this.message) { this.message = "" }
    else { this.message = "Salut toi !" }
  }
}
```

</div>
<div>
<video data-autoplay loop src="videos/event.webm"></video>
</div>

</div>

---
#### Double binding sur les champs de formulaire

- Pour lier une propriété à un champ de formulaire, la liaison doit se faire dans 
les deux sens : 
- C'est l'attribut spécial `[(ngModel)]` qui permet cela.

<div class="twocolumn">

<div>

```angular17html
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';

@Component({
  selector: 'app-user',
  template: `
    <p>Username: {{ username }}</p>
    <p>Votre nom: 
      <input type="text" 
        [(ngModel)] = "username"/> 
  `,
  standalone: true,
  imports: [FormsModule],
})
export class UserComponent {
  username = 'Cercei';
}
```

</div>
<div>
<video autoplay loop src="videos/forms.webm"/>
</div>

</div>
---
#### Passage de paramètres entre composants

- Parent vers enfant 
  - Quand une propriété doit être passée par la composant parent, on l'annote avec `@Input()`
  - Le composant parent utilise un attribut de même nom pour passer la valeur à l'enfant.

<div class="twocolumn">

<div>

```angular17html
@Component({
  selector: 'app-user',
  template: `
    <p>L'age de l'utilisateur est {{ age }}</p>`,
  standalone: true,
})
export class UserComponent {
  @Input() age = ""
}

@Component({
  selector: 'app-root', 
  imports: [UserComponent],
  template: `<app-user age="20"/>`,
  standalone: true,
})
export class AppComponent {}
```

</div>
<div>
<img src="backgrounds/input.png"/>
</div>

</div>
---
#### Passage de paramètres entre composants

- Enfant vers parent
  - L'enfant envoie un évènement au parent. 
  - Cet évènement est annoté par `@Output()`
  - Le parent intercepte l'évènement en le liant à une de ses méthodes.

<div class="twocolumn">

<div>

```typescript
@Component({
  selector: 'app-child',
  template: `
   <button class="btn" (click)="incCompteur()">
     Incrémenter compteur</button>`,
  standalone: true,
})
export class ChildComponent {
  @Output() compteurChange 
    = new EventEmitter<number>()
  compteur = 0
  
  incCompteur() {
    this.compteur++
    this.compteurChange.emit(this.compteur)
  }
}
```

</div>
<div>

```typescript
@Component({
  selector: 'app-root',
  template: `
    <app-child
     compteurChange="onChange($event)" />
    <p>Compteur : {{ compteur }}</p>`,
  standalone: true,
  imports: [ChildComponent],
})
export class AppComponent {
  compteur = 0
  onChange(value: number) {
    this.compteur = value
  }
}
```

</div>

</div>
---

#### Répartir son composant en plusieurs fichiers

- Pour l'instant nous avons placé le template et le code du composant dans un même fichier
- En principe on décompose le template, ses styles, et le code dans trois fichiers différents 
 
<div class="twocolumn">

<div>

- app.component.ts 

```typescript
@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
    compteur = 0
}
```

</div>
<div>

- app.component.html
```typescript
@Component({
  <p> {{ compteur }} </p>
}
```

- app.component.css
```css
.rouge {
    color: red
}
```

</div>

</div>
---

#### Placer son code dans des services injectables

- On peut créer des classes *services* injectables dans les composants qui en ont besoin.
- Exemple avec un service permettant d'obtenir les données d'une liste de personnes : 

<div class="twocolumn">

<div>

```typescript
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {
  personnes = [
   { id: 1, nom: "Stark", prenom: "Ned"},
   { id: 2, nom: "Baratheon", prenom: "Cercei"},
   { id: 3, nom: "Lanister", prenom: "Jaime"}]

  getPersonnes() {
    return this.personnes;
  }
}
```

</div>
<div>

```typescript
import {Component, inject} from '@angular/core';
import {PersonneService} from './car.service';

@Component({
  selector: 'app-root',
  template: `
    <p>Personnes : {{ display }}</p>`,
  standalone: true,
})
export class AppComponent {
  display = '';
  // ici, on injecte le service 
  personneService = inject(PersonneService);

  constructor() {
   this.display = this.personneService
      .getPersonnes()
      .map(p => p.prenom + " " + p.nom)
      .join(", ")
  }
}
```
</div>
</div>
---
#### Faites des requêtes web dans un service (1/2)

- Les services sont le bon endroit où exécuter les requêtes réseaux. 
- C'est l'objet `HttpClient` qui permet de faire des requêtes HTTP.
- L'appel est asynchrone, l'objet retourné est un *observable* auquel on s'abonne pour obtenir le résultat plus tard.

- Pour pouvoir utiliser HttpClient dans un service, il faut qu'il soit *produit* par le fichier `main.ts`

```typescript
import { provideHttpClient } from '@angular/common/http';

boostrapApplication(App, {providers: [
  provideHttpClient(),
]});
```

---
#### Faites des requêtes web dans un service (2/2)

- On injecte ensuite `HttpClient` dans le service et on injecte le service dans les composants qui en ont besoin.

<div class="twocolumn">

<div>

```typescript
@Injectable({
    providedIn: 'root'
})
export class CarService {

  http = inject(HttpClient)
  
  // retourne un observable d'un objet 
  // de type { data: string }
  getTexte() {
   return this.http.get<{ data: string }>
     ('https://meowfacts.herokuapp.com/')
        
  }
}
```

</div>
<div>

```typescript
Component({
  selector: 'app-root', 
  template: '{{ texte }}',
  standalone: true
})

// export component
export class AppComponent {
  carService = inject(CarService);
  texte = ""
  
  constructor() {
    this.carService.getTexte()
     // on s'abonne à l'observable
     // pour récupérer le résultat
     .subscribe(resp => {
        this.texte = resp.data
      })
  }
}
```
</div>

</div>
---

#### Références

- Tutorial interactif et nouvelle documentation Angular : 
  - https://angular.dev/

- Les IDE pour Angular : 
<ul>
<li> Webstorm <img class="myicon" src="backgrounds/webstorm.png"/> ou IntelliJ Ultimate <img class="myicon" src="backgrounds/IntelliJ.png"/></li>
<li> Visual Studio Code <img class="myicon" src="backgrounds/vscode.png"></li>
<li> SublimeText <img class="myicon" src="backgrounds/sublimetext.png"></li>
</ul>






