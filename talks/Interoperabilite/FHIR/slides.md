##### La norme FHIR

![logo](backgrounds/image.png)  <!-- .element height="50%" width="50%" -->

*Nicolas Singer*
---

#### FHIR ? 
- Standard pour structurer et échanger des données patients
- basé sur les technologies du Web (REST, XML, JSON)
- Définit des formats, des ressources, et des API pour l'échange d'information médicale

---
#### Historique

![logo](backgrounds/historique.jpg)  <!-- .element height="50%" width="50%" -->
- HL7v2 en 1987, format texte, encore utilisé dans beaucoup d'établissements

```text
 MSH|^~\&|GHH LAB|ELAB-3|GHH OE|BLDG4|200202150930||ORU^R01|CNTRL-3456|P|2.4<cr>
 PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320|F|||153 FERNWOOD DR.^
 ^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520<cr>
 OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||
 555-55-5555^PRIMARY^PATRICIA P^^^^MD^^|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD<cr>
 OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F<cr>
 ```

- HL7v3 trop lent et trop dur à apprendre (seule CDA a eu du succès)

---

#### l'acronyme FHIR

- F : Fast (à implémenter)
  - Exemple de codes et serveurs de test
- H : Health
- I : Interopérable
- R : Ressources 
  - Elles sont les briques de base servant à la construction des modèles
  - Reliées entre elles par des URLs
  - Sont échangées entre les systèmes via des API
---

#### Reprise des protocoles du web 
- Les instances de documents sont partagées en XML ou en JSON
- Des API Web sont utilisés pour connecter les systèmes
- Utilise HTTPS, OAUth, etc. pour la sécurité
---

#### Les ressources FHIR
- Chaque ressource est 
  - une unité logique d'échange
  - a une signification dans le monde médical
  - appartient à une catégorie
  - a un degré de maturité selon la progression de la norme (1-5, et N quand stable)
- Exemples : 
  - Administratif
    - Patient, Médecin, Organisation, Localisation, Facture
  - Médical 
    - Allergies, Santé, Antécédents médicaux, Traitment
  - Infastructure
    - Document, Message, Profil

- https://hl7.org/fhir/resourcelist.html
---

#### Structure d'une ressource
- Les ressources ont trois parties

  ![logo](backgrounds/ressource.jpg)  <!-- .element height="50%" width="50%" -->
---

#### Exemple avec un patient

```xml
<Patient xmlns="http://hl7.org/fhir">
  <id value="example"/> 
  <text> 
    <status value="generated"/> 
    <div xmlns="http://www.w3.org/1999/xhtml">
      Madame Emilie Pascaux, 9 rue de la tuilerie à Castres, joignable au 06 72 83 12 84.
    </div>
  </text>
  <name> 
    <use value="official"/> 
    <family value="Pascaux"/> 
    <given value="Emilie"/> 
  </name> 
  <telecom> 
    <system value="phone"/> 
    <value value="(33) 6 72 83 12 84"/> 
    <use value="mobile"/> 
    <rank value="1"/> 
  </telecom> 
  <gender value="female"/> 
  <birthDate value="1974-12-25">
    <extension url="http://hl7.org/fhir/StructureDefinition/patient-birthTime">
      <valueDateTime value="1974-12-25T14:35:45-05:00"/> 
    </extension> 
  </birthDate> 
  <deceasedBoolean value="false"/> 
</Patient> 
```
---

#### Documentation des ressources
Pour chaque ressource, la documentation spécifie : 
 - Le contexte et des notes sur l'usage
 - La structure de la ressource (en UML, XML, et JSON)
 - La terminologie liée (les codes utilisables par exemple pour le genre, le statut, le language, ...)
 - Les paramètres de recherche de l'interface REST
 - Des exemples 
---
#### Exemple pour la ressource Patient (UML)
![logo](backgrounds/umlpatient.jpg)  

---
#### Exemple pour la ressource Patient (Schéma XML)

```xml
<xs:schema targetNamespace="http://hl7.org/fhir" elementFormDefault="qualified" version="1.0">
<xs:include schemaLocation="fhir-base.xsd"/>
<xs:element name="Patient" type="Patient">
<xs:annotation>
<xs:documentation xml:lang="en">
Demographics and other administrative information about an individual or animal receiving care 
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:complexType name="Patient">
<xs:annotation>
<xs:documentation xml:lang="en">
If the element is present, it must have either a @value, an @id, or extensions
</xs:documentation>
</xs:annotation>
<xs:complexContent>
<xs:extension base="DomainResource">
<xs:sequence>
<xs:element name="identifier" minOccurs="0" maxOccurs="unbounded" type="Identifier">
<xs:annotation>
<xs:documentation xml:lang="en">An identifier for this patient.</xs:documentation>
</xs:annotation>
</xs:element>
...
```
---

#### Relations entre ressources
- Les ressources peuvent se référencer
 
  ![logo](backgrounds/relations.svg)  <!-- .element height="60%" width="60%" -->

- Pour cela elles ont un identifiant et on peut établir des références à ces indentifiants.
---

#### Types d'identifiant
- Identifiant logique
  - Assigné par l'entrepôt de données (serveur)
- Identifiant métier
  - Assigné par le domaine (ex: numéro INSEE) 
- La ressource est accessible via un URL qui utilise cet identifiant
  - ex: http://test.fhir.org/rest/Patient/123, ici 123 représente l'identifiant logique d'un patient
---

#### Types de références
- Quand la ressource est exposée publiquement, la référence est un URL qui peut-être absolu ou relatif  
- Sinon c'est une valeur (par exemple le numéro INSEE du patient) 

```xml
<!-- relatif -->
<subject>
  <reference value="Patient/034AB16" />
</subject>

<!-- absolu avec type de la ressource cible-->
<target>
  <reference value="http://example.org/fhir/Observation/history/2" />
  <type>Patient</type>
</target>

<!-- Référence logique avec type de la ressource cible  -->
<owner>
  <identifier value="1 143 234 53 123 57" />
  <type>Patient</type>
</owner>
```
---

#### Types de données (1/2)
- Basés sur les schémas XML et les types ISO
- Voici les primitives
 
  ![logo](backgrounds/primitive.jpg)  <!-- .element height="80%" width="80%" -->
---

#### Types de données (2/2)
- Voici les données générales de base
 
  ![logo](backgrounds/generaldata.jpg)  
---

#### Le type Element 

```xml
<xs:complexType name="Element">
    <xs:annotation>
        <xs:documentation xml:lang="en">Base definition for all elements in a resource.</xs:documentation>
        <xs:documentation xml:lang="en">If the element is present, it must have a value 
          for at least one of the defined elements, an @id referenced from the Narrative, or extensions</xs:documentation>
    </xs:annotation>
    <xs:sequence>
        <xs:element name="extension" type="Extension" minOccurs="0" maxOccurs="unbounded">
             <xs:annotation>
                <xs:documentation xml:lang="en">
                  May be used to represent additional information 
                  that is not part of the basic definition of the element. 
                  To make the use of extensions safe and manageable, there is a strict set of governance  
                  applied to the definition and use of extensions. Though any implementer can define 
                  an extension, there is a set of requirements that SHALL be met as part of 
                  the definition of the extension.
                </xs:documentation>
            </xs:annotation>
        </xs:element>
    </xs:sequence>
    <xs:attribute name="id" type="string-primitive" use="optional"/>
</xs:complexType>

<xs:simpleType name="string-primitive">
<xs:restriction base="xs:string">
  <xs:minLength value="1"/>
</xs:restriction>
</xs:simpleType>
```
---

#### Les autres types se basent sur Element

```xml
<xs:complexType name="integer">
    <xs:complexContent>
        <xs:extension base="Element">
            <xs:attribute name="value" type="integer-primitive" use="optional"/>
        </xs:extension>
    </xs:complexContent>
</xs:complexType>

<xs:simpleType name="integer-primitive">
    <xs:restriction base="xs:int">
        <xs:pattern value="-?([0]|([1-9][0-9]*))"/>
    </xs:restriction>
</xs:simpleType>
```

---

#### Descriptions non structurées
- Les ressources peuvent comporter une représentation non structurée de leur contenu à destination d'un lecteur
humain.
- C'est une question de sécurité médicale
  - Si le système ne comprend pas la structure, on se rabat sur la description humaine.
- La partie narrative comprend un statut et le texte proprement dit au format html
```xml
<Patient xmlns="http://hl7.org/fhir">
  <text>
    <status value="generated"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <p>Une description non structurée du patient...</p>
    </div>
  </text>
</Patient>
```

---

#### Extensions
- Comment étendre les structures quand elles sont figées par la norme ?
  - Dans tous les standards médicaux, il y a une grande variabilité entre les pays, les institutions, 
les spécialités médicales...
- En FHIR, tout élément peut être étendu. 
- Ces extensions doivent être déposées et validées 
- La gouvernance des extensions dépend de leur niveau
  - extension locale à un projet
  - extension spécifique à un domaine (ex: cardiologie)
  - extension conforme à un standard national
  - extension officielle publiée par HL7
---

#### Extensions (exemple 1/2)
- On définit une structure, le nom de son type et on la publie. 

<table style="width: 1000px; border: 2px solid black">
<tr><th> Champ </th><th> Format </th></tr>
<tr><td> Nom</td><td>EyeColor</td></tr>
<tr><td> Contexte</td><td>Ressource Patient </td></tr>
<tr><td> Definition</td><td>La couleur des yeux du patient </td></tr>
<tr><td> Cardinalité</td><td>0..1</td></tr>
<tr><td> Type</td><td>String</td></tr>
<tr><td> Valeurs possibles</td><td>blue, brown, hazel, green, mixed</td></tr>
</table>
---

#### Extensions (exemple 2/2)
- Une fois publiée, on peut ensuite s'en servir 
```xml
<Patient xmlns="http://hl7.org/fhir">
  <extension url="http://hl7.org/fhir/patient#eyecolor" >
    <valueCode value="blue" />
  </extension>
</Patient>
```






