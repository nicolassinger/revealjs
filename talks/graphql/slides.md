##### GraphQL

![logo](backgrounds/GraphQL_Logo.svg.png)  <!-- .element height="20%" width="20%" -->

<https://graphql.org/>

Nicolas Singer


#### GraphQL est un langage de requête pour les API 

- alternative à REST
- créé par Facebook en 2012
- permet de décrire les données de l'API (schéma)
- permet au client de définir uniquement les données qu'il veut récupérer 
  - Résout l'*over-fetching*
- permet d'agréger les résultats de plusieurs requêtes
  - pour résoudre l'*under-fetching* sans utiliser plusieurs requêtes


#### L'*over-fetching*
C'est récupérer plus de données qu'il n'en faut
- Exemple: Soit un point d'accès REST qui retourne une promotion d'étudiants au format suivant : 
```json
    "promotion": {
      "nom": "La promotion du futur",
      "annee": 2033,
      "etudiants": [
        {
          "nom": "Carlier",
          "prenom": "Cerise"
        },
        {
          "nom": "Meunier",
          "prenom": "Jérome"
        }
      ]
    }
```
- Dommage d'avoir toute la liste des étudiants si le client ne s'intéresse qu'aux propriétés de la promotion.



### L'*over-fetching*
- Avec REST on résoudrait le problème en créant un autre point d'accès qui retournerait uniquement la promotion

<table>
<tr><td class="large">

```
GET /promotionLight
```
```json
    "promotion": {
      "nom": "La promotion du futur",
      "annee": 2033,
    }
```
</td><td class="large">

```
GET /promotion
```
```json
    "promotion": {
      "nom": "La promotion du futur",
      "annee": 2033,
      "etudiants": [
        {
          "nom": "Carlier",
          "prenom": "Cerise"
        },
        {
          "nom": "Meunier",
          "prenom": "Jérome"
        }
      ]
    }
```
</td></tr>
</table>

- Ok sur un cas simple comme celui-ci mais quand les données sont plus complexes, cela peut mener à la multiplication des
urls



### L'*under-fetching*
- Soit un point d'accès REST qui renvoie les données d'un étudiant en fonction de son id 

```
GET /etudiant/{id}`
```
```json
{ "nom": "Carlier", "prenom": "Cerise" }
```
- Si on veut l'étudiant 1 et l'étudiant 2, on est obligé d'appeler deux fois le point d'accès :
```
  GET /etudiant/1
  GET /etudiant/2
```
- C'est l'*under-fetching*. On aurait préféré une seule requête pour récupérer d'un coup les deux étudiants.


### Principe de base de GraphQL
- Soit des données dont le format est décrit dans un langage de programmation quelconque (ici **Kotlin**) 
```kotlin
class Promotion(val nom: String, val année: Int, val etudiants: List<Etudiant>) 
class Etudiant(val id: String, val nom: String, val prénom: String)
class Adresse(val rue: String, val codePostal: Int, val ville: String)
```
- et deux opérations implémentées coté serveur qui retournent respectivement une promotion et un étudiant

```kotlin
    @Query("promotion")
    fun getPromotions(): Promotion {
        // on va chercher la promotion dans la base 
        return bd.getPromotion()
    }

    @Query("etudiant")
    fun getEtudiant(id: Int): Etudiant? {
        // on va chercher l'étudiant dans la base en fonction de son id
        return bd.getEtudiantById(id)
    }
```


### Principe de base
- GraphQL utilise un schéma qui va servir de base aux requêtes:

<table> <tr><td>

```graphql
type Promotion {
  annee: Int!
  etudiants: [Etudiant]
  nom: String
}
```
</td>
<td>

 ```graphql
type Etudiant {
  adresse: Adresse
  id: Int!
  nom: String
  prenom: String
} 
 ```
</td>
<td>

 ```graphql
type Adresse {
  codePostal: Int!
  rue: String
  ville: String
}
 ```
</td></tr>
<tr><td colspan="3">

```graphql
type Query {
etudiant(id: Int!): Etudiant
promotion: Promotion
}
```
</td></tr>
</table>



### Les requêtes 

Elles permettent d'interroger le schéma en précisant ce que l'on veut obtenir

<table> 
<tr><th>Requête</th><th>Réponse</th></tr>
<tr><td>

```graphql
{
  promotion {
    nom
  }
}
```
</td><td class="large">

```graphql
 {
  "data": {
    "promotion": {
      "nom": "La promotion du futur"
    }
  }
}
```

</td></tr>
</table>


### Les requêtes 

<table> 
<tr><th> Requête</th></th><th>Réponse</th></tr>
<tr><td>

```graphql
{
  promotion {
    nom
    annee
  }
}
```
<td class="large">

```graphql
{
  "data": {
    "promotion": {
      "nom": "La promotion du futur",
      "annee": 2033
    }
  }
}
```

</td></tr>
</table>


### Les requêtes 

<table> 
<tr><th> Requête</th></th><th>Réponse</th></tr>
<tr><td>

```graphql
{
  promotion {
    nom
    annee
    etudiants {
      nom
      prenom
    }
  }
}
```
<td class="large">

```graphql
{
  "data": {
    "promotion": {
      "nom": "La promotion du futur",
      "annee": 2033,
      "etudiants": [
        {
          "nom": "Carlier",
          "prenom": "Cerise"
        },
        {
          "nom": "Meunier",
          "prenom": "Jérome"
        }
      ]
    }
  }
}
```

</td></tr>
</table>


### Les requêtes

<table> 
<tr><th> Requête</th></th><th>Réponse</th></tr>
<tr><td>

```graphql
{
  promotion {
    etudiants {
      id
      adresse {
        rue
        ville
      }
    }
  }
}
```
<td class="large">

```graphql
{
  "data": {
    "promotion": {
      "etudiants": [
        {
          "id": 1,
          "adresse": {
            "rue": "9 rue Henri Salvador",
            "ville": "Toulouse"
          }
        },
        {
          "id": 2,
          "adresse": {
            "rue": "17 rue Jacque Brel",
            "ville": "Castres"
          }
        }
      ]
    }
  }
}
```

</td></tr>
</table>


### Les requêtes avec arguments
- on précise l'argument et sa valeur entre parenthèses après le type

<table> 
<tr><th> Requête</th></th><th>Réponse</th></tr>
<tr><td>
<code><pre>
{
  etudiant(<span class="surligne">id: 1</span>) {
    id
    adresse {
      rue
      ville
    }
  }
}</pre></code>
<td class="large">

```graphql
{
  "data": {
    "etudiant": {
      "id": 1,
      "adresse": {
        "rue": "9 rue Henri Salvador",
        "ville": "Toulouse"
      }
    }
  }
}
```

</td></tr>
</table>


### Composition de requêtes

<table> 
<tr><th> Requête</th></th><th>Réponse</th></tr>
<tr><td class="medium">

```graphql
{
  promotion {
    nom
  }
  etud1: etudiant(id: 1) {
    nom
  }
  etud2: etudiant(id: 2) {
    nom
  }
}
```
<td class="large">

```graphql
{
  "data": {
    "promotion": {
      "nom": "La promotion du futur"
    },
    "etud1": {
      "nom": "Carlier"
    },
    "etud2": {
      "nom": "Meunier"
    }
  }
}
```

</td></tr>
</table>


### Les fragments

- Les fragments permettent de nommer une seule fois un ensemble de propriétés que l'on veut récupérer 
    pour s'y référer plus tard. <!-- .element: class="smaller" -->
- <!-- .element: class="smaller" --> 
On définit un fragment avec le mot clé `fragment` et on y fait référence dans les requêtes avec la notation
...*nom_du_fragment* 

<table> 
<tr><td class="large">

```graphql
{
  promotion {
    nom
  }
  etud1: etudiant(id: 1) {
    ...etudiantProps
  }
  etud2: etudiant(id: 2) {
    ...etudiantProps
  }
}

fragment etudiantProps on Etudiant {
  nom
  prenom
  adresse {
    codePostal
  }
}
```
<td class="large">

```graphql{
  "data": {
    "promotion": {
      "nom": "La promotion du futur"
    },
    "etud1": {
      "nom": "Carlier",
      "prenom": "Cerise",
      "adresse": {
        "codePostal": 31000
      }
    },
    "etud2": {
      "nom": "Meunier",
      "prenom": "Jérome",
      "adresse": {
        "codePostal": 81100
      }
    }
  }
}
```
</td></tr>
</table>


### Nom de l'opération
- On peut donner un nom aux opérations et c'est même recommandé. 
- Dans ce cas on indique le type de l'opération (ici *query* car il s'agit d'une requête) et on donne ensuite son nom.
- Voici par exemple la version complète de la requête retournant le nom de la promotion :

<pre>
<span class="surligne">query getPromotion</span> 
{
  promotion {
    nom
  }
}
</pre>


### Opérations paramétrées et variables 
- On peut paramétrer une opération avec une ou plusieurs variables.
- Les variables se déclarent dans le nom de l'opération
- on s'y réfère ensuite dans la requête

<pre>

query getEtudiant(<span class="surligne">$id: Int!</span>) {
  etudiant(<span class="surligne">id: $id</span>) {
    nom
  }
}

</pre>

- le client GraphQL permet ensuite d'appeler la requête en précisant la valeur de la variable


### Opérations paramétrées et directives
- On peut aussi paramétrer les opérations pour définir ce qu'elles doivent renvoyer
- les directives `@include` et `@skip` permettent d'inclure ou de couper des propriétés en fonction de la valeur d'une variable booléenne.
- Par exemple, soit la requête : 

```graphql
query getPromotion($avecEtudiants: Boolean = true) {
    promotion {
        annee
        etudiants @include(if: $avecEtudiants) {
            nom
            prenom
        }   
    }
}
```


### Réponses : 

<table>
<tr>
<td><code>$avecEtudiants=true</code></td><td><code>$avecEtudiants=false</code></td>
</tr> <tr>
<td class="large">

```graphql
{
  "data": {
    "promotion": {
      "annee": 2033
      "etudiants": [
        {
          "nom": "Carlier",
          "prenom": "Cerise"
        },
        {
          "nom": "Meunier",
          "prenom": "Jérome"
        }
      ]
    }
  }
}
```
</td><td class="large">

```graphql
{
  "data": {
    "promotion": {
      "annee": 2033
    }
  }
}
```

</td></tr>
</table>


### Les mutations
- Les requêtes (query) ne sont pas les seules opérations possibles avec GraphQL
- Les *mutations* permettent de modifier ou d'ajouter des données.
- Ce sont en réalité des appels de procédures distants dont les retours sont des éléments du schéma. 
Le fait de les appeler *mutation* n'est qu'une convention pour indiquer que ces opérations ont des effets de bords.
- Ajoutons par exemple au serveur une méthode qui permet d'ajouter un étudiant dans la promotion :
```kotlin
@Mutation("addEtudiant")
fun addEtudiant(etudiant: Etudiant): Etudiant {
    bd.insertEtudiant(etudiant)
    return etudiant
}
```



### Les mutations
- Suite à cet ajout, une nouvelle opération apparaît dans le schéma GraphQL: 
```graphql
type Mutation {
  addEtudiant(etudiant: Etudiant): Etudiant
}
```
On peut l'appeler ainsi :
```graphql
mutation addEtudiant {
    addEtudiant(
        etudiant: {id: 0, nom: "Delbos", prenom: "Marine", 
            adresse: {rue: "58 rue Serpentine", codePostal: 81100, ville: "Castres"}}) {
        id
        nom
        prenom
    }
}
```


### Les mutations
- Dans un cas réel on passe l'étudiant en paramètre avec une variable:
<table>
<tr><td class="large">

```graphql
mutation addEtudiant($e: Etudiant) {
  addEtudiant(etudiant: $e)
   {
    id
    nom
    prenom
  }
}
```
</td><td class="large">

```json
{  
    "e": { 
        "id": 0, 
        "nom": "Delbos", 
        "prenom": "Marine", 
        "adresse": 
          { "rue": "58 rue Serpentine", 
            "codePostal": 81100, 
            "ville": "Castres"} 
    }
}
```
</tr></td>
</table>



### Les mutations
- Tout comme pour les requêtes, on peut exécuter plusieurs mutations en une seule opération :
<table>
<tr><td class="large">

```graphql
mutation addEtudiant( 
    $e: Etudiant, 
    $f: Etudiant) {
  etud1: addEtudiant( etudiant: $e)
   {
    id
    nom
    prenom
  }
  etud2: addEtudiant( etudiant: $f)
   {
    id
    nom
    prenom
  }
}
```
</td><td class="large">

```json
{
  "data": {
    "etud1": {
      "id": 8,
      "nom": "Delbos",
      "prenom": "Marine"
    },
    "etud2": {
      "id": 9,
      "nom": "Delmas",
      "prenom": "Nicolas"
    }
  }
}
```
</tr></td>
</table>


### Schéma GraphQL: Les interfaces 
- une interface est une notation qui précise tous les champs qu'un type peut avoir. Les objets implémentant cette interface doivent
avoir tous ces champs, mais peuvent en avoir d'autres.
<table>
<tr><td>

```graphql
interface Adresse {
    rue: String
    codePostal: Int
    ville: String
}
```
</td></tr><tr>
<td class="large">

```graphql
type AdresseWithTel implements Adresse {
  rue: String
  codePostal: Int
  ville: String
  tel: String
}
```
</td><td class="large">

```graphql
type AdresseWithEmail implements Adresse {
rue: String
codePostal: Int
ville: String
email: String
}
```
</tr></td>
</table>


### Les interfaces 
- Avec un étudiant du type `AdresseWithEmail` et un autre de type `AdresseWithTel`, 
si on interroge les champs communs aux deux types d'adresse, tout va bien.
<table>
<tr><td>

```graphql
promotion {
    nom
    etudiants {
        nom
        prenom
        adresse {
            rue
        }
    }
}
```
</td><td class="large">

```json
{
    "promotion": {
      "nom": "La promotion du futur",
      "etudiants": [
        {
          "nom": "Carlier",
          "prenom": "Cerise",
          "adresse": {
            "rue": "9 rue Henri Salvador"
          }
        },
        {
          "nom": "Meunier",
          "prenom": "Jérome",
          "adresse": {
            "rue": "17 rue Jacque Brel"
          }
        }
      ]
    }
}
```
</td>
</tr>
</table>


### Les interfaces 
- Mais si on interroge sur un champ qui diffère, sans surprise, on a une erreur
<table>
<tr><td>

```
promotion {
    nom
    etudiants {
        nom
        prenom
        adresse {
            rue
            email
        }
    }
}
```
</td><td class="large petitcode">

```json
{
  "errors": [
    {
   "message": "Cannot query field email on type Adresse,
   Did you mean to use an inline fragment on AdresseWithEmail?"
   }
}
```
</td>
</tr></td>
</table>



### Les interfaces 
- La solution est d'utiliser la notation `... on <type>` qui permet de sélectionner les champs en fonction du type réel. 
- Pour afficher le type réel on utilise le champ spécial *`__typename`* <!-- .element: class="smaller" -->
<table>
<tr><td class="large">

``` 
promotion {
    nom
    etudiants {
      nom
      prenom
      adresse {
        rue
        ... on AdresseWithEmail {
          email
        }
        ... on AdresseWithTel {
          tel
        }
      }
    }
}
```
</td><td class="large petitcode">

```json
{
    "promotion": {
      "nom": "La promotion du futur",
      "etudiants": [
        {
          "nom": "Carlier",
          "prenom": "Cerise",
          "adresse": {
            "__typename": "AdresseWithEmail",
            "rue": "9 rue Henri Salvador",
            "email": "Cerise.Carlier@gmail.com"
          }
        },
        {
          "nom": "Meunier",
          "prenom": "Jérome",
          "adresse": {
            "__typename": "AdresseWithTel",
            "rue": "17 rue Jacque Brel",
            "tel": "0945125421"
          }
        }
      ]
    }
}
```
</td>
</tr></td>
</table>


### Conclusion
- Le positif
  - Avec GraphQL, le client peut définir précisément les données qu'il souhaite obtenir, et *packager* plusieurs requêtes en une seule
  opération
  - Les schémas diffusés par le serveur permettent au client de naviguer parmi les ressources et les opérations disponibles
- Le négatif
  - GraphQL reste plus complexe à implémenter que REST, aussi bien coté serveur que client.
  - La flexibilité des requêtes pour le client peut avoir un coût du côté du serveur dont le travail est d'aller chercher les 
  données demandées.


<section data-background-image="backgrounds/rest-api-icon.jpg" data-background-size="10%" data-background-position="top left">

### REST vs GraphQL 
  - Pour des données simples et peu imbriquées, REST reste le choix par défaut, c'est le protocole utilisé par la majorité des API. 
  - Pour des données plus complexes avec de nombreux niveaux imbriqués, GraphQL est à considérer.
</section>


 <!-- .slide: class="mysmall" -->
### Références 
- A la bibliothèque
  - Wieruch, Robin. *The Road to GraphQL: With React Class Components*. 2018. 
- En ligne
  - Site Officiel: <https://graphql.org/>
  - GraphQL, client et serveur en Javascript, la bibliothèque Apollo 
    - <https://www.apollographql.com/>
  - GraphQL, serveur java avec Quarkus 
    - <https://quarkus.io/guides/smallrye-graphql>
