##### Les listes dans Jetpack Compose

![logo](backgrounds/compose.png)  <!-- .element height="20%" width="20%" -->

Développement Android

*Nicolas Singer*
---
### Comment afficher des listes de composants dans Jetpack Compose ?
- Par le code, mais pas très efficace car le composant est généré même s'il n'est pas visible
```kotlin
@Composable
fun Screen() {
    Column() {
        (1..10).forEach() {
            Image(
                painterResource(R.drawable.fleur),
                contentDescription = "Des fleurs",
            )
        }
    }
}
```
---
#### Avec les Lazy lists et les lazy grids
- Ces composants ne dessinent leurs enfants que quand ils sont visibles
- Quatre composants permettent cela :  `LazyColumn`, `LazyRow`, `LazyVerticalGrid` et `LazyHorizontalGrid`
```kotlin
@Composable
fun Screen() {
    LazyColumn() {
        items(10) {
            Image(
                painterResource(R.drawable.fleur),
                contentDescription = "Des fleurs",
            )
        }
    }
}
```
---

#### Définition des enfants 
- On peut ajouter un enfant unique avec `item`: 
```kotlin
LazyColumn() {
        item {
            Image(
                painterResource(R.drawable.fleur),
                contentDescription = "Des fleurs",
            )
        }
    }
```
- ou plusieurs avec `items` et une liste d'objets:
```kotlin
import androidx.compose.foundation.lazy.grid.items
val mots =  List(100) { index -> "hello" + index}
    LazyColumn {
        items(mots) { mot ->
            Text(text = mot)
        }
    }
```

---
#### Exemple en vidéo

<video data-autoplay loop src="videos/lazycolumns.mp4"></video>
---

#### Références

- Documentation officielle sur les listes de composants avec Jetpack Compose 
  - https://developer.android.com/jetpack/compose/lists



