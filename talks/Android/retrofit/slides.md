##### Retrofit

![logo](backgrounds/logo.jpg)  <!-- .element height="50%" width="50%" -->

Une bibliothèque pour accéder à une API HTTP sous la forme d'une interface Kotlin

*Nicolas Singer*
---

#### Sans Retrofit
1. On effectue une requête HTTP vers l'URL de l'API avec :
   - [OkHttp](https://square.github.io/okhttp/)
   - [Volley](https://google.github.io/volley/)
   
2. On traduit le résultat JSON en objet Java ou Kotlin avec :
   - [Gson](https://github.com/google/gson)
   - [Moshi](https://github.com/square/moshi)
   - [KotlinX Serialization](https://kotlinlang.org/docs/serialization.html#example-json-serialization)

> Retrofit permet cela de façon plus pratique.
---
#### Principe de Retrofit (1)
1. Définition des types Kotlin correspondant aux résultats JSON de l'API

<div class="twocolumn">

```json
{
  "page": 1,
  "results": [
    {
      "backdrop_path": "/pHs6YIRIPEHnObtJ1y6U.jpg",
      "genre_ids": [
        18,
        878
      ],
      "id": 62215,
      "original_title": "Melancholia",
      "overview": "Descriptif du film",
      "poster_path": "/fMneszMiQuTKY8X5vwqJf.jpg",
      "release_date": "2011-05-26",
      "title": "Melancholia"
    }
  ]
}
```
<div>

```kotlin
class TmdbMovieResult(
  var page: Int = 0, 
  val results: List<TmdbMovie> = listOf())

class TmdbMovie(
  var overview: String = "", 
  val release_date: String = "", 
  val id: String = "", 
  val title: String = "", 
  val original_title: String = "", 
  val backdrop_path: String? = "", 
  val genre_ids: List<Int> = listOf(),
  val poster_path: String? = "")
```

</div>
<div>
</div>
</div>
---

#### Principe de Retrofit (2)

<ol start="2">
<li> Annotation dans l'interface pour lier l'URL de l'API à l'interface Java

```kotlin
interface Api {
   @GET("trending/movie/week")
   suspend fun lastmovies(@Query("api_key") api_key: String): TmdbMovieResult
}
``` 
</li>
<li> Implémentation de l'interface

```kotlin
  val retrofit = Retrofit.Builder()
   .baseUrl("https://api.themoviedb.org/3/")
   .addConverterFactory(MoshiConverterFactory.create())
   .build();

  val api = retrofit.create(Api::class.java)
  // à partir de là, on peut appeler api.lastMovies(...)
```
</li>
</ol>
---

#### Utilisation
- On appelle les méthodes de l'api dans le ViewModel
- Comme c'est une methode *suspend* il faut l'appeler dans un `viewModelScope`
- En général le résultat est placé dans une variable observable comme un `MutableStateFlow` kotlin.

```kotlin
class MainViewModel : ViewModel() {
  val movies = MutableStateFlow<List<TmdbMovie>>(listOf())

  fun getMovies() {
    viewModelScope.launch {
      movies.value = api.lastmovies(api_key)
    }
  }
}
```
---

#### Récapitulation 

- Dans un fichier `models.kt` on définit les classes kotlin correspondantes aux résultats de l'API.
- Dans un fichier `api.kt`, on définit l'interface annotée.
- Dans le ViewModel, on crée le service Retrofit (l'implémentation de l'interface) et on appelle ses méthodes.
---

#### Focus sur les annotations de liaison
<ul>
<li>

Paramètres des URLS (*query parameters*)
```kotlin 
@GET("search/movie")
suspend fun searchmovies(@Query("api_key") api_key: String, 
  @Query("query") searchtext: String): TmdbMovieResult
```

<div class="twocolumn">
<div>
Appel

```kotlin
searchmovies(123, "stars")
```
</div>
<div>
Url généré
<div class="mysmaller">

```http
/search/movie?query=stars&api_key=123
```
</div>
</div>
</div>
</li>
<li>

URLs avec parties variables
```kotlin
@GET("movie/{id}")
suspend fun movieDetails(@Path("id") id: String): TmdbMovie
```
<div class="twocolumn">
<div>

Appel

```kotlin
movieDetails("x67") 
```
</div>
<div>

Url généré

```http
/movie/x67
```
</div>
</div>
</li>
</ul>
---
#### Références
- Site officiel de Retrofit 
   - https://square.github.io/retrofit/
- Dépendances à inclure au projet Android
```gradle
implementation 'com.squareup.retrofit2:retrofit:2.9.0'
implementation 'com.squareup.retrofit2:converter-moshi:2.9.0'
```
