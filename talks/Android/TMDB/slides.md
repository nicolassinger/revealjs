##### l'API TMDB

![logo](backgrounds/accueil.jpg)  <!-- .element height="60%" width="60%" -->

*Nicolas Singer*
---
#### TMDB est une base de données sur les films et les séries
 - Accessible au moyen d'une API 
 - Permet de récupérer les données brutes et structurées 
  
   ![image](backgrounds/strangerthings.jpg)  <!-- .element height="60%" width="60%" -->

Voyons comment est architecturée cette API.
---

#### La clé d'API
- Pour pouvoir utiliser l'API il faut obtenir une clé d'accès.
- On l'obtient en s'enregistrant sur le site de TMDB puis en demandant une clé :  
  - https://www.themoviedb.org/settings/api
  
 ![logo](backgrounds/key.jpg)  <!-- .element height="60%" width="60%" -->

---

#### Construction des URLs pour l'API (1)
- Tous les urls doivent commencer par :
> https://api.themoviedb.org/3/

- On y ajoute une partie qui dépend de la requête à réaliser, par exemple : 
  - pour chercher un film par mots clés : `/search/movie`
  - pour chercher une série par mots clés : `/search/tv`

- Certains chemins intègrent un paramètre
  - par exemple pour obtenir les détails d'un film dont
on connait l'identifiant, le chemin est : `/movie/{movie_id}`
---

#### Construction des URLs pour l'API (2)
- Souvent on ajoute des paramètres à la fin de l'URL
- Il faut toujours spécifier la clé d'API avec le paramètre `api_key`.
- Les autres paramètres sont décrits dans la documention
- Par exemple pour chercher un film par mot clé, le mot clé s'exprime avec le paramètre `query`.
- Si on veut donc faire une recherche sur les films ayant pour mot clé *melancholia* et que notre clé d'API 
est 123 on écrira : 

> https://api.themoviedb.org/3/search/movie?api_key=123&query=melancholia
---

#### Construction des URLs pour l'API (3)
- Pour trouver tous les paramètres, il faut savoir lire la documentation
 
  ![logo](backgrounds/apisearch.jpg)  <!-- .element height="40%" width="40%" -->

- Par exemple, le paramètre `language` permet de spécifier la langue du résultat: https://api.themoviedb.org/3/search/movie?api_key=123&query=melancholia&language=fr
---

#### Format du résultat 
- Le résultat est exprimé au format JSON
- Son format exact dépend de la requête et est décrit dans la documentation.
- Par exemple voici ce qui est retourné pour la requête précédente :
```json
{ "page":1,
  "results":[
    { "adult":false,
      "backdrop_path":"/pHs6YIRIPEHnnersHlvObtJ1y6U.jpg",
      "genre_ids":[18,878],"id":62215,
      "original_language": "en",
      "original_title": "Melancholia",
      "overview": "À l'occasion de leur mariage, Justine et Michael donnent une 
      somptueuse réception dans la maison de la soeur de Justine et de son beau-frère. 
      Pendant ce temps, la planète Melancholia se dirige vers la Terre…",
      "popularity":14.723,
      "poster_path":"/fMneszMiQuTKY8JUXrGGB5vwqJf.jpg",
      "release_date":"2011-05-26",
      "title":"Melancholia",
      "video":false,
      "vote_average":7.2,
      "vote_count":2881
    }
  ]
}
```
---

#### Accéder aux images (1)
- Dans les résultats, seul le nom des images est retourné, mais pas l'URL complet qui y mène : 
```json
  "backdrop_path":"/pHs6YIRIPEHnnersHlvObtJ1y6U.jpg"
```

- Il faut ajouter à ce nom, l'URL de base, ainsi que la taille de l'image. Ces informations s'obtiennent en consultant l'API `/configuration` : 

```json
{"images":
  {"base_url":"http://image.tmdb.org/t/p/",
    "secure_base_url":"https://image.tmdb.org/t/p/",
    "backdrop_sizes":["w300","w780","w1280","original"],
    "logo_sizes":["w45","w92","w154","w185","w300","w500","original"],
    "poster_sizes":["w92","w154","w185","w342","w500","w780","original"],
    "profile_sizes":["w45","w185","h632","original"],
    "still_sizes":["w92","w185","w300","original"]}
]}
```
---

#### Accéder aux images (2)
- Ainsi si on veut accéder à notre image de *backdrop* en largeur 780 pixels, on écria : 

> https://image.tmdb.org/t/p/w780/pHs6YIRIPEHnnersHlvObtJ1y6U.jpg
 
![logo](backgrounds/melancholia.jpg)  
---

#### Les principales requêtes
- Films à l'affiche cette semaine : `trending/movie/week`
- Série diffusées cette semaine : `trending/tv/week`
- Personnes à l'affiche cette semaine : `trending/person/week` 
- Recherche de films par mots clés : `search/movie`
- Recherche de séries par mots clés : `search/tv`
- Recherche de personnes par mots clés : `search/person`
- Détail d'un film avec sa distribution : `movie/{id}?append_to_response=credits`
- Détail d'une série avec sa distribution : `tv/{id}?append_to_response=credits`
---
#### Références

- Site principal
  - https://www.themoviedb.org/
- Documentation
  - https://developers.themoviedb.org/3/getting-started/introduction
 
--- 
#### Exercices

- Obtenir une clé d'accès chez TMDB
- Obtenir la liste des films à l'affiche cette semaine en français
- Obtenir la distribution du premier film obtenu précédemment
- Obtenir la date de naissance de la première personne de la distribution du film précédent
- Visualiser la photo de cette première personne
