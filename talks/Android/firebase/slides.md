## Programmation Android

![logo](backgrounds/google-firebase-logo.webp)
<http://firebase.google.com>

Nicolas Singer


## Présentation de Firebase
Firebase est un service cloud opéré par google qui : 

- permet le développement d’application
  - Realtime database
  - Crash reporting
  - Authentification
  - …
- Permet de promouvoir et de suivre les activités liées à une application
  - Google analytics
  - AdMob et AdWords
  - App Indexing
  - …


## Présentation de Firebase
On accède aux services de firebase avec une API implémentée : 

 - Sous la forme de bibliothèques pour Android, IOS, javascript (web), C++, unity…
 - Avec le protocole REST 


## Le service d'authentification
 Permet d’authentifier un utilisateur :
 - Par email/password
 - Par numéro de téléphone
 - Par un fournisseur d’identité comme facebook, twitter, google ,…


## Authentification: fonctionnement
 1. L’application demande à l’utilisateur de décliner son identité avec une des méthodes proposées par le service (email, téléphone, oAuth, etc.)
 2. Firebase vérifie cette identité et retourne une réponse au client
 3. Après une authentification réussie, le client dispose d’un token qui peut-être utilisé : 
    - Par firebase pour contrôler l’accès à ses services internes (base de données, etc.)
    - Par votre back-end comme identifiant de l’utilisateur


## Authentification : exemple (1/2)
```kotlin
// On liste les fournisseurs d’authentification possibles
val providers = arrayListOf(AuthUI.IdpConfig.EmailBuilder().build(),
                            AuthUI.IdpConfig.PhoneBuilder().build(),
                            AuthUI.IdpConfig.GoogleBuilder().build(),
                            AuthUI.IdpConfig.FacebookBuilder().build(),
                            AuthUI.IdpConfig.TwitterBuilder().build())
// On lance le formulaire d’authentification
startActivityForResult(AuthUI.getInstance()
                       .createSignInIntentBuilder()
                       .setAvailableProviders(providers).build(),RC_SIGN_IN) 
```


## Authentification : exemple (2/2)
```kotlin<
override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
   super.onActivityResult(requestCode, resultCode, data)
   if (requestCode == RC_SIGN_IN) {
          val response = IdpResponse.fromResultIntent(data)
          if (resultCode == Activity.RESULT_OK) {
            // Authentifié avec succès
            val user = FirebaseAuth.getInstance().currentUser
            // ...       
          } else {
            // Echec de l’authentification. Si la réponse est nulle, l’utilisateur
            // a annulé en utilisant le bouton « back ». Sinon il faut tester
            // response.getError().getErrorCode() et gérer l’erreur.
            // ...       
          }   
   }
} 
```


## Cloud Firestore
- Firebase permet l’hébergement d’une base de données temps réel

- Le temps réel signifie que tous les clients connectés partagent un accès à la base et sont prévenus immédiatement des changements

- Cette base de données possède également un mode « offline » qui permet au client d’accéder à une copie quand il n’y a pas de réseau


## Cloud Firestore : format (1/2)
- Le modèle de données utilisée est de type NoSQL
- La brique de base est le document qui porte un nom et des propriétés 
```javascript
alovelace {
    first: "Ada",
    last: "Lovelace",
    born: 1830
}
```
- Les valeurs peuvent être des « map », cad contenir des propriétés imbriquées (comme en JSON)
```javascript
alovelace {
    name : {
        first: "Ada",
        last: "Lovelace",
    },
    born: 1830
}
```


## Cloud Firestore : format (2/2)
- Les documents sont regroupés dans des collections
```javascript
users [ 
    alovelace {
        name : {
            first: "Ada",
            last: "Lovelace",
        }
        born: 1830
    },
    aturing {
        name : {
            first: "Alan",
            last: "Turing",
        },
        born: 1912
    }
]
```
- Il est recommandé de placer des documents de même structure dans une collection


## Cloud Firestore : Les références
- Chaque document est identifié par son emplacement dans la base de données
- Pour faire référence à un document dans le code, on doit obtenir une référence  :
```kotlin 
val alovelaceDocumentRef = db.collection("users").document("alovelace") 
```
- On peut aussi obtenir une référence vers une collection :
```kotlin 
val usersCollectionRef = db.collection("users") 
```


## Cloud Firestore : Ajout sans préciser l'id
- On ajoute le document en référençant sa collection et en utilisant la méthode add().
- On peut passer en paramètre à cette méthode un objet Kotlin qui sera transformé en document
```kotlin 
db.collection("cities")
    .add(data)
    .addOnSuccessListener { 
       docRef -> Log.d(TAG, "Document ajouté avec l’id : ${docRef.id}")
    }.addOnFailureListener { 
        e -> Log.w(TAG, "Erreur lors de l’ajout du document", e)
    } 
```
- Un callback peut être spécifié pour récupérer l’id unique du document créé


## Cloud Firestore: Ajout d'un document avec id
- On ajoute le document avec set() en référençant son id et sa collection 
```kotlin 
db.collection("cities").document("marseille").set(data) 
```
- On peut mettre à ajour un champ d’un document avec l’opération update() : 
```kotlin 
val marseilleRef = db.collection("cities").document("marseille")
marseilleRef.update("population" , 1000000)
```
- Et on peut effacer un document avec l’opération delete()
```kotlin 
db.collection("cities").document("marseille").delete()
```


## Cloud Firestore : Accès aux documents stockés
- On peut accéder à un document de deux façons différentes : 
  - On appelle une méthode pour le lire
  - On met en place un écouteur qui reçoit sa valeur actuelle et qui sera appelé chaque fois que cette valeur changera
- Pour accéder en une seule fois, on utilise la méthode get() : 
```kotlin 
val docRef = db.collection("cities").document("marseille")
docRef.get().addOnSuccesListener {
    document -> val city = document.toObject(City::class.java)
}
```


## Cloud Firestore : Accès aux documents stockés
- On peut récupérer une liste de document appartenant à une collection.
- Pour obtenir les documents en fonction d’un critère on utilise des méthodes _where()_ : 
```kotlin 
db.collection("cities")
    .whereLessThan("population", 500000)
    .get()
    .addOnSuccessListener { documents ->
        for (document in documents) {
            // on cycle sur la liste obtenu
        }
    }.addOnFailureListener { exception ->
        // erreur de lecture     
    } 
```
- Si pas de _where()_ on obtient tous les documents de la collection


## Firebase Storage
- Le Firebase Storage permet de stocker des données binaires.

- Il permet d’uploader et de télécharger ces données.

- Permet de stocker des images, des sons, des vidéos, ou toutes données de votre choix.

- Ces données sont aussi accessibles via le Google Cloud Storage


## Firebase Storage : Fonctionnement
- On obtient une instance pour stocker avec : 
```kotlin 
val storage = FirebaseStorage.getInstance() 
```
- On demande une référence vers un conteneur
```kotlin 
// Crée une référence vers « images »
var imagesRef: StorageReference? = storageRef.child("images")
// On peut aussi spécifier des chemins
var spaceRef = storageRef.child("images/space.jpg") 
```


## Firebase Storage : Fonctionnement
- On uploade un fichier avec : 

```kotlin 
var file = Uri.fromFile(File("path/to/images/rivers.jpg"))
val riversRef = storageRef.child("images/rivers.jpg")
uploadTask = riversRef.putFile(file)
// Met en place des observateurs qui écoutent si le 
// téléversement réussi ou échoue
uploadTask.addOnFailureListener {
        // Echec
    }.addOnSuccessListener {
        // Réussite
    } 
```


## Firebase Storage : Fonctionnement
- On télécharge un fichier avec : 

```kotlin 
var islandRef = storageRef.child("images/island.jpg")
val ONE_MEGABYTE: Long = 1024 * 1024
islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
    // Les données sont prêtes à être utilisées
}.addOnFailureListener {
   // Echec du téléchargement
} 
```
