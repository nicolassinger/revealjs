
![logo](backgrounds/logo.png)  <!-- .element height="70%" width="70%" -->

Gérer et créer des notifications sous Android

*Nicolas Singer*
---

#### Notification
- C'est un message proposé en dehors de l'interface principale de l'application.
- Utilisé en général pour rappeler à l'utilisateur qu'une action est disponible comme :
  - Un nouveau mail vient d'arriver 
  - Une promotion peut être consultée
- Le fait de cliquer sur la notification ouvre l'application qui l'a générée
---

#### Notification
- Lors de son déclenchement, la notification apparaît comme une icône dans la barre de statut de l'appareil
 
<div class="r-stack">

  ![logo](backgrounds/notificone.png)  

</div>
---

#### Notification
- L'utilisateur peut faire glisser vers le bas la barre de statut pour ouvrir le menu de notification et consulter
les détails des notifications.

<div class="r-stack">

  ![logo](backgrounds/notificationdrawer.png) <!-- .element height="70%" width="70%" -->

</div>

---

#### Notification
- Certaines notifications peuvent présenter des actions immédiatement utilisables sans passer par l'application

<div class="r-stack">

![logo](backgrounds/notificationrepondre.png) <!-- .element height="70%" width="70%" -->

</div>
---

#### Composition d'une notification
<div class="r-stack">

![logo](backgrounds/notificationcomposition.png) <!-- .element height="70%" width="70%" -->
</div>

<div class="r-stack">
<div class="justify">
1 - Petite icone  <br/>
2 - Nom de l'application <br/>
3 - Date et heure <br/>
4 - Grande icône  <br/>
5 - Titre <br/>
6 - Texte
</div>
</div>
---

#### Canal de notification
- Une notification doit être assignée à un canal (*channel*). 
- Les utilisateurs peuvent désactiver ou paramétrer les canaux qu'ils souhaitent (au lieu de désactiver toutes
les notifications d'une application)

<div class="r-stack">

  ![logo](backgrounds/notificationchannel.png) <!-- .element height="50%" width="50%" -->
</div>
---

#### Avoir le droit de générer des notifications 1/2
- L'application doit obtenir la permission de générer des notifications
- Dans le fichier AndroidManifest.xml :
```xml
     <uses-permission android:name="android.permission.POST_NOTIFICATIONS"/>
```

---

#### Avoir le droit de générer des notifications 1/2
- Code pour demander la permission à l'utilisateur : 
 
```kotlin
 private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission() ) { isGranted: Boolean ->
        if (isGranted) {
          Toast.makeText(this, "Permissions obtenues", Toast.LENGTH_SHORT).show()
        } else {
           Toast.makeText(this, "Pas de notifications", Toast.LENGTH_LONG).show()
        }
    }

 private fun askNotificationPermission() {
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) ==
                PackageManager.PERMISSION_GRANTED
            ) {
                // La permission est déjà accordée
            } else {
                // On demande la permission
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }
```

---

#### Créer un canal de notification 

```kotlin
  val name = "Description du canal"
  val channelId = "id du canal"
  val descriptionText = "Description du canal"
  val importance = NotificationManager.IMPORTANCE_DEFAULT
  val channel = NotificationChannel(channelId, name, importance).apply {
    description = descriptionText
  }
  // Enregistrer le canal auprès du système
  val notificationManager: NotificationManager =
    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.createNotificationChannel(channel)
```

---

#### Créer une notification 1/3
- Définir l'apparence de la notification : 
 
```kotlin
val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
var builder = NotificationCompat.Builder(this, channelId)
        .setSmallIcon(R.drawable.notification_icon)
        .setContentTitle("Titre de la notification")
        .setContentText("Détail de la notification")
        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        .setSound(defaultSoundUri)
```

---

#### Créer une notification 2/3
- Définir ce qui se passe quand on clique sur la notification :

```kotlin
val intent = Intent(this, SecondActivity::class.java)
intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
// quelque chose à passer à l'activité ? 
intent.putExtra("titre", "Titre de la notification")
val pendingIntent = PendingIntent.getActivity(this, 0 , intent, 
  PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)

val notificationBuilder = builder.setAutoCancel(true)
            .setContentIntent(pendingIntent)
```

---

#### Créer une notification 3/3
- Afficher la notification 

```kotlin
with(NotificationManagerCompat.from(this)) {
    // notificationId est un entier qui doit unique pour chaque notification définie 
    notify(notificationId, notificationBuilder.build())
}

```

---

#### Récupérer des paramètres
- Quand on définit l'activité à lancer lors du click sur une notification, on peut passer des extras dans l'`Intent`
- L'activité lors de son lancement peut récupérer ces informations en lisant cet `Intent` :

```kotlin
class SecondActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        val value = intent.extras?.getString("titre")?:""

        setContent {
            Text(value)
        }

    }
}
``` 

---

#### Firebase Cloud Messaging (FCM)

- Firebase est un ensemble de services managés par Google
- Firebase Cloud Messaging est le service qui permet d'envoyer des notifications à des applications clientes.
  - Une application peut s'abonner à des notifications envoyées par FCM
  - Les notifications peuvent être définies manuellement via une interface graphique 
  - Mais aussi automatisées via un SDK
---

#### FCM : Campagnes manuelles
<div class="r-stack">

![logo](backgrounds/FCMcampagnes.jpg) <!-- .element height="70%" width="70%" -->
</div>

---

#### Intégration de FCM dans une application Android 1/3
- Créer un compte sur firebase et via la console web créer un nouveau projet.
- Toujours avec la console web, ajouter l'application au projet en précisant son nom de package 
- Télécharger le fichier `google-services.json` et l'ajouter dans le dossier `app` de l'application
  - Ce fichier contient les codes nécessaires à votre application pour se connecter à firebase

---
#### Intégration de FCM dans une application Android 2/3
Dans le fichier `build.gradle` de la racine du projet ajouter dans la partie `buildscript` : 
```
buildscript {
    dependencies {
      classpath 'com.google.gms:google-services:4.3.14'
    }
}
```

---

#### Intégration FCM dans une application Android 3/3
Dans le fichier `build.gradle` de l'application ajouter dans la partie `plugins` : 

```
plugins {
    ...
    id 'com.google.gms.google-services'
    ...
}
```
et dans la partie `dependencies` :
```
dependencies {
  implementation platform('com.google.firebase:firebase-bom:31.1.0')
  implementation 'com.google.firebase:firebase-analytics'
  implementation 'com.google.firebase:firebase-messaging-ktx'
}
```

---
#### Réception des notifications
- Une fois FCM intégré, FCM peut envoyer des notifications à tous les appareils où votre 
application a été installée
  - Possibilité de viser un appareil en particulier 
  - Possibilité pour les appareils de s'abonner à un *topic* et d'envoyer les notifications uniquement à ce *topic*.
  - Ceci est faisable aussi bien avec le SDK qu'avec l'interface graphique de FCM. 
- **Cas particulier** : Si l'application est au premier plan lorsque la notification FCM 
survient, elle n'apparaît pas. Il faut implémenter un service pour la récupérer.

---

#### Le service pour recevoir les notifications FCM si l'application est au premier plan

```kotlin
class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Est-ce que le message contient une notification ? 
        remoteMessage.notification?.let {
            // on génère la notification pour qu'elle apparaisse dans la barre des taches
            it.body?.let { body -> sendNotification(body) }
        }
    }
}
```
et ajouter dans le `AndroidManifest.xml` : 
```xml
<service
   android:name=".MyFirebaseMessagingService"
   android:exported="false">
   <intent-filter>
      <action android:name="com.google.firebase.MESSAGING_EVENT" />
   </intent-filter>
</service>
``` 


---

#### Le SDK FCM (coté Android)
- Récupérer le token qui désigne l'appareil auprès de FCM
 
```kotlin
Firebase.messaging.getToken().addOnCompleteListener(OnCompleteListener { task ->
        if (!task.isSuccessful) {
                Log.w("etiquette", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
        })

```
---

#### Le SDK FCM (coté Android)
- S'abonner à un topic

```kotlin
fun subscribeTopic() {
        Firebase.messaging.subscribeToTopic("weather")
            .addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("etiquette", "Failed to subscribe to weather topic")
                }
        }
}

```
---

#### Envoyer des notifications coté serveur 1/2
- Si vous voulez écrire un serveur NodeJS qui envoie des notifications à des appareils Android via FCM : 
- Avec la console web firebase, ajoutez une application de type *WebApp* au projet FCM
- Dans l'onglet *Comptes de service*, générez une nouvelle clé privée et récupérez le fichier obtenu
<div class="r-stack">

  ![logo](backgrounds/cleprive.jpg) <!-- .element height="70%" width="70%" -->
</div>

---
#### Envoyer des notifications coté serveur 2/2
- Placez ce fichier à la racine de votre projet NodeJS et appelez-le `key.json`
- Ajoutez le SDK FCM aux dépendances du projet et envoyez une notification à un topic comme ci-dessous : 
 
```javascript
var admin = require("firebase-admin");
var serviceAccount = require('./key.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const weather = {
    notification : {
       title : "FCM IS COOL !",
       body : "weather good",
        content_available : "true",
    }
}

admin.messaging().sendToTopic("weather", weather, options)
    .then(function (response) {
        console.log("notification envoyée")
    })
```

---
