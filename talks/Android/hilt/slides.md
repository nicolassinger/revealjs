##### Injection de dépendances avec Hilt

Améliorer l'architecture de son application avec l'injection de dépendances

*Nicolas Singer*
---

#### Problématique
- Situation actuelle
  - La vue fait appel au ViewModel pour ses données
  - Le ViewModel réalise les appels réseaux vers l'API TMDB

![logo](backgrounds/viewmodelsimple.svg)  <!-- .element height="60%" width="60%" -->

- La vue a donc besoin du ViewModel qui lui-même a besoin de l'interface API TMDB
---

#### Introduction du Repository
![logo](backgrounds/repository.svg)  <!-- .element height="70%" width="70%" -->

- Dépendance de la vue au ViewModel
- Dépendance du ViewModel au Repository
- Dépendance du Repository à l'API TMDB et la base de données
---

#### Gestion manuelle des dépendances

```kotlin
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setContent {
            val viewmodel = MainViewModel()
            Films(viewmodel)
        }
    }
}

class MainViewModel : ViewModel() {
    val repo = Repository()
    
    val movies = MutableStateFlow<List<TmdbMovie>>(listOf())

    fun getMovies() {
        viewModelScope.launch { movies.value = repo.lastMovies() }
    }
}

class Repository {
    val tmdbapi = Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org/3/")
        .addConverterFactory(MoshiConverterFactory.create())
        .build().create(Tmdbapi::class.java)

    suspend fun lastMovies() = tmdbapi.lastmovies().results
}
```
---
#### Inconvénient de la gestion manuelle des dépendances 
- Cette façon de faire couple étroitement les objets entre eux 
  - C'est le ViewModel qui décide du Repository qu'il utilise
  - C'est le Repository qui décide d'utiliser Retrofit pour instancier une interface TmdbAPI

- *C'est mieux si c'est un système externe qui couple un composant avec sa dépendance !*

---
#### Principe de l'injection de dépendances
1. Quand on construit l'objet, on lui passe ses dépendances en paramètres.

```kotlin
class MainViewModel(repo : Repository) : ViewModel() {
    val movies = MutableStateFlow<List<TmdbMovie>>(listOf())

    fun getMovies() {
        viewModelScope.launch { movies.value = repo.lastMovies() }
    }
}

class Repository(tmdbapi: Tmdbapi) {
    suspend fun lastMovies() = tmdbapi.lastmovies().results
}

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setContent {
            val viewmodel : MainViewModel = viewModel()
            Films(viewmodel)
        }
    }
}
```
- Mais qui passe ses paramètres ? Le moteur d'injection de dépendances ! 



---
#### Hilt: Le moteur d'injection de dépendances pour Android
- Basé sur *Dagger* 
- En simplifie l'utilisation
- Permet d'indiquer comment construire les dépendances des classes Android
  - Annotations pour désigner les dépendances
  - Modules pour expliquer comment les construire
---

#### Hilt pas à pas (1/5)
- Création d'une classe `Application` qu'Android initialise au lancement comme ci-dessous : 
```kotlin
@HiltAndroidApp
class MyApplication: Application()
```
- Si la classe `Application` n'existait pas déjà, il faut la référencer dans le `AndroidManifest.xml` comme suit : 
```xml
<application
        android:name=".MyApplication"
        ...
``` 
---

#### Hilt pas à pas (2/5)
- Annotation de l'activité

```Kotlin 
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    ...
}
```

- Annotation des dépendances du ViewModel
 
```kotlin
@HiltViewModel
class MainViewModel @Inject constructor(private val repo: Repository) : ViewModel() {

}
```
---
#### Hilt pas à pas (3/5)
- Définition du Module expliquant comment construire les dépendances
 
```kotlin
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

@Singleton
@Provides
fun provideTmdbApi() : Tmdbapi =
  Retrofit.Builder()
    .baseUrl("https://api.themoviedb.org/3/")
    .addConverterFactory(MoshiConverterFactory.create())
    .build().create(Tmdbapi::class.java)

@Singleton
@Provides
fun provideRepository(api: Tmdbapi, db: FilmDao) =
  Repository(api, db)
}
```
---

#### Hilt pas à pas (4/5)
- Ajout au build.gradle de l'application

```
plugins {
    ....
    // pour hilt
    id("com.google.dagger.hilt.android") version "2.52" apply false
}
```
---
#### Hilt pas à pas (5/5)
- Ajouts au build.gradle du module
 
```
plugins {
  ....
  // pour hilt
  id("kotlin-kapt")
  id("com.google.dagger.hilt.android")
}


dependencies {
    ....
    // pour hilt
    implementation("com.google.dagger:hilt-android:2.52")
    kapt("com.google.dagger:hilt-compiler:2.52")
    implementation("androidx.hilt:hilt-navigation-compose:1.0.0")
}
```
---

#### Implémentation d'une fausse interface réseau avec Hilt
- On crée une *fausse* interface de l'API TMDB
- On demande à Hilt de nous injecter cette fausse interface à la place de la vraie
- Voici la fausse interface : 
 
```kotlin
class FakeTmdbApi : Tmdbapi {
    val moshi: Moshi = Moshi.Builder().build()
    val jsonAdapter: JsonAdapter<TmdbMovieResult> = moshi.adapter(TmdbMovieResult::class.java)

    // Faux Json 
    val jsonresult = "{\"page\":1,\"results\":[{\"adult\":false, ... " 

    override
    suspend fun lastmovies(@Query("api_key") api_key: String): TmdbMovieResult {
        val res = jsonAdapter.fromJson(jsonresult)
        if (res != null) return res
        else return TmdbMovieResult()
    }
}
```
---

#### Paramétrage du module pour injection de la fausse interface

```kotlin
@Qualifier annotation class FakeApi
@Qualifier annotation class RealApi

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @RealApi
    @Singleton
    @Provides
    fun provideTmdbApi() : Tmdbapi =
        Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(MoshiConverterFactory.create())
            .build().create(Tmdbapi::class.java)

    @FakeApi
    @Singleton
    @Provides
    fun provideFakeTmdbApi() : Tmdbapi { return FakeTmdbApi() }

    @Singleton
    @Provides
    fun provideRepository(@FakeApi api: Tmdbapi, db: FilmDao) = Repository(api, db)
}
```
---
#### Références
- Injection de dépendances avec Hilt : 
  - https://developer.android.com/training/dependency-injection/hilt-android




