##### Jetpack Compose

![logo](backgrounds/compose.png)  <!-- .element height="20%" width="20%" -->

Développement Android

*Nicolas Singer*
---
#### Compose sert à construire des interfaces utilisateurs natives avec Android
- version stable en 2022
- remplace la définition des interfaces au format XML
- logique basée sur la décomposition en composants
---
#### Approche composants
- Compose permet de définir des composants qui définissent leur affichage 
- Cette définition se fait par du code Kotlin.

<div class="twocolumn">
<div>

```kotlin
@Composable
fun Screen() {
  Text(
          text = "Des fleurs",
          style = MaterialTheme.typography.h4,
          modifier = Modifier.padding(10.dp)
)
  Image(
          painterResource(id = R.drawable.fleur),
          contentDescription = "Des fleurs",
          modifier = Modifier.size(200.dp)
)
```
</div>
<div>

![logo](backgrounds/fleurs.png)  <!-- .element height="50%" width="50%" -->
</div>
</div>
---
#### Approche composants
Cette approche permet la décomposition du rendu  

```kotlin
@Composable
fun Screen() {
    Texte()
    MonImage()
}

@Composable
fun Texte(){
    Text(
        text = "Des fleurs",
        style = MaterialTheme.typography.h4,
        modifier = Modifier.padding(10.dp)
    )
}

@Composable
fun MonImage() {
    Image(
        painterResource(id = R.drawable.fleur),
        contentDescription = "Des fleurs",
        modifier = Modifier.size(200.dp)
    )
}
```
---

#### L'annotation `@Composable`
- C'est l'annotation `@Composable` qui identifie une fonction comme définissant un composant.
- Cette fonction peut prendre 
des paramètres qui sont des valeurs que l'on doit passer au composant 

<div class="twocolumnsl">
<div>

```kotlin
@Composable
fun Screen() {
  Texte("Des fleurs")
  MonImage(R.drawable.fleur)
}
```
</div>
<div>

```kotlin
@Composable
fun Texte(contenu: String){
  Text(
          text = contenu,
          style = MaterialTheme.typography.h4,
          modifier = Modifier.padding(10.dp)
  )
}

@Composable
fun MonImage(id: Int) {
  Image(
          painterResource(id),
          contentDescription = "Des fleurs",
          modifier = Modifier.size(200.dp)
)
```
</div>
</div>
---

##### Les différents composants prédéfinis
- Compose propose des composants de base fidèles au thème natif d'android (Material Design)
- On y trouve des boutons, des champs de formulaire, des barres de progression, etc.
---

#### Les principaux composants
  <img class="r-stretch" src="backgrounds/materialdesign1.png"/>
---
#### Les principaux composants
  <img class="r-stretch" src="backgrounds/materialdesign2.png">
---
#### Les principaux composants
  <img class="r-stretch" src="backgrounds/materialdesign3.png">
---
#### Les principaux composants
  <img src="backgrounds/materialdesign4.png">

- Une application appelée **Compose Material Catalog** est disponible sur le *play store* pour visualiser tous ces composants et des exemples de code : 
  > https://play.google.com/store/apps/details?id=androidx.compose.material.catalog
---

#### Focus sur le composant `Text`
<video src="videos/text.mp4"></video>
---


#### Les composants de layout
- Ce sont les composants qui définissent le placement de leur contenu
- Par défaut Compose superpose les éléments :
 
<div class="twocolumn">
<div>

```kotlin
@Composable
fun Texte(){
    Text( text = "Des fleurs" )
    Text( text = "Offrez-en" )
}
```
</div>
<div>

![logo](backgrounds/superpose.png)  <!-- .element height="50%" width="50%" -->
</div>
</div>
---

#### Les composants de layout
- `Row` les place horizontalement
- `Column` les place verticalement

<div class="twocolumn">
<div>

```kotlin
@Composable
fun Texte(){
    Row() {
        Text(text = "Des fleurs")
        Text(text = "Offrez-en")
    }
}
```
</div>
<div>

![logo](backgrounds/horizontal.png)  <!-- .element height="50%" width="50%" -->
</div>
</div>

<div class="twocolumn">
<div>

```kotlin
@Composable
fun Texte(){
    Column() {
        Text(text = "Des fleurs")
        Text(text = "Offrez-en")
    }
}
```
</div>
<div>

![logo](backgrounds/vertical.png)  <!-- .element height="50%" width="50%" -->
</div>
</div>
---

#### Les composants de layout
- `Box` superpose mais en permettant de préciser l'alignement du contenu

<div class="twocolumn">
<div>

```kotlin
@Composable
fun Screen() {
    Box(contentAlignment = Alignment.TopCenter) {
        Image(
            painterResource(R.drawable.fleur),
            contentDescription = "Des fleurs"
        )
        Text(
            text = "Des fleurs", 
            Modifier.background(Color.White)
        )
    }
}
```
</div>
<div>

![logo](backgrounds/fleurssuperpose.png)  <!-- .element height="70%" width="70%" -->
</div>
</div>
---

#### Les composants de layout
- `Spacer` permet d'insérer un composant invisible pour ajouter des espaces dans la mise en page

<video src="videos/spacer.mp4"></video>
---

#### Les modificateurs (modifiers)
Ils permettent de décorer ou de paramétrer un composant. 
- Changer sa taille, sa disposition, son comportement, son apparence 
- Traiter l'action de l'utilisateur
- Ajouter des éléments d'interaction comme la possibilité de clic ou de scroll

<div class="twocolumn">
<div>

```kotlin

@Composable
fun Screen() {
    Column(
        Modifier
            .padding(10.dp)
            .background(Color.Red)) {
        Text(
            text = "un texte en rouge", 
            Modifier.padding(10.dp)
        )
    }
}
```
</div>
<div>

![logo](backgrounds/modifiertexte.png)  <!-- .element height="70%" width="70%" -->

</div>
</div>
---
#### Les modificateurs de forme (shape)
<video src="videos/shapes.mp4"></video>
---

#### Les composants ossatures 

- Le composant **Scaffold** permet de définir le contenu des emplacements classiques d'une 
application android
  - ToolBar
  - Contenu principal
  - Barre de navigation

<div class="twocolumnsr">
<div>
<pre><code data-trim>
@Composable
Scaffold(
    topBar = {
        TopAppBar { /* Contenu de la barre du haut */ }
    }
    bottomBar = {
        BottomAppBar { /* Contenu de la barre du bas */ }
    }
    floatingActionButton = {
        FloatingActionButton(onClick = { /* ... */ }) {
            /* Contenu de l'élément flottant */
        }
    },
) {
    // Contenu principal de l'écran
}

</code></pre>
</div>
<div>

![logo](backgrounds/scaffold.png)  <!-- .element height="60%" width="60%" -->
</div>
</div>
---

#### Utiliser des variables dans les composants
- Ici on masque le texte si le paramètre passé au composant est faux
<pre><code data-trim data-line-numbers="2|8-13">
@Composable
fun Screen(textevisible: Boolean) {
    Box(contentAlignment = Alignment.TopCenter) {
        Image(
            painterResource(R.drawable.fleur),
            contentDescription = "Des fleurs"
        )
        if (textevisible) {
            Text(
                text = "Des fleurs",
                Modifier.background(Color.White)
            )
        }
    }
}
</code></pre>
- La variable vient de l'extérieur du composant
---

#### Utiliser des variables dans les composants
- On peut créer des variables internes au composant
 
<pre code="kotlin"><code data-trim data-line-numbers="3|10-15">
@Composable
fun Screen() {
    var textevisible by remember { mutableStateOf(true) }
    
    Box(contentAlignment = Alignment.TopCenter) {
        Image(
            painterResource(R.drawable.fleur),
            contentDescription = "Des fleurs"
        )
        if (textevisible) {
            Text(
                text = "Des fleurs",
                Modifier.background(Color.White)
            )
        }
    }
}
</code></pre>

- La variable du composant mémorise l'état, et on va pouvoir le changer...
---

#### Changer l'état d'un composant
- Repérage d'un clic sur le composant image et changement de la visibilité du texte

<div class="twocolumnsr">
<div>
<pre code="kotlin">
<code data-trim data-line-numbers="1-18|3,8-10">
@Composable
fun Screen() {
    var textevisible by remember { mutableStateOf(true) }
    Box(contentAlignment = Alignment.TopCenter) {
        Image(
            painterResource(R.drawable.fleur),
            contentDescription = "Des fleurs",
            Modifier.clickable { 
              textevisible = !textevisible 
            }
        )
        if (textevisible) {
            Text(
                text = "Des fleurs",
                Modifier.background(Color.White)
            )
        }
    }
}
</code></pre>
</div>
<div>
<video data-autoplay loop src="videos/textimagevisibility.mp4"></video>
</div>
</div>
---

#### Navigation entre composants 
 <!-- .slide:  data-visibility="hidden" -->
- Comment suite à une action utilisateur remplacer un composant par un autre ? 
- C'est le rôle du composant de navigation :
 
<div class="twocolumn">
<div>

```kotlin
@Composable
fun Screen() {
    val navController = rememberNavController()
    NavHost(
        navController = navController, 
        startDestination = "image") {
            composable("image") { 
                Image(navController) 
            }
            composable("spacer") { 
                Spacer(navController) 
            }
    }
}
```
</div>
<div>

```kotlin
@Composable
fun Image(navController: NavController) {
    Image(
        painterResource(R.drawable.fleur),
        contentDescription = "Des fleurs",
        Modifier.clickable {
            navController.navigate("spacer")
        }
    )
}

@Composable
fun Spacer(navController: NavController) {
    Text("texte")
}
```
</div>
</div>

- Dépendance à ajouter dans le `build.gradle` :
```kotlin
implementation "androidx.navigation:navigation-compose:2.5.3"
```

---

#### Afficher des images désignées par un URL 

- Pour afficher des images externes il faut utiliser une bibliothèque qui réalise 
l'appel réseau et le décodage de l'image
- Par exemple la bibliothèque **Coil**

- Dépendance à ajouter dans le `build.gradle` :
```kotlin
implementation("io.coil-kt:coil-compose:2.1.0")
```
- Composant affichant l'image : 
```kotlin
AsyncImage(
    model = "https://example.com/image.jpg",
    contentDescription = "Ma super image"
)
```

---

#### Exemple de chargement d'image avec Coil

<video data-autoplay loop src="videos/coil.mp4"></video>
---

#### Références

- Layouts
  - https://developer.android.com/reference/kotlin/androidx/compose/foundation/layout/package-summary
- Composants 
  - https://developer.android.com/reference/kotlin/androidx/compose/material/package-summary#components
- Modificateurs: 
  - https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier
- Composants material 3
  - https://developer.android.com/jetpack/androidx/releases/compose-material3
- Liste d'icônes prédéfinies 
  - https://fonts.google.com/icons


