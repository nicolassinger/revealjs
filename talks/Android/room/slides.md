![logo](backgrounds/logo.png)  <!-- .element height="70%" width="70%" -->

La bibliothèque de persistance **Room**

*Nicolas Singer*
---

#### Problématique
- Stocker des données persistantes et structurées **sur l'appareil**.
- Android permet déjà de créer des bases de données au format SQLite.
- Room vient ajouter une couche d'abstraction qui facilite cela.
---

#### Principe
- Trois composants principaux dans *Room* : 
  - La classe base de données.
  - Les entités à stocker dans la base.
  - Les objets DAO (Data Access Objects) qui fournissent les méthodes pour interroger la base, et insérer/supprimer des données.

---

#### Vue d'ensemble

![logo](backgrounds/schema.svg) 

---

#### Définition d'une entité

```kotlin
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class FilmEntity(
  @PrimaryKey val id: String,
  val titre: String)
```

- Attention ! Pas de liste dans les propriétés.
- Si la propriété est un objet complexe, il faudra définir un convertisseur de type.
  - En effet Room ne permet pas les références entre objets dans les entités.
  - Trop couteux de faire des jointures de tables coté client.
---

#### Convertisseurs de types (1/2)
- Si on veut qu'une colonne de la base de données soit un type complexe : 
```kotlin
@Entity
data class FilmEntity(val fiche: TmdbMovie, @PrimaryKey val id: String)
```
- On doit le convertir vers un type que Room sait stocker (par exemple `String`).On définit alors une classe 
qui réalise cette conversion dans les deux sens :

```kotlin
@ProvidedTypeConverter
class Converters(moshi: Moshi) {
    val filmJsonadapter = moshi.adapter(TmdbMovie::class.java)

    @TypeConverter
    fun StringToTmdbMovie(value: String): TmdbMovie? {
        return filmJsonadapter.fromJson(value)
    }

    @TypeConverter
    fun TmdbMovieToString(film: TmdbMovie): String {
        return filmJsonadapter.toJson(film)
    }
}
```

---

#### Convertisseurs de types (2/2)
- Puis quand on crée la base, on spécifie le convertisseur de types : 

```kotlin
 Room.databaseBuilder( context, AppDatabase::class.java, "database-name" )
        .addTypeConverter(Converters(Moshi.Builder().build()))
        .build().filmDao()
```

---
#### Définition d'un DAO

```kotlin
@Dao
interface FilmDao {
  @Query("SELECT * FROM filmentity")
  suspend fun getFavFilms(): List<FilmEntity>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun insertFilm(film: FilmEntity)

  @Query("DELETE FROM filmentity WHERE id = :id")
  suspend fun deleteFilm(id: String)
}
```

---

#### Définition de la base de données

```kotlin
@Database(entities = [FilmEntity::class, SerieEntity::class, ActeurEntity::class], version = 4)
abstract class AppDatabase : RoomDatabase() {
    abstract fun filmDao(): FilmDao
}
```
---

#### Utilisation 

- Pour créer la base et le DAO : 
 
 ```kotlin
 val database = Room.databaseBuilder( context, AppDatabase::class.java, "database-name" )
        .fallbackToDestructiveMigration()
        .build()
 val dao = database.filmDao()
```
- Pour utiliser le DAO:
 
```kotlin
  // liste des films de la base de données
  val favs = db.getFavFilms()
  // insérer un nouveau film `film` dans la base
  val filmEntity = FilmEntity(id = film.id, titre = film.title)
  db.insertFilm(filmEntity)
  // supprimer un film
  db.deleteFilm(film.id)
```

---

#### Dépendances

- Dépendances à ajouter au `build.gradle` de l'application (top level)
```gradle
id("androidx.room") version "2.6.1" apply false
```

- Dépendances à ajouter au `build.gradle`  du module pour utiliser Room

```gradle

plugins {
  ....
  id("androidx.room")
}

android {
  room {
    schemaDirectory("$projectDir/schemas")
  }
}

dependencies {
 ....
 implementation("androidx.room:room-runtime:2.6.1")
 kapt("androidx.room:room-compiler:2.6.1")
}
```




