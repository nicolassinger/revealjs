##### Navigation avec Jetpack Compose

Gérer les transitions entre composants

*Nicolas Singer*
---

#### Problématique
- Comment réaliser une transition entre deux composants ?  

<video src="videos/transitions.webm" height="600" > ></video>

---

#### Aprroche naïve 
- Un état pour mémoriser le composant qui doit s'afficher
- Une condition qui affiche le bon composant en fonction de l'état 


```kotlin
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            var screenNumber by remember { mutableStateOf(1) }
            when (screenNumber) {
                1 -> Screen1(onClick = { screenNumber = 2 })
                2 -> Screen2(onClick = { screenNumber = 1 })
            }
        }
    }
}
```


<div class="twocolumn">

```kotlin
@Composable
fun Screen1(onClick: () -> Unit) {
    Text("Premier écran")
    Button(onClick = onClick) {
        Text("Prochain écran")
    }
}
```

```kotlin
@Composable
fun Screen2(onClick: () -> Unit) {
    Text("Second écran")
    Button(onClick = onClick) {
        Text("Ecran précédent")
    }
}
```

</div>
---
#### Utiliser plutôt le module de Navigation 
- Ajouter les dépendances
 
```kotlin
dependencies {
    implementation("androidx.navigation:navigation-compose:2.8.2")
}

// egalement nécessaires pour la sérialisation des destinations
plugins {
    kotlin("plugin.serialization") version "1.9.25"
}
dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")
}
```

---

- Aller chercher le contrôleur de navigation:
```kotlin
val navController = rememberNavController()
```
- Définir les destinations de navigation comme des classes sérialisables :

```kotlin
@Serializable class Destination1
@Serializable class Destination2
@Serializable class Destination3
```
---

- Utiliser ces classes et le composant `NavHost` pour construire la table des destinations

```kotlin
NavHost(navController = navController, startDestination = Destination1()) {
    composable<Destination1> { Screen1(/*...*/) }
    composable<Destination2> { Screen2(/*...*/) }
    composable<Destination3> { Screen3(/*...*/) }
}
```
- Ici la destination de départ est `Destination1`, le composant affiche donc le composant `Screen1`.


- Quand on voudra transitionner vers la deuxième destination (et donc afficher le composant `Screen2`), on écrira dans le code : 
 
```kotlin
navController.navigate(Destination2())
```

---
### On peut ajouter des animations de transition
 <!-- .slide:  data-visibility="hidden" -->
```kotlin
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            NavHost(navController = navController, startDestination = "screen1") {
                composable(route  = "screen1",
                    enterTransition = {
                        slideIntoContainer(
                            animationSpec = tween(300, easing = EaseIn),
                            towards = AnimatedContentTransitionScope.SlideDirection.Start
                        )

                    },
                    exitTransition = {
                        slideOutOfContainer(
                            animationSpec = tween(300, easing = EaseOut),
                            towards = AnimatedContentTransitionScope.SlideDirection.Start
                        )
                    }) 
                { Screen1 { navController.navigate("screen2") } }
        }
    }
}
``` 
---

### Connaître la destination actuelle
- On peut interroger l'objet `navController` pour connaître la destination actuelle
 

```kotlin
val navBackStackEntry by navController.currentBackStackEntryAsState()
val currentDestination = navBackStackEntry?.destination

// est-ce qu'on est sur Destination3 ?
val isDestination3 = currentDestination?.hasRoute<Destination3>() == true
```

- Cela peut être utile pour adapter l'IHM à la destination en cours.

---
### Naviguer avec des arguments
- On peut transmettre des arguments à des destinations.
- Pour cela on ajoute des propriétés à la classe représentant la destination 

```kotlin
@Serializable
data class FilmDetail(val id: String)
```
- L'objet destination se récupère dans la table de navigation, on peut alors passer ses propriétés au composant :
```kotlin
  NavHost(...) {
    composable<FilmDetail> { backStackEntry ->
        val filmDetail: FilmDetail = backStackEntry.toRoute()
        FilmDetails(model, filmDetail.id)
    }
}
```
- Quand on navigue vers la destination, on spécifie le paramètre : 
```kotlin
navController.navigate(FilmDetail("23"))
```

---

### Exemple complet avec barre de navigation 
 
```kotlin
@Serializable class Profil
@Serializable class Edition

val navController = rememberNavController()
val navBackStackEntry by navController.currentBackStackEntryAsState()
val currentDestination = navBackStackEntry?.destination

Scaffold(
    bottomBar = { 
      NavigationBar {
        NavigationBarItem(
            icon = { ... }, label = { Text("Mon profil") },
            selected = currentDestination?.hasRoute<Profil>() == true,
            onClick = { navController.navigate(Profil()) })
        NavigationBarItem(
            icon = { ... }, label = { Text("Edition du profil") },
            selected = currentDestination?.hasRoute<Edition>() == true,
            onClick = { navController.navigate(Edition()) })
        }
      }) 
    { innerPadding ->
        NavHost(navController, startDestination = Profil(),
               Modifier.padding(innerPadding)) {
                  composable<Profil> { ProfilScreen() }
                  composable<Edition> { EditionScreen() }
           }
    }
```
---
#### Résultat 

<video src="videos/bottombar.webm" height="600" data-autoplay> ></video>
