##### ViewModels et états

![logo](backgrounds/compose.png)  <!-- .element height="20%" width="20%" -->

Développement Android

*Nicolas Singer*
---

#### Architecture classique d'une application android

![logo](backgrounds/viewmodel.png)  <!-- .element height="70%" width="70%" -->
---

#### Qu'est-ce que le ViewModel ? 
- C'est : 
  - Une classe pour stocker et gérer les données relatives à l'interface graphique
  - Une classe dont les données *résistent* aux changements de configuration de l'appareil 
  - Une classe dont les données sont observables 
  - Une classe pour traiter les actions de l'utilisateur

---
#### Quelle relation entre l'interface utilisateur et le ViewModel 
- Les composants compose peuvent dépendre d'un état défini dans le ViewModel
  - Quand l'état change le composant est redessiné. 
  - Cela suppose que l'état soit observable (typiquement de type `State<T>`)
- Quand un composant compose détecte une action de l'utilisateur, il la transmet au ViewModel 
  - C'est le ViewModel qui décide quoi faire en fonction de l'action, et qui modifie un de ses états en conséquence.
---
#### Implémentation d'un ViewModel

```kotlin
class MainViewModel : ViewModel() {

    var compteur by MutableStateOf(0)
   
    fun incrementer_compteur() {
            compteur = compteur + 1
    } 
}
```
---

#### Utilisation du ViewModel par l'activité

```kotlin
class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    
    val viewmodel: MainViewModel by viewModels()
    
    setContent {
      Compteur(viewmodel)
    }
}

@Composable
fun Compteur(viewmodel: MainViewModel) {

    Text(text = viewmodel.compteur.toString(),
        modifier = Modifier.clickable {
            viewmodel.incrementer_compteur()
        })
}
```
---

#### Le type `State<T>`
- Le type `State<T>` est un type observable spécifique à Jetpack compose. Une variable de ce type 
se déclare :
```kotlin 
var compteur by MutableStateOf(0)
```

- La syntaxe `by` nécessite les imports suivants : 
```kotlin
 import androidx.compose.runtime.getValue
 import androidx.compose.runtime.setValue
```

- La variable s'utilise ensuite comme une variable classique à ceci près que compose est averti
de ses changements. 
- Attention le composant ne sera pas mis à jour si la nouvelle valeur est 
identique à l'ancienne ! 
---

### Autres types observables
<ul>
<li>Kotlin propose un autre type observable non lié à Compose. C'est le type <code>Flow&lt;T></code></li>
<li>Un Flow est une sorte de collection dont les éléments sont émis au fur et à mesure</li>
<li>Voici un exemple de création d'un <code>Flow&lt;Int></code> :
 
```kotlin 
suite_entiers = flow {
   emit(1)
   emit(2)
   emit(3)
}
```
</li>
<li>On observe un Flow dans un composant Compose comme ceci :

```kotlin 
val pageState by viewmodel.suite_entiers.collectAsStateWithLifecycle()
```
 et on utilise ensuite la variable comme une variable classique.
</li>
</ul>
---

### Un flow particulier, le StateFlow<T>
 - Un StateFlow est un flow qui mémorise sa dernière valeur et qui ne peut pas être vide

```kotlin
// déclaration et initialisation
val etat = MutableStateFlow("coucou")


// changement de valeur
etat.value = "hello"

// observation dans un composant compose
val etat by viewmodel.etat.collectAsStateWithLifecycle()
```
        
---

#### Références
- ViewModels : 
  - https://developer.android.com/topic/libraries/architecture/viewmodel
- Etats dans JetPack Compose
  - https://developer.android.com/jetpack/compose/state
- StateFlow : 
  - https://developer.android.com/kotlin/flow/stateflow-and-sharedflow
- Dépendance pour pouvoir utiliser les ViewModels :
`androidx.lifecycle:lifecycle-runtime-ktx:2.5.1` <!-- .element: class="mysmall" -->




