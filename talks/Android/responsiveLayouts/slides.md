##### Layouts responsive avec Jetpack Compose

![logo](backgrounds/compose.png)  <!-- .element height="20%" width="20%" -->

Développement Android

*Nicolas Singer*
---
### Pourquoi des layouts adaptatifs ?
- Une interface doit occuper au mieux l'espace disponible 
  - Cet espace change selon l'orientation de l'écran ou le type d'appareil
  - Dans les nouvelles versions d'android, on peut faire du multi-fenêtrage.
  - La forme des écrans peut varier en cours d'utilisation avec par exemple les écrans pliables.
  - Votre application peut être amené à s'exécuter en dehors de l'environnement android (windows 11, chomebooks, ...)

- Cette adaptation peut conduire à changer complètement l'IHM en fonction de la taille de l'écran
---
#### Comment s'adapter à la taille de l'écran ?
- Android définit trois classes de longueurs qu'on peut utiliser pour proposer un layout adaptatif : 
  - pour la largeur : 

![logo](backgrounds/width.jpg)  
---

#### Comment s'adapter à la taille de l'écran ?
- pour la hauteur
 
  ![logo](backgrounds/height.jpg) 
---

#### Récupérer les classes de taille d'écran 

- On le fait dans le SetContent de l'activité, et on passe les classes aux composants

```kotlin
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setContent {
          val windowSizeClass = currentWindowAdaptiveInfo().windowSizeClass
          Screen(windowSizeClass)
       }
    }
}

@Composable
fun Screen(classes: WindowSizeClass) {
  val classeHauteur = classes.windowHeightSizeClass
  val classeLargeur = classes.windowWidthSizeClass
  when (classeLargeur) { 
    WindowWidthSizeClass.COMPACT -> /* largeur faible */
    WindowWidthSizeClass.MEDIUM -> /* largeur moyenne */
    WindowWidthSizeClass.EXPANDED -> /* grande largeur */
  }
}
```
---

#### Exemple 
```kotlin
@Composable
fun Screen(windowClass: WindowSizeClass) {
    when (windowClass.windowWidthSizeClass) {
        WindowWidthSizeClass.COMPACT -> {
            Column(Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                horizontalAlignment = Alignment.CenterHorizontally) {
                Image(
                    painterResource(R.drawable.fleur),
                    contentDescription = "Des fleurs",
                )
                Text(text = "Description de l'image")
            }
        }
        else -> {
            Row(Modifier.fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painterResource(R.drawable.fleur),
                    contentDescription = "Des fleurs",
                )
                Text(modifier = Modifier.padding(10.dp),
                    text = "Description de l'image")
            }
        }
    }
}
```
---
#### Exemple
<div class="twocolumn">
<div>

![logo](backgrounds/portrait.png)  <!-- .element width="50%" -->
</div>

<div>

![logo](backgrounds/paysage.png)  <!-- .element width="80%" -->

</div>

</div>

---

#### Références

- Gestion des différentes tailles d'écran : 
  - https://developer.android.com/guide/topics/large-screens/support-different-screen-sizes

- Dépendances à inclure pour pouvoir mesurer l'écran : 

```
  implementation("androidx.compose.material3.adaptive:adaptive-android:1.0.0")
```



