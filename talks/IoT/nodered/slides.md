![logo](backgrounds/node-red.svg)  <!-- .element height="50%" width="50%" -->

*Nicolas Singer*
---

#### Node-RED
- Application web pour créer des applications basées sur les évènements
- Permet d'interconnecter des sources d'évènements à des actions
- Basée sur le concept de programmation pilotée par les flux
  - Conception modulaire et centrée sur les données
  - le logiciel est un réseau de boîtes noires composées et réutilisées indépendamment. 
  - se concentre sur le flux de données entre les composants interconnectés.
---

#### Principe
- Node-RED propose un éditeur de flows créées en reliant des noeuds 

![message](backgrounds/set-message-property-fixed.png)  <!-- .element height="50%" width="50%" -->

- Ces noeuds sont choisis dans une bibliothèque de noeuds prédéfinies 
  - certains noeuds sont le fruit de la communauté
  - possibilité de créer ses propres noeuds

- Actuellement la bibliothèque communautaire contient plus de 5000 types de noeuds 

---

#### Historique
- Créé dans les laboratoires d'IBM pour manipuler les topics de MQTT
- Open-Source en 2013 et piloté par l'*Open JS Foundation* depuis 2016

![message](backgrounds/OpenJS_Foundation_logo.png)  <!-- .element height="50%" width="50%" -->

---

#### Usages
- Principalement utilisé pour : 
  - La domotique
  - Le pilotage d'équipements *edge*.
  - La définition de processus BPM (Business process managment) ou ETL (Extract, Transform, Load)

---

#### Fonctionnement
- Node-RED repose sur un moteur node.js et utilise javascript pour la définition des noeuds
- L'éditeur permet 
  - de sélectionner des noeuds dans une palette
  - de les configurer
  - de les relier entre eux.

- le flow final est un fichier au format json 

---

#### Vue d'ensemble

![message](backgrounds/noderedflow.svg)  <!-- .element height="100%" width="100%" -->

