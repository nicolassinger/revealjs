
![logo](backgrounds/telegraf.png)  <!-- .element height="50%" width="50%" -->

*Nicolas Singer*
---

#### telegraf 

- Un agent intermédiaire pour collecter des données à partir de différentes sources et les envoyer à différentes cibles.
  - Par exemple collecter des données sur un bus MQTT et les envoyer vers une base de données.
   
- Prend la forme d'un programme exécutable très léger et disponible sur la majorité des plateformes.
 
- OpenSource et édité par la société **influx***data*.

---

#### Présentation

<iframe class="r-stretch" src="https://www.youtube.com/embed/vGJeo3FaMds" 
  title="Intro to Telegraf" frameborder="0" 
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>

---
#### Fonctionnement 
- Création du fichier de configuration `telegraf.conf` pour définir les entrées (sources) de données, et les sorties. 
- Lancement : 
```bash
telegraf --config telegraf.conf
```
- Une fois lancé le programme reste actif et surveille en permanence les sources pour alimenter les sorties 
---

#### Le fichier de configuration 
- Il définit au moins une source de données et une sortie 
- Il peut aussi contenir des aggrégateurs et des processeurs (filtres) de données qui s'insèrent entre les entrées et les sorties. 
- Exemple : 

```ini
[[inputs.mqtt_consumer]]
  servers = ["tcp://127.0.0.1:1883"]
  topics = [ "rpi/+" ]
  
[[outputs.influxdb_v2]]
  urls = ["http://127.0.0.1:8086"]
  token = "sdfjkjzaefqldkdlsfk=="
  organization = "isis"
  bucket = "picapteurs"
```
---

#### Format des données
- Telegraf transforme les données sources dans le modèle d'*InfluxDB*. Chaque entrée a ainsi : 
  - Un nom de mesure (*measurement*)
  - Zéro ou plusieurs étiquettes (*tags*)
  - Un ou plusieurs champs (*fields*)
  - Un horodatage (*timestamp*)

- Par défaut la donnée est exprimée dans le format *line protocol* mais d'autres formats de sortie sont possibles
 
---

#### Exemple 1
Configuration 
 
```ini
[[inputs.mqtt_consumer]]
  servers = ["tcp://192.168.2.165:1883"]
  ## Topics that will be subscribed to.
  topics = [ "rpi/pression", "rpi/temperature", "rpi/humidite" ]

  data_format = "value"
  data_type = "float"

[[outputs.file]]
  files = ["stdout"]
```
Sortie
 
```
mqtt_consumer,host=raspisis,topic=rpi/pression value=1000.566162109375 1668066425183256256
mqtt_consumer,host=raspisis,topic=rpi/temperature value=33.73648452758789 1668066425183357712
mqtt_consumer,host=raspisis,topic=rpi/humidite value=46.27458190917969 1668066425183614166
```
---

#### Exemple 2
Configuration

```ini
[[inputs.mqtt_consumer]]
  servers = ["tcp://192.168.2.165:1883"]
  ## Topics that will be subscribed to.
  topics = [ "rpi/pression", "rpi/temperature", "rpi/humidite" ]
  data_format = "value"
  data_type = "float"

  [[inputs.mqtt_consumer.topic_parsing]]
     topic = "rpi/+"
     measurement = "_/measurement"
     tags = "location/_"

[[outputs.file]]
  files = ["stdout"]
```
Sortie

```
pression,host=raspisis,location=rpi,topic=rpi/pression value=1005.9130859375 1668066961613232911
temperature,host=raspisis,location=rpi,topic=rpi/temperature value=33.73648452758789 1668066961613323170
humidite,host=raspisis,location=rpi,topic=rpi/humidite value=46.27458190917969 1668066961613435357
```
---
#### Exemple 3
Configuration

```ini
[[inputs.mqtt_consumer]]
  servers = ["tcp://192.168.2.165:1883"]
  ## Topics that will be subscribed to.
  topics = [ "rpi/pression", "rpi/temperature", "rpi/humidite" ]
  data_format = "value"
  data_type = "float"

  [[inputs.mqtt_consumer.topic_parsing]]
     topic = "rpi/+"
     measurement = "_/measurement"
     tags = "location/_"

[[outputs.file]]
  tagexclude = ["host","topic"]
  files = ["stdout"]
```
Sortie

```
pression,location=rpi value=1005.9130859375 1668067103243142155
temperature,location=rpi value=33.73648452758789 1668067103243220644
humidite,location=rpi value=46.27458190917969 1668067103243270748
```
---
#### Exemple 4
Configuration

```ini
[[inputs.mqtt_consumer]]
  servers = ["tcp://192.168.2.165:1883"]
  ## Topics that will be subscribed to.
  topics = [ "rpi/pression", "rpi/temperature", "rpi/humidite" ]
  data_format = "value"
  data_type = "float"

  [[inputs.mqtt_consumer.topic_parsing]]
     topic = "rpi/+"
     measurement = "_/measurement"
     tags = "location/_"

[[outputs.file]]
  tagexclude = ["host","topic"]
  files = ["stdout"]
  data_format = "json"
```
Sortie

```json
{"fields":{"value":1005.9130859375},"name":"pression","tags":{"location":"rpi"},"timestamp":1668067207}
{"fields":{"value":33.73648452758789},"name":"temperature","tags":{"location":"rpi"},"timestamp":1668067207}
{"fields":{"value":46.27458190917969},"name":"humidite","tags":{"location":"rpi"},"timestamp":1668067207}
```
---

#### Un grand choix de sources de données (241 !)

- Des bases de données (SQL, MongoDB, Redis, InfluxDB, ...)
- Des hubs Iot (Philips Hue, MQTT, Fibaro, ...)
- Des services Cloud (Azure, AWS, ...)
- Des métriques applicatives (GitHub, Plex, 
---

#### Idem pour les sorties (57)
- Beaucoup de bases de données (InfluxDB, SQL, NoSQL, ...)
- Iot (MQTT publisher)

---

####  Références
- Documentation générale sur telegraf avec :
  - Toutes les options de configurations
  - Tous les plugins disponibles    
    - https://docs.influxdata.com/telegraf
