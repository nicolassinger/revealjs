![logo](backgrounds/influxdb.png)  <!-- .element height="50%" width="50%" -->

*Nicolas Singer*
---

#### InfluxDB

- Une base de données orientées séries temporelles haute performance
- Très rapide en écriture
- Adaptée au stockage de données en provenance de capteurs
- Développée par la société *InfluxData* sous licence libre

---

#### Principe (1/2)
- L'unité de stockage est le point constitué d'un ou plusieurs champs (*field*) constitués chacun d'une paire clé-valeur
- Exemple:  
  - une température : `temp = 35`
  - Un relevé d'accéléromètre : `x=1.4, y=0.7, z=0`
- Chaque point est associé à un *timestamp* qui mesure le temps avec une précision d'une nanoseconde.
---

#### Principe (2/2)

- Chaque point peut être associé à des étiquettes (*tag*) constituées chacune d'une paire clé-valeur.
- Exemple `location = "Castres", usager = "Singer"`
 
- Enfin on regroupe un ensemble de points dans un conteneur appelé *measurement* qui joue le même rôle qu'une
  table *sql*
- Exemple le *measurement*  `acceleration`

---

#### Exemple

<table>
<thead><tr>
<th>measurement</th>
<th>time</th>
<th class="field">x</th>
<th class="field">y</th>
<th class="field">z</th>
<th class="tag">location</th>
</tr></thead>
<tbody><tr>
<td>acceleration</td>
<td>2022-08-18T14:02:01Z</td>
<td class="field">1.4</td>
<td class="field">0.7</td>
<td class="field">0</td>
<td class="tag">Castres</td>
</tr>
<tr>
<td>acceleration</td>
<td>2022-08-18T14:02:04Z</td>
<td class="field">0.9</td>
<td class="field">0.2</td>
<td class="field">0.6</td>
<td class="tag">Castres</td>
</tr>
<tr>
<td>acceleration</td>
<td>2022-08-18T14:02:02Z</td>
<td class="field">1.4</td>
<td class="field">0.1</td>
<td class="field">0</td>
<td class="tag">Castres</td>
</tr>
</tbody>
</table>

- Dans ce tableau <span class="field">`x`</span>, <span class="field">`y`</span> et <span class="field">`z`</span> 
sont des champs, alors que <span class="tag">`location`</span> est une étiquette.

---

#### Champs ou étiquette ? 

- Les étiquettes sont indexées, les champs ne le sont pas
- Le choix dépend donc du type de requêtes

> En règle générale, les champs représentent des données qui varient beaucoup quand les étiquettes sont plus stables
> 
---

#### Représentation syntaxique d'un point
<ul>
  <li> Le <span class="italic">line protocol</span>
  <ul>
  <li> Les étiquettes sont séparées du <span class="italic">measurement</span> par une virgule. 
Puis viennent les champs et enfin le <span class="italic">timestamp</span>.</li>
  </ul>

``` 
<measurement>[,<tag_key>=<tag_value>[,<tag_key>=<tag_value>]] 
  <field_key>=<field_value>[,<field_key>=<field_value>] [<timestamp>]
```
<li> Exemple </li>

``` 
acceleration,location="Castres" x=1.4,y=0.2,z=0 1556813561098000000
```
</ul>
---

#### Typage des valeurs
- Une valeur numérique est par défaut un nombre flottant (*float*).
- Les chaines de caractères sont entre guillemets.
- Les entiers non signés sont suffixés par le caractère `u`
- Les entiers signés sont suffixés par le catactère `i`
- Les booléens s'écrivent `true`, `false`, `t` ou `f`, en majuscules ou en minuscules.
- Exemple
 
``` 
ville,location="France",nom="Castres",prefecture=false,population=54234u temp=23,4 1556813561098000000
```

---

#### Organisations et buckets
- Les séries de valeurs (*measurements*) sont regroupés dans un **bucket**. C'est l'équivalent de la base de données 
en *sql*.
- Les *buckets* peuvent avoir une période de rétention. Si le *timestamp* d'un point est en dehors de cette période, le point
est automatiquement supprimé. Si la période de rétention n'est pas précisée, elle est infinie.
- Les *buckets* appartiennent à une **organisation**. L'organisation sert donc de conteneur de plus haut niveau.
---

#### Outils pour manipuler les données 
- InfluxDB propose plusieurs outils pour créer/ajouter/effacer des données
  - l'API InfluxDB: Des requêtes HTTP au format REST 
  - Le CLI, un outil en ligne de commande 
  - Des bibliothèques dans la plupart des langages de programmation

---
#### Le CLI
- Influx dipose d'un outil en ligne de commande qui permet de manipuler les données
- Création d'une organisation appelée *isis*
 
```
influx org create -n isis
```
- Création d'un bucket appelé *capteurs* avec une rétention des données pendant 72 heures.
 
```
influx bucket create -n capteurs -o isis -r 72h
```
---
 
#### Ecriture via le CLI
- Ecrire un nouveau point avec le CLI
```console
influx write --bucket capteurs "acceleration,location='Castres' x=1.4,y=0.2,z=0 1556813561098000000"
```

- Ecrire des nouveaux points décrits dans un fichier au format *line protocol* 
```shell
influx write --bucket capteurs --file chemin/vers/fichier.txt
```
---

#### Ecriture via l'API

- On effectue une requête `POST` vers l'URL : 
  - https://votreserveur.org/api/v2/write?org=isis&bucket=capteurs
- et on passe un `BODY` avec la liste des points au format *line protocol*

---

#### Les requêtes 
- InfluxDB (depuis sa version 2) utilise le langage *flux* pour interroger la base de données
- Le principe de ce langage est de spécifier les différentes étapes nécessaires à sélectionner les points que l'on souhaite récupérer
- Les étapes de base sont : 
  - Spécifier la source : `from()`
  - Spécifier l'intervalle de temps: `range()`
  - Filtrer les données : `filter()`
  - Afficher les données : `yield()`
---

#### Un exemple de requête
```
from(bucket: "airflow")
  |> range(start: -30d)
  |> filter(fn: (r) => r["_measurement"] == "airSensors")
  |> filter(fn: (r) => r["_field"] == "humidity")
  |> yield()
```
---

#### Spécifier la source
- On utilise `from()` avec un paramètre `bucket` positionné sur le nom du *bucket* qui correspond aux données sources.
```
from(bucket: "Nom du bucket")
```
---

#### Spécifier un intervalle de temps
- On utilise `range()` avec les paramètres `start` et `stop` qui peuvent définir une date absolue ou relative : 
- Exemple en relatif : 
```
range(start: -1h, stop: -10m)
```
- Exemple en absolu : 
```
range(start: 2021-01-01T00:00:00Z, stop: 2021-01-01T12:00:00Z)
```
---

#### Spécifier un filtre
- On utilise `filter()` avec le paramètre `fn` qui spécifie une fonction qui prend en paramètre 
le prochain enregistrement. Si la fonction renvoie faux, l'enregistrement est écarté par le filtre, 
sinon il est conservé. 
- Exemple :
```
filter(fn: (r) => r._measurement == "acceleration" and r._field == "x" and r.location == "castres")
```
---

#### Transformer les données
- On peut vouloir effectuer des calculs sur les données, ou les transformer avant de les afficher :
  - Agréger les données par intervalle de temps ou par type
  - Trier 
  - Limiter le nombre de résultats
  - Remplir les données manquantes 
  - Calculer des moyennes
  - Supprimer des champs ou des étiquettes
  - etc.
---

#### Partitionner les données dans des fenêtres temporelles 
- On utilise `window()` avec le paramètre `every` qui spécifie la largeur de la fenêtre.
- Exemple si on veut regrouper les données par fenêtres de 5 minutes :
```
window(every: 5m)
```
- Les unités de durées possibles sont :
  - 1ns, 1us, 1ms, 1s,  1m,  1h,   1d,  1w,  1mo, 1y 
---

#### Partitionner les données en fonction des colonnes
- on utilise `group()` avec le paramètre `columns` qui spécifie un tableau de colonnes.
- La partition se fait en regroupant les données qui ont les mêmes valeurs dans ces colonnes
- Exemple si on veut regrouper les données par location:
```
group(columns: ["location"])
```
---

#### Les opérations sur les données partitionnées
- `mean()` calcule la valeur moyenne pour chaque colonne 
- `median()` calcule la médiane
- `min()` et `max()`  calculent le min et le max 
- `sum()` effectue la somme des valeurs
- `count()` retourne le nombre de valeurs dans l'agrégation
- `first()` et `last()` retournent la première et la dernière valeur de l'agrégation

---

#### Les opérations deux en un pour partitionner et agréger (sous-échantillonnage)
- `aggregateWindow()` sous-échantillonne en regroupant les données dans une fenêtre temporelle et en
appliquant une fonction d'agrégation. 
- Exemples :
  - Sous échantillon des données par mois en prenant la moyenne
```
aggregateWindow(every: 1mo, fn: mean)
```
- Sous échantillon des données par semaine en prenant le max 
```
aggregateWindow(every: 1w, offset: -3d, fn: max)
```
> Note : L'offset sert à partir du lundi, sinon par défaut la semaine commence le jeudi.
---

#### Exemple en vidéo
On insère les données suivantes dans la base :
```
temperature,location=Castres temp=22 1667257200
temperature,location=Castres temp=20 1667257260
temperature,location=Castres temp=18 1667257320
temperature,location=Castres temp=19 1667257380
temperature,location=Castres temp=22 1667257440
temperature,location=Castres temp=21 1667257500
temperature,location=Castres temp=19 1667257560
temperature,location=Castres temp=20 1667257620
temperature,location=Castres temp=21 1667257680
temperature,location=Mazamet temp=26 1667257200
temperature,location=Mazamet temp=28 1667257260
temperature,location=Mazamet temp=27 1667257320
temperature,location=Mazamet temp=29 1667257380
temperature,location=Mazamet temp=30 1667257440
temperature,location=Mazamet temp=28 1667257500
temperature,location=Mazamet temp=29 1667257560
temperature,location=Mazamet temp=24 1667257620
temperature,location=Mazamet temp=27 1667257680
```

---

#### Exemple en vidéo

<video data-autoplay loop src="videos/influxdb.mp4"></video>

---

#### Références

- Le format *line protocol* :
  - https://docs.influxdata.com/influxdb/v2.5/reference/syntax/line-protocol
- Liste de toutes les fonctions du langage *flux* : 
  - https://docs.influxdata.com/flux/v0.x/stdlib/all-functions
