
![logo](backgrounds/iot.jpg)  <!-- .element height="50%" width="50%" -->

*Nicolas Singer*
---

#### Internet Of Things 

- c'est le réseau qui permet à des terminaux physiques ou logiciels de se connecter à d'autres terminaux. 
  - Ces terminaux peuvent être des appareils domestiques ou des outils industriels.
  - Ils peuvent être dotés de capteurs ou au contraire consommer ces données.

- Les objets physiques peuvent partager et collecter des données sans intervention humaine.

- L'IoT est la **rencontre du monde digital avec le monde physique**

---

#### Problématiques de l'IoT

- Une des problématiques principales est l'échange de données dans un contexte de **couplage faible** entre les équipements. 
- Une autre problématique est la fréquence importante à laquelle les données peuvent être échangées. 
- Cela nécessite des **architectures spécifiques** qui diffèrent de celles du web.
---
#### Sujet traité dans ce cours 

- Présentation d'une architecture classique en IoT basée sur :
  - Des **terminaux physiques** émetteurs de données.
  - Un **broker** vers lequel sont émises ces données avec un protocole publish/subscribe sur des *topics*.
  - Une **base de données** temporelle alimentées par les données publiées sur le broker.
  - Un **logiciel de contrôle** de type tableau de bord permettant de visualiser l'évolution des données et de générer des actions ou des alertes.
---

#### Schéma global de l'architecture à concevoir 

![logo](backgrounds/schema.jpg)  <!-- .element height="80%" width="80%" -->
