
![logo](backgrounds/logo.webp)  <!-- .element height="50%" width="50%" -->

*Nicolas Singer*
---

#### MQTT

- Un protocole de transport de messages par *publish/subscribe*.
- léger et simple à implémenter.
- Idéal dans un environnement numérique contraint au niveau du réseau ou de la mémoire.
  - Machine to Machine (M2M).
  - Internet of Things (IoT).
---

#### Historique
- Créé en 1999 par IBM pour connecter des pipelines par satellites (MQ Telemetry Transport).
- Version 3.11 opensource en 2010.
- Approuvé comme une norme par l'*OASIS Standard* en 2014.
- Dernière version 5 en 2019.
---

#### Fonctionnement de MQTT
- Basé sur l'achitecture publish/subscribe.
  - En client/serveur, le client communique directement avec le serveur.
  - En publish/subscribe, les deux rôles sont découplés.
    - Le client envoi le message à un intermediaire (le *broker*).
    - Le *serveur* (en fait un autre client) s'abonne aux données du broker qui l'intéresse.
    - le role du broker est de distribuer les messages entrants aux clients abonnés.

---
#### Vue d'ensemble
![logo](backgrounds/pubsub.jpg)  

---

#### Intérêt
- les clients n'ont pas à se connaître (découplage).
  - Découplage spatial (pas à se connaitre du point de vue réseau).
  - Découplage temporel (pas à tourner en même temps).
  - Découplage de la synchronisation (pas besoin de rester à l'écoute en permanence des messages).
- Architecture plus facile à mettre à l'échelle en fonction du nombre de clients.
- Filtrage des messages par le broker sur commande des clients (systèmes des **topics**).
---

#### Les *topics*
- MQTT permet de filtrer leur message en fonction de leur sujet (*topic*).
  - Chaque message doit être posté sur un certain topic.
  - Un client peut s'abonner à un ou plusieurs topics.
  - Le *broker* ne lui envoie que les messages destinés à ce topic.

---

#### Ce que MQTT n'est pas
- Ce n'est pas un système de gestion des queues de message (*Message Queue*)
  - Pas de gestion de la file d'attente si personne ne consomme un message publié.
  - Quand un message arrive, il est envoyé à tous les clients qui sont abonnés au topic.
    - Par opposition dans les *Message Queue*, le message est par défaut envoyé à un seul client.
  - Les topics sont créés à la volée (au moment des abonnements ou des publications) tandis que les queues
 doivent être créées au préalable. 
---

#### Le client MQTT
- C'est un publisher, un subscriber, ou les deux.
- C'est un appareil qui utilise une bibliothèque MQTT pour se connecter à un *broker*
- MQTT est un protocole très simple, tout langage de programmation dispose d'une bibliothèque MQTT.
  - Android, Ardhuino, C, C++, C#, GO, Java, Python, Ruby, Javascript, etc.
- Il existe des clients graphiques pour Windows, MacOS, Android, UNIX, etc.
---

#### Le *broker* MQTT
- Reçoit les messages et les retransmet aux clients concernés.
- Tient à jour les abonnements des clients et de leurs topics.
- Maintient les sessions des clients ayant des sessions permanentes.
- Peut authentifier et autoriser les clients
- Peut être capable de traiter des millions de connexions simultanées.

--- 

#### La connexion MQTT
- Se fait par TCP/IP sur le port par défaut 1883
 
![logo](backgrounds/osi.svg)  

---

#### Les topics
- Un topic désigne le sujet du message.
- C'est un ensemble de termes séparés par des `/`
![logo](backgrounds/topics.svg)
- Les topics commençant par `$` sont spéciaux car réservés à la publication de données internes au broker.
  - exemple le topic `$SYS/broker/client/connected` permet d'obtenir le nombre de clients connectés au broker.
---

#### Abonnement à un topic
- Un client peut s'abonner au topic exact ou utiliser des jokers pour désigner plusieurs topics.
- `+` pour désigner un seul niveau.
  - `domicile/+/temperature` : Les topics `temperature` de toutes les pièces de la maison.
- `#` pour désigner tous les niveaux du dessous.
  - `domicile/#`: Tous les topics du domicile.
---

#### La Qualité de Service (QoS)
- C'est un contrat passé entre l'envoyeur et le receveur sur la garantie de délivrer un message
- MQTT défini trois niveaux de QoS : 
  - 0 : Aucune garantie. Le récepteur n'accuse pas réception.
  - 1 : Le récepteur doit acquitter. L'expéditeur re-expédie au bout d'un certain temps si pas d'acquittement
(risque de doublon)
  - 2 : Le récepteur doit acquitter et attendre l'acquittement retour de l'envoyeur pour envoyer le message. 
(supprime le risque de doublon)
---

### Quelle QoS Utiliser ? 
- **QoS 0** si la liaison réseau est stable et la perte d'un message de temps en temps n'est pas critique.
  - Egalement si pas de besoin de maintenir des sessions persistantes (voir plus loin).
- **QoS 1** si nécessaire d'avoir tous les messages, mais pas grave d'en avoir certains plusieurs fois.
- **QoS 2** s'il est critique de recevoir chaque message une et une seule fois. Attention à la surcharge liée aux
interactions plus complexes.
---

#### Les sessions persistantes
- Quand une connexion est coupée entre un client *subscriber* et le broker, le client doit se reconnecter et
s'abonner à nouveau aux topics qu'il l'intéresse.
- Pour l'éviter, un client peut demander une session persistante. 
  - Le broker mémorise les topics auxquels le client est abonné
  - Quand le client revient : 
    - Avec une QoS de 0, les abonnements sont renouvelés (mais les messages arrivés entre temps sont perdus)
    - Avec une QoS de 1, idem que 0 mais le broker a conservé les messages qui sont transmis au client
    - Avec une QoS de 2, idem que 1 mais le broker a conservé les messages publiés mais non acquittés par le client.
--- 

#### Les messages mémorisés 
- Un message peut être publié avec un drapeau de mémorisation (*Retain*)
- Le broker enregistre chaque dernier message d'un topic portant ce drapeau
- Il transmet ce message à tout client s'abonnant pour la première fois au topic. 
- **Cela permet à un nouveau client de visualiser immédiatement la dernière valeur transmise au topic**.
 
---
#### Le message de dernière volonté
- MQTT est fréquemment utilisé dans des environnements ou le réseau est instable. 
  - Le broker teste régulièrement la connexion de ses clients 

- Quand un client se connecte au broker, il peut spécifier un message de dernière volonté possèdant 
un topic et un contenu. 
- Quand le broker détecte que le client s'est déconnecté intempestivement, il publie ce message.
- **On peut donc s'abonner à ce topic pour savoir si un client est bien connecté.**
---

#### Cas d'usage du message de dernière volonté
- Un client publie des températures sur le topic `dom/temp`.
- Lors de sa connexion, il publie le message `online` sur le topic `dom/temp_statut` en mode *Retain*
- Son message de dernière volonté est de publier `offline` sur le topic `dom/temp_statut` en mode *Retain*.
- Si le broker détecte la déconnexion du client, il exécute le message de dernière volonté.
- **Un client voulant connaître le statut du publiant de temperature peut donc s'abonner au topic `dom/temp_statut`**.
